//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/allocator.hpp"
#include "core/core_init.hpp"
#include "core/hash_set.inl"
#include "core/linear_allocator.hpp"
#include "core/log.hpp"
#include "core/mono_time.hpp"
#include "core/sorted_map.inl"

#include <assert.h>

#include <map>
#include <unordered_set>

void benchmark(int64_t count) {
  LOGI("count: %lld", count);
  LOGI("  Insert benchmark:");
  linear_allocator_t hash_map_allocator;
  la_init(&hash_map_allocator, "hash_map_allocator");
  hash_set_t<int64_t> hash_set;
  ht_init(&hash_set, &hash_map_allocator);
  ht_reserve(&hash_set, count);

  mono_time_t t = mtime_now();
  for (int64_t i = 0; i < count; ++i)
    ht_insert_or_get(&hash_set, i);
  LOGI("  - hash_set_t:            %fms", mtime_to_ms(mtime_now() - t));

  t = mtime_now();
  std::unordered_set<int64_t> std_unordered_set;
  for (int64_t i = 0; i < count; ++i)
    std_unordered_set.insert(i);
  LOGI("    std::unordered_set:    %fms", mtime_to_ms(mtime_now() - t));

  // t = mtime_now();
  // linear_allocator_t sorted_map_allocator;
  // la_init(&sorted_map_allocator, "sorted_map_allocator");
  // sorted_map_t<int64_t, int64_t> sorted_map;
  // sa_init(&sorted_map, &sorted_map_allocator);
  // sa_reserve(&sorted_map, count);
  // for (int64_t i = 0; i < count; ++i)
  //   sa_insert_or_get(&sorted_map, i, i);
  // LOGI("  - sorted_map_t:          %fms", mtime_to_ms(mtime_now() - t));

  // t = mtime_now();
  // std::unordered_map<int64_t, int64_t> std_map;
  // for (int64_t i = 0; i < count; ++i)
  //   std_map[i] = i;
  // LOGI("    std::map:              %fms", mtime_to_ms(mtime_now() - t));

  // for (int64_t i = 0; i < count; ++i) {
  //   assert(std_map[i] == std_unordered_map[i]);
  //   assert(sa_find(&sorted_map, i)->value == ht_find(&hash_map, i)->value);
  //   assert(std_unordered_map[i] == ht_find(&hash_map, i)->value);
  // }

  LOGI("  Lookup benchmark:");

  int64_t ht_sum = 0;
  t = mtime_now();
  for (int64_t i = 0; i < count; ++i) {
    auto it = ht_find(&hash_set, i);
    ht_sum += it.ht->keys.p[it.idx].key;
  }
  LOGI("  - hash_set_t:            %fms, sum = %lld",
       mtime_to_ms(mtime_now() - t),
       ht_sum);

  int64_t std_sum = 0;
  t = mtime_now();
  for (int64_t i = 0; i < count; ++i)
    std_sum += *std_unordered_set.find(i);
  LOGI("    std::unordered_set:    %fms, sum = %lld",
       mtime_to_ms(mtime_now() - t),
       std_sum);

  // t = mtime_now();
  // for (int64_t i = 0; i < count; ++i)
  //   sa_find(&sorted_map, i);
  // LOGI("  - sorted_map_t:          %fms", mtime_to_ms(mtime_now() - t));

  // t = mtime_now();
  // for (int64_t i = 0; i < count; ++i)
  //   std_map.find(i);
  // LOGI("    std::map:              %fms", mtime_to_ms(mtime_now() - t));

  allocator_destroy(&hash_map_allocator);
  // allocator_destroy(&sorted_map_allocator);
}

int main() {
  core_init(OS_LIT("map_benchmark.log"));
  benchmark(1000);
  benchmark(10000);
  benchmark(100000);
  benchmark(1000000);
  benchmark(10000000);

  return 0;
}
