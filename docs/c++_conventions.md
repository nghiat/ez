# Header Guards
Use
```
#pragma once
```
instead of
```
#ifdef
#define
#endif
```
**Reason**: Much shorter, less spelling errors (maybe faster).
# Include files
* Alphabetically ordered.
* Full paths (except system headers)
* Order
  * Corresponding header file (if you are editing a cpp file) + empty line.
  * Project + third party header files.
  * System header files.
  * C++ standard headers + C standard headers.
* Don't use c* headers (cstdlib, cmath, ...)

  **Reason**: Functions in c** headers are in std namespace.
# Naming
## Folder name
* lower_case
## Filename
* Prefix: lower_case
* Suffix:
  * .hpp for C/C++ headers.
  * .cpp for C/C++ sources.
  * .inl for template function implementations.
## Preprocessor/Macro:
* UPPER_CASE
* Changing values are closed to the end (e.g. ARCH_X86, ARCH_ARM... not X86_ARCH, ARM_ARCH...) unless they are words (e.g. BIG_ENDIAN, LITTLE_ENDIAN...)
## Namespace
* lower_case_name
## Template parameter
* Camel_Case
## Class/Struct
* lower_case_t
## Enum
* enum_e
* ENUM_VALUE
## Function/Method
* Function/Method name should start with a verb (maybe common prefix depends on file name).
* Function/Method names: lower_case
* In parameters names: lower_case
* Out parameters names: o_camel_case
* Trivial function/method can be inlined.
## Variables
* Global variable names: g_lower_case
* Static variable names: s_lower_case
* Const variable names (except parameters, local variables): c_lower_case
* local variable names: lower_case
* Public field: lower_case
* Private field: lower_case_
* Multiple prefixes if the variable has multiple attributes: g, _ > s > c, o
## Enum values
* e_lower_case
# Misc.
* Reference(&, &&) and asterisk(*) is next to typename.
* Spaces instead of tabs.
* Default tabs size: 2.
* Max line width: 80.
* Class can only contain private members.

  **Exception**: const members.
* Parameters order: in, update, out.
* Parameters passed by reference must be const

  **Reason**: On the caller's side, the code looks like you are passing the argument by value, therefore it should also behave like you are passing the argument by value.
