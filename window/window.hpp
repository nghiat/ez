//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/string.hpp"
#include "window/key_code.hpp"

struct platform_data_t;
struct window_t;

struct window_fns_t {
public:
  bool (*init)(window_t*);
  void (*destroy)(window_t*);
  void (*loop)(window_t*);
  void (*on_mouse_down)(window_t*, int x, int y);
  void (*on_mouse_up)(window_t*, int x, int y);
  void (*on_mouse_move)(window_t*, int x, int y);
  void (*on_key_event)(window_t*, key_code_e key, bool is_down);
};

struct window_t {
  const os_char_t* title = nullptr;
  platform_data_t* data = nullptr;
  const window_fns_t* fns = nullptr;
};

bool window_init(window_t* w, const os_char_t* title, const window_fns_t* fns);
void window_destroy(window_t* w);
void window_loop(window_t* w);
