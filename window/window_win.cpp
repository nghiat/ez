//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "window/window.hpp"

#include "core/allocator.hpp"
#include "core/core_allocators.hpp"
#include "core/log.hpp"
#include "gfx/opengl/gl_loader.hpp"
#include "gfx/opengl/wgl.hpp"
#include <Windows.h>
#include <windowsx.h>

struct platform_data_t {
  HWND hwnd;
  HDC device_context;
  HGLRC shared_gl_context;
  HGLRC core_gl_context;
};

namespace {

key_code_e g_vk_to_key_code[256];

void init_key_codes_map() {
  g_vk_to_key_code[0x41] = key_code_e::KC_A;
  g_vk_to_key_code[0x44] = key_code_e::KC_D;
  g_vk_to_key_code[0x53] = key_code_e::KC_S;
  g_vk_to_key_code[0x57] = key_code_e::KC_W;
}

LRESULT CALLBACK wnd_proc(_In_ HWND hwnd,
                          _In_ UINT msg,
                          _In_ WPARAM w_param,
                          _In_ LPARAM l_param) {
  window_t* w = (window_t*)GetWindowLongPtr(hwnd, GWLP_USERDATA);
  switch (msg) {
  case WM_CLOSE:
    DestroyWindow(hwnd);
    break;
  case WM_DESTROY:
    PostQuitMessage(0);
    break;
  case WM_KEYUP:
  case WM_KEYDOWN:
    w->fns->on_key_event(w, g_vk_to_key_code[w_param], msg == WM_KEYDOWN);
    break;
  case WM_LBUTTONDOWN:
    w->fns->on_mouse_down(w, GET_X_LPARAM(l_param), GET_Y_LPARAM(l_param));
    break;
  case WM_LBUTTONUP:
    w->fns->on_mouse_up(w, GET_X_LPARAM(l_param), GET_Y_LPARAM(l_param));
    break;
  case WM_MOUSEMOVE:
    w->fns->on_mouse_move(w, GET_X_LPARAM(l_param), GET_Y_LPARAM(l_param));
    break;
  default:
    return DefWindowProc(hwnd, msg, w_param, l_param);
  }
  return 0;
}

} // namespace

bool window_init(window_t* w, const os_char_t* title, const window_fns_t* fns) {
  HINSTANCE hinstance;
  WNDCLASSEX wcex;
  HWND hwnd;
  PIXELFORMATDESCRIPTOR pfd;
  HDC device_context;
  int pixel_format;
  HGLRC shared_gl_context;
  constexpr int c_context_attribs[] = {WGL_CONTEXT_MAJOR_VERSION_ARB,
                                       3,
                                       WGL_CONTEXT_MINOR_VERSION_ARB,
                                       3,
                                       WGL_CONTEXT_PROFILE_MASK_ARB,
                                       WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
                                       0,
                                       0};
  HGLRC core_gl_context;

  w->title = title;
  w->fns = fns;
  init_key_codes_map();
  hinstance = GetModuleHandle(NULL);
  wcex.cbSize = sizeof(WNDCLASSEX);
  wcex.style = CS_OWNDC;
  wcex.lpfnWndProc = &wnd_proc;
  wcex.cbClsExtra = 0;
  wcex.cbWndExtra = 0;
  wcex.hInstance = hinstance;
  wcex.hIcon = LoadIcon(hinstance, MAKEINTRESOURCE(IDI_APPLICATION));
  wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
  wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
  wcex.lpszMenuName = NULL;
  wcex.lpszClassName = w->title;
  wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

  if (!RegisterClassEx(&wcex)) {
    LOGW("Can't register WNDCLASSEX");
    return false;
  }
  hwnd = CreateWindow(w->title,
                      w->title,
                      WS_OVERLAPPEDWINDOW,
                      CW_USEDEFAULT,
                      CW_USEDEFAULT,
                      1024,
                      768,
                      NULL,
                      NULL,
                      hinstance,
                      NULL);
  if (!hwnd) {
    LOGW("Can't create HWND");
    return false;
  }
  SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)w);
  ShowWindow(hwnd, SW_SHOWNORMAL);
  UpdateWindow(hwnd);
  pfd = {sizeof(PIXELFORMATDESCRIPTOR),
         1,
         PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER, // Flags
         PFD_TYPE_RGBA, // The kind of framebuffer. RGBA or palette.
         32,            // Colordepth of the framebuffer.
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         24, // Number of bits for the depthbuffer
         8,  // Number of bits for the stencilbuffer
         0,  // Number of Aux buffers in the framebuffer.
         PFD_MAIN_PLANE,
         0,
         0,
         0,
         0};
  device_context = GetDC(hwnd);
  pixel_format = ChoosePixelFormat(device_context, &pfd);
  SetPixelFormat(device_context, pixel_format, &pfd);
  shared_gl_context = wglCreateContext(device_context);
  wglMakeCurrent(device_context, shared_gl_context);
  gfx::load_platform_gl_extensions();
  core_gl_context = wgl_create_context_attribs_arb(
      device_context, shared_gl_context, c_context_attribs);
  wglMakeCurrent(device_context, core_gl_context);
  bool rv = w->fns->init(w);
  w->data = (platform_data_t*)allocator_alloc(g_persistent_allocator,
                                              sizeof(platform_data_t));
  *w->data = {hwnd, device_context, shared_gl_context, core_gl_context};
  return rv;
}

void window_destroy(window_t* w) {
  DestroyWindow(w->data->hwnd);
  allocator_free(g_persistent_allocator, w->data);
}

void window_loop(window_t* w) {
  MSG msg;
  while (true) {
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
      if (msg.message == WM_QUIT)
        break;
      TranslateMessage(&msg);
      DispatchMessage(&msg);
    } else {
      w->fns->loop(w);
      SwapBuffers(w->data->device_context);
    }
  }
}
