//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "window/window.hpp"

#include "core/allocator.hpp"
#include "core/core_allocators.hpp"
#include "core/log.hpp"
#include "gfx/opengl/gl.hpp"
#include "gfx/opengl/gl_loader.hpp"
#include "gfx/opengl/glx.hpp"
#include <GL/glx.h>
#include <X11/Xlib-xcb.h>
#include <X11/Xlib.h>
#include <stdlib.h>
#include <xcb/xcb.h>
#include <xcb/xcb_keysyms.h>
#include <xkbcommon/xkbcommon-keysyms.h>

struct platform_data_t {
  Display* xdisplay;
  xcb_connection_t* xcb_connection;
  xcb_intern_atom_reply_t* reply2;
  uint32_t xcb_window_id;
  GLXContext glx_context;
  GLXWindow glx_window;
  xcb_key_symbols_t* key_symbols;
};

namespace {
key_code_e g_xcb_key_code_to_key_code[256];

void init_key_codes_map(window_t* w) {
  const int c_key_code_to_xkb[key_code_e::KC_COUNT] = {
      XKB_KEY_A,
      XKB_KEY_D,
      XKB_KEY_S,
      XKB_KEY_W,
  };
  for (int i = 0; i < key_code_e::KC_COUNT; i++) {
    key_code_e code = (key_code_e)i;
    xcb_keycode_t* kc =
        xcb_key_symbols_get_keycode(w->data->key_symbols, c_key_code_to_xkb[i]);
    if (kc) {
      g_xcb_key_code_to_key_code[*kc] = code;
      free(kc);
    } else {
      LOGW("xcb_keycode_t* for key code %d is NULL", code);
    }
  }
}
} // namespace

bool window_init(window_t* w, const os_char_t* title, const window_fns_t* fns) {
  Display* xdisplay = nullptr;
  int default_screen;
  xcb_connection_t* xcb_connection = nullptr;
  xcb_screen_t* screen = nullptr;
  const int c_visual_attribs[] = {GLX_X_RENDERABLE,
                                  True,
                                  GLX_DRAWABLE_TYPE,
                                  GLX_WINDOW_BIT,
                                  GLX_RENDER_TYPE,
                                  GLX_RGBA_BIT,
                                  GLX_X_VISUAL_TYPE,
                                  GLX_TRUE_COLOR,
                                  GLX_RED_SIZE,
                                  8,
                                  GLX_GREEN_SIZE,
                                  8,
                                  GLX_BLUE_SIZE,
                                  8,
                                  GLX_ALPHA_SIZE,
                                  8,
                                  GLX_DEPTH_SIZE,
                                  8,
                                  GLX_STENCIL_SIZE,
                                  8,
                                  GLX_DOUBLEBUFFER,
                                  True,
                                  None};
  int num_fb_configs = 0;
  GLXFBConfig* fb_configs = nullptr;
  int best_fb_config = -1;
  int best_num_samples = -1;
  GLXFBConfig fb_config;
  int vid = 0;
  GLXContext glx_context = 0;
  uint32_t xcb_window_id;
  uint32_t value_mask;
  uint32_t event_mask;
  xcb_colormap_t colormap;
  uint32_t value_list[3];
  xcb_intern_atom_cookie_t cookie;
  xcb_intern_atom_reply_t* reply;
  xcb_intern_atom_cookie_t cookie2;
  xcb_intern_atom_reply_t* reply2;
  constexpr int c_context_attribs[] = {GLX_CONTEXT_MAJOR_VERSION_ARB,
                                       3,
                                       GLX_CONTEXT_MINOR_VERSION_ARB,
                                       3,
                                       GLX_CONTEXT_PROFILE_MASK_ARB,
                                       GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
                                       None};
  GLXWindow glx_window;
  xcb_key_symbols_t* key_symbols;

  w->title = title;
  w->fns = fns;
  xdisplay = XOpenDisplay(0);
  if (!xdisplay) {
    LOGW("XOpenDisplay failed");
    goto out_err;
  }
  default_screen = DefaultScreen(xdisplay);
  xcb_connection = XGetXCBConnection(xdisplay);
  if (!xcb_connection) {
    LOGW("XGetXCBConnection failed");
    goto out_err;
  }
  XSetEventQueueOwner(xdisplay, XCBOwnsEventQueue);
  screen = xcb_setup_roots_iterator(xcb_get_setup(xcb_connection)).data;

  fb_configs = glXChooseFBConfig(
      xdisplay, default_screen, c_visual_attribs, &num_fb_configs);
  if (!(fb_configs && num_fb_configs)) {
    LOGW("glXChooseFBConfig failed");
    goto out_err;
  }
  // Pick the FBConfig with the most samples per pixel.
  for (int i = 0; i < num_fb_configs; ++i) {
    XVisualInfo* vi = glXGetVisualFromFBConfig(xdisplay, fb_configs[i]);
    if (vi) {
      int sample_buffers;
      int samples;
      glXGetFBConfigAttrib(
          xdisplay, fb_configs[i], GLX_SAMPLE_BUFFERS, &sample_buffers);
      glXGetFBConfigAttrib(xdisplay, fb_configs[i], GLX_SAMPLES, &samples);
      if ((best_fb_config < 0 || sample_buffers) &&
          (samples > best_num_samples)) {
        best_fb_config = i;
        best_num_samples = samples;
      }
    }
  }
  fb_config = fb_configs[best_fb_config];
  XFree(fb_configs);

  glXGetFBConfigAttrib(xdisplay, fb_config, GLX_VISUAL_ID, &vid);
  glx_context =
      glXCreateNewContext(xdisplay, fb_config, GLX_RGBA_TYPE, 0, True);
  if (!glx_context) {
    LOGW("glXCreateNewContext failed\n");
    goto out_err;
  }
  xcb_window_id = xcb_generate_id(xcb_connection);
  value_mask = XCB_CW_EVENT_MASK | XCB_CW_COLORMAP;
  event_mask = XCB_EVENT_MASK_BUTTON_1_MOTION | XCB_EVENT_MASK_BUTTON_PRESS |
               XCB_EVENT_MASK_BUTTON_RELEASE | XCB_EVENT_MASK_KEY_PRESS |
               XCB_EVENT_MASK_KEY_RELEASE;
  colormap = xcb_generate_id(xcb_connection);
  xcb_create_colormap(
      xcb_connection, XCB_COLORMAP_ALLOC_NONE, colormap, screen->root, vid);
  value_list[0] = event_mask;
  value_list[1] = colormap;
  value_list[2] = 0;
  xcb_create_window(xcb_connection,
                    XCB_COPY_FROM_PARENT,
                    xcb_window_id,
                    screen->root,
                    0,
                    0,
                    150,
                    150,
                    0,
                    XCB_WINDOW_CLASS_INPUT_OUTPUT,
                    vid,
                    value_mask,
                    value_list);
  xcb_change_property(xcb_connection,
                      XCB_PROP_MODE_REPLACE,
                      xcb_window_id,
                      XCB_ATOM_WM_NAME,
                      XCB_ATOM_STRING,
                      8,
                      str_get_len(w->title),
                      w->title);
  cookie = xcb_intern_atom(xcb_connection, 1, 12, "WM_PROTOCOLS");
  reply = xcb_intern_atom_reply(xcb_connection, cookie, 0);

  cookie2 = xcb_intern_atom(xcb_connection, 0, 16, "WM_DELETE_WINDOW");
  reply2 = xcb_intern_atom_reply(xcb_connection, cookie2, 0);

  glx_window = glXCreateWindow(xdisplay, fb_config, xcb_window_id, 0);
  xcb_change_property(xcb_connection,
                      XCB_PROP_MODE_REPLACE,
                      xcb_window_id,
                      (*reply).atom,
                      4,
                      32,
                      1,
                      &(*reply2).atom);
  xcb_map_window(xcb_connection, xcb_window_id);

  gfx::load_platform_gl_extensions();
  glx_context = glx_create_context_attribs_arb(
      xdisplay, fb_config, 0, True, c_context_attribs);

  if (!glXMakeContextCurrent(xdisplay, glx_window, glx_window, glx_context)) {
    LOGW("glXMakeContextCurrent failed");
    goto out_err_free;
  }
  if (!w->fns->init(w))
    goto out_err_free;
  xcb_flush(xcb_connection);
  key_symbols = xcb_key_symbols_alloc(xcb_connection);
  w->data = (platform_data_t*)allocator_alloc(g_persistent_allocator,
                                              sizeof(platform_data_t));
  *w->data = {
      xdisplay,
      xcb_connection,
      reply2,
      xcb_window_id,
      glx_context,
      glx_window,
      key_symbols,
  };
  init_key_codes_map(w);
  return true;
out_err_free:
  glXDestroyWindow(xdisplay, glx_window);
  xcb_destroy_window(xcb_connection, xcb_window_id);
  glXDestroyContext(xdisplay, glx_context);
out_err:
  return false;
}

void window_destroy(window_t* w) {
  glXDestroyWindow(w->data->xdisplay, w->data->glx_window);
  xcb_destroy_window(w->data->xcb_connection, w->data->xcb_window_id);
  glXDestroyContext(w->data->xdisplay, w->data->glx_context);
  xcb_key_symbols_free(w->data->key_symbols);
  allocator_free(g_persistent_allocator, w->data);
}

void window_loop(window_t* w) {
  bool running = true;
  while (running) {
    xcb_generic_event_t* event = xcb_poll_for_event(w->data->xcb_connection);
    if (event) {
      switch (event->response_type & ~0x80) {
      case XCB_BUTTON_PRESS: {
        xcb_button_press_event_t* bp = (xcb_button_press_event_t*)event;
        w->fns->on_mouse_down(w, bp->event_x, bp->event_y);
      } break;
      case XCB_BUTTON_RELEASE: {
        xcb_button_release_event_t* br = (xcb_button_release_event_t*)event;
        w->fns->on_mouse_up(w, br->event_x, br->event_y);
      } break;
      case XCB_KEY_PRESS: {
        xcb_key_press_event_t* kp = (xcb_key_press_event_t*)event;
        w->fns->on_key_event(w, g_xcb_key_code_to_key_code[kp->detail], true);
      } break;
      case XCB_KEY_RELEASE: {
        xcb_key_release_event_t* kp = (xcb_key_release_event_t*)event;
        w->fns->on_key_event(w, g_xcb_key_code_to_key_code[kp->detail], false);
      } break;
      case XCB_MOTION_NOTIFY: {
        xcb_motion_notify_event_t* m = (xcb_motion_notify_event_t*)event;
        w->fns->on_mouse_move(w, m->event_x, m->event_y);
      } break;
      case XCB_CLIENT_MESSAGE:
        if ((*(xcb_client_message_event_t*)event).data.data32[0] ==
            (*w->data->reply2).atom)
          running = false;
        break;
      default:
        break;
      }
      free(event);
    }
    w->fns->loop(w);
    glXSwapBuffers(w->data->xdisplay, w->data->glx_window);
  }
}
