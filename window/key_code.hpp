//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

enum key_code_e {
  KC_A = 0,
  KC_D,
  KC_S,
  KC_W,

  KC_COUNT,
};

extern key_code_e g_key_code_map[256];
