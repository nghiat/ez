//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

// OS macros
#if defined(NDEBUG)
#  define BUILD_RELEASE
#else
#  define BUILD_DEBUG
#endif
