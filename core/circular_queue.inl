//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/circular_queue.hpp"

#include "core/dynamic_array.inl"

template <typename T>
bool operator==(const circular_queue_it_t<T>& lhs,
                const circular_queue_it_t<T>& rhs) {
  return lhs.array == rhs.array && lhs.array_it == rhs.array_it;
}

template <typename T>
bool operator!=(const circular_queue_it_t<T>& lhs,
                const circular_queue_it_t<T>& rhs) {
  return !(lhs == rhs);
}

template <typename T>
T& operator*(circular_queue_it_t<T>& it) {
  return *it.array_it;
}

template <typename T>
circular_queue_it_t<T>& operator++(circular_queue_it_t<T>& it) {
  if (++it.array_it == end(*it.array))
    it.array_it = begin(*it.array);
  return it;
}

template <typename T>
circular_queue_it_t<T>& operator--(circular_queue_it_t<T>& it) {
  if (it.array_it == begin(*it.array))
    it.array_it = end(*it.array);
  --(it.array_it);
  return it;
}

template <typename T>
bool cq_init(circular_queue_t<T>* cq, allocator_t* allocator) {
  bool rv = da_init(&cq->array, allocator);
  da_resize(&cq->array, 1);
  cq->begin_it = begin(cq->array);
  cq->end_it = end(cq->array) - 1;
  cq->length = 0;
  return rv;
}

template <typename T>
void cq_destroy(circular_queue_t<T>* cq) {
  da_destroy(&cq->array);
}

template <typename T>
circular_queue_it_t<T> begin(const circular_queue_t<T>& cq) {
  return circular_queue_it_t<T>{&cq.array, cq.begin_it};
}

template <typename T>
circular_queue_it_t<T> end(const circular_queue_t<T>& cq) {
  return circular_queue_it_t<T>{&cq.array, cq.end_it};
}

template <typename T, typename... Args>
void cq_append(circular_queue_t<T>* cq, Args... args) {
  size_t curr_arr_len = cq->array.length;
  if (cq->length + 1 < curr_arr_len) {
    new (cq->end_it) T(args...);
    circular_queue_it_t<T> end_it_it = end(*cq);
    cq->end_it = (++end_it_it).array_it;
  } else if (cq->end_it == end(cq->array) - 1) {
    new (cq->end_it) T(args...);
    da_resize(&cq->array, curr_arr_len + 1);
    cq->begin_it = begin(cq->array);
    cq->end_it = end(cq->array) - 1;
  } else {
    int32_t begin_it_dist = cq->begin_it - begin(cq->array);
    int32_t end_it_dist = cq->end_it - begin(cq->array);
    da_resize(&cq->array, curr_arr_len + 1);
    memmove(end(cq->array) - 1, begin(cq->array), sizeof(T));
    if (end_it_dist - 1 > 0)
      memmove(begin(cq->array),
              begin(cq->array) + 1,
              sizeof(T) * (end_it_dist - 1));
    cq->begin_it = begin(cq->array) + begin_it_dist;
    cq->end_it = begin(cq->array) + end_it_dist;
    circular_queue_it_t<T> end_it_it = end(*cq);
    new ((--end_it_it).array_it) T(args...);
  }
  ++cq->length;
}

template <typename T>
T cq_pop(circular_queue_t<T>* cq) {
  dynamic_array_it_t<T> it = cq->begin_it;
  --cq->length;
  circular_queue_it_t<T> begin_it_it = begin(*cq);
  cq->begin_it = (++begin_it_it).array_it;
  return *it;
}
