//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/stack_trace.hpp"

#include <cxxabi.h>
#include <execinfo.h>
#include <mutex>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

namespace {

char g_stack_trace_buffer[internal::gc_max_stack_trace_length];

std::mutex g_mutex;

} // namespace

bool stack_trace_init() { return true; }

char const* get_stack_trace() {
  std::lock_guard<std::mutex> lk(g_mutex);
  void* frames[internal::gc_max_traces];
  char** symbols;
  memset(g_stack_trace_buffer, 0, sizeof(g_stack_trace_buffer));
  char* current_buffer_pointer = g_stack_trace_buffer;
  size_t remaining_length = internal::gc_max_stack_trace_length;
  int frames_num = backtrace(frames, internal::gc_max_traces);
  symbols = backtrace_symbols(frames, frames_num);
  for (int i = 0; i < frames_num; ++i) {
    size_t symbol_len = strlen(symbols[i]);
    char* opening_bracket = (char*)memchr(symbols[i], '(', symbol_len);
    char* plus_sign = (char*)memchr(symbols[i], '+', symbol_len);
    char* final_symbol = nullptr;
    if (!plus_sign || !opening_bracket)
      final_symbol = symbols[i];
    char mangled_name[internal::gc_max_symbol_length];
    /// Demangled name of the function only
    char demangled_name[internal::gc_max_symbol_length];
    /// Demangled name of the whole string
    char demangled_string[internal::gc_max_symbol_length];
    if (!final_symbol) {
      int mangled_nameLen = plus_sign - opening_bracket - 1;
      memcpy(mangled_name, opening_bracket + 1, mangled_nameLen);
      mangled_name[mangled_nameLen] = '\0';
      int status;
      /// __cxa_demangle requires a non-const argument.
      size_t final_len = internal::gc_max_symbol_length;
      abi::__cxa_demangle(mangled_name, demangled_name, &final_len, &status);
      if (status == 0) {
        memcpy(demangled_string, symbols[i], opening_bracket - symbols[i] + 1);
        memcpy(demangled_string + (size_t)(opening_bracket - symbols[i]) + 1,
               demangled_name,
               strlen(demangled_name));
        memcpy(demangled_string + (size_t)(opening_bracket - symbols[i]) + 1 +
                   strlen(demangled_name),
               plus_sign,
               symbol_len - (plus_sign - symbols[i]));
        final_symbol = demangled_string;
      } else {
        final_symbol = symbols[i];
      }
    }
    const char* format;
    if (i)
      format = "\n\t%s";
    else
      format = "\t%s";
    size_t used_len = snprintf(
        current_buffer_pointer, remaining_length, format, final_symbol);
    current_buffer_pointer += used_len;
    remaining_length -= used_len;
  }
  free(symbols);
  return g_stack_trace_buffer;
}
