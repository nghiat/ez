//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/dynamic_array.hpp"

template <typename T>
struct circular_queue_t {
  // Reserves 1 element for |end|.
  dynamic_array_t<T> array;
  dynamic_array_it_t<T> begin_it;
  dynamic_array_it_t<T> end_it;
  size_t length = 0;
};

template <typename T>
struct circular_queue_it_t {
  const dynamic_array_t<T>* array;
  dynamic_array_it_t<T> array_it;
};

template <typename T>
bool operator==(const circular_queue_it_t<T>& lhs,
                const circular_queue_it_t<T>& rhs);

template <typename T>
bool operator!=(const circular_queue_it_t<T>& lhs,
                const circular_queue_it_t<T>& rhs);

template <typename T>
T& operator*(circular_queue_it_t<T>& it);

template <typename T>
circular_queue_it_t<T>& operator++(circular_queue_it_t<T>& it);

template <typename T>
circular_queue_it_t<T>& operator--(circular_queue_it_t<T>& it);

template <typename T>
bool cq_init(circular_queue_t<T>* cq, allocator_t* allocator);

template <typename T>
void cq_destroy(circular_queue_t<T>* cq);

template <typename T>
circular_queue_it_t<T> begin(const circular_queue_t<T>& cq);

template <typename T>
circular_queue_it_t<T> end(const circular_queue_t<T>& cq);

template <typename T, typename... Args>
void cq_append(circular_queue_t<T>* cq, Args... args);

template <typename T>
T cq_pop(circular_queue_t<T>* cq);
