//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/mono_time.hpp"

#include <time.h>

bool mtime_init() { return true; }

mono_time_t mtime_now() {
  timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  return ts.tv_sec * 1000000000 + ts.tv_nsec;
}

mono_time_t s_to_mtime(double s) { return s * 1000000000.0; }

double mtime_to_s(mono_time_t t) { return t / 1000000000.0; }

double mtime_to_ms(mono_time_t t) { return t / 1000000.0; }

double mtime_to_us(mono_time_t t) { return t / 1000.0; }
