//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/linear_allocator.hpp"

#include "core/allocator_internal.hpp"
#include "core/log.hpp"
#include <stdlib.h>

struct la_page_t {
  size_t size;
  la_page_t* prev;
};

namespace {

size_t get_current_page_remaning_size(const linear_allocator_t* la) {
  return la->current_page->size - (la->top - (uint8_t*)(la->current_page));
}

void la_destroy(allocator_t* allocator) {
  linear_allocator_t* la = (linear_allocator_t*)allocator;
  la_page_t* page = la->current_page;
  while (page != (la_page_t*)&(la->stack_page[0])) {
    la_page_t* prev = page->prev;
    free(page);
    page = prev;
  }
}

void* la_aligned_alloc(allocator_t* allocator,
                       uint32_t size,
                       uint32_t alignment) {
  linear_allocator_t* la = (linear_allocator_t*)allocator;
  if (!check_aligned_alloc(la->name, size, alignment))
    return nullptr;

  uint8_t* p = la->top + sizeof(allocation_header_t);
  p = align_forward(p, alignment);
  size_t real_size = (p - la->top) + size;
  if (get_current_page_remaning_size(la) < real_size) {
    size_t new_page_size =
        sizeof(la_page_t) + sizeof(allocation_header_t) + size + alignment;
    if (new_page_size < la->default_page_size)
      new_page_size = la->default_page_size;
    la_page_t* new_page = (la_page_t*)malloc(new_page_size);
    if (!new_page) {
      LOGD("Out of memory for new page for linear allocator \"%s\"", la->name);
      return nullptr;
    }
    new_page->size = new_page_size;
    new_page->prev = la->current_page;
    la->size += new_page_size;
    la->used_size += get_current_page_remaning_size(la);
    la->current_page = new_page;
    la->top = (uint8_t*)(la->current_page + 1);
    p = align_forward(la->top + sizeof(allocation_header_t), alignment);
    real_size = (p - la->top) + size;
  }
  *get_allocation_header(p) = allocation_header_t(la->top, size, alignment, p);
  la->top += real_size;
  la->used_size += real_size;
  return (void*)p;
}

void* la_realloc(allocator_t* allocator, void* p, uint32_t size) {
  linear_allocator_t* la = (linear_allocator_t*)allocator;
  if (!check_realloc(la->name, p, size))
    return nullptr;

  allocation_header_t* header = get_allocation_header(p);
  size_t old_size = header->size;
  // Not at top
  if ((uint8_t*)p + header->size != la->top) {
    void* new_p = allocator_aligned_alloc(allocator, size, header->alignment);
    memcpy(new_p, p, header->size);
    return new_p;
  }
  // Remaining space is not enough.
  if (size > get_current_page_remaning_size(la)) {
    void* new_p = allocator_aligned_alloc(allocator, size, header->alignment);
    memcpy(new_p, p, header->size);
    return new_p;
  }
  if (size == old_size)
    return p;
  la->used_size = la->used_size + size - old_size;
  header->size = size;
  la->top = (uint8_t*)p + size;
  return p;
}

void la_free(allocator_t* allocator, void* p) {
  linear_allocator_t* la = (linear_allocator_t*)allocator;
  allocation_header_t* header = get_allocation_header(p);
  if ((uint8_t*)p + header->size != la->top) {
    return;
  }
  la->top = header->start;
  la->used_size -= header->size + ((uint8_t*)p - header->start);
}

const allocator_fns_t gc_la_fns = {&la_destroy,
                                   &la_aligned_alloc,
                                   &la_realloc,
                                   &la_free};

} // namespace

bool la_init(linear_allocator_t* la, const char* name, size_t page_size) {
  la->fns = &gc_la_fns;
  la->name = name;
  la->default_page_size = page_size;
  la->current_page = (la_page_t*)&(la->stack_page[0]);
  la->current_page->size = linear_allocator_t::sc_stack_page_size;
  la->current_page->prev = nullptr;
  la->top = (uint8_t*)(la->current_page + 1);
  la->size = linear_allocator_t::sc_stack_page_size;
  la->used_size += sizeof(la_page_t);
  return true;
}
