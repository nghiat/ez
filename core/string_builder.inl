//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/dynamic_array.inl"

namespace {

template <typename T>
void* strb_template_build(string_builder_t* sb) {
  size_t total_len = 0;
  for (const void* str : sb->strings)
    total_len += str_get_len((T*)(str));

  void* result;
  result = allocator_alloc(sb->allocator, (total_len + 1) * sizeof(T));
  size_t offset = 0;
  for (const void* str : sb->strings) {
    size_t str_len = str_get_len((T*)(str));
    memcpy((T*)result + offset, str, str_len * sizeof(T));
    offset += str_len;
  }
  ((T*)result)[total_len] = 0;
  return result;
}

} // namespace
