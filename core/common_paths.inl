//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "common_paths.hpp"

namespace {

os_char_t* g_exe_path;
os_char_t* g_exe_dir;

} // namespace

const os_char_t* common_paths_get_exe_path() { return g_exe_path; }

const os_char_t* common_paths_get_exe_dir() { return g_exe_dir; }
