//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/platform.hpp"
#include <stddef.h>
#include <stdint.h>

struct allocator_t;

#ifdef OS_WIN
#  define OS_LIT(x) L##x
#  define OS_PCT "%S"
typedef wchar_t os_char_t;
#else
#  define OS_LIT(x) x
#  define OS_PCT "%s"
typedef char os_char_t;
#endif

const char* str_find_substr(const char* str, const char* substr);
char* str_find_substr(char* str, const char* substr);
const wchar_t* str_find_substr(const wchar_t* str, const wchar_t* substr);
wchar_t* str_find_substr(wchar_t* str, const wchar_t* substr);
size_t str_get_len(const char* str);
size_t str_get_len(const wchar_t* str);
char* str_copy(allocator_t* allocator, const char* src, size_t len);
wchar_t* str_copy(allocator_t* allocator, const wchar_t* src, size_t len);
bool str_compare(const char* s1, const char* s2);
bool str_compare(const wchar_t* s1, const wchar_t* s2);
