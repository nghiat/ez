//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/string.hpp"

bool common_paths_init();
const os_char_t* common_paths_get_exe_path();
const os_char_t* common_paths_get_exe_dir();
