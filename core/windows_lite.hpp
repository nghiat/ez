//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#define EZ_FORWARD_DECLARE_HANDLE(name) \
  struct name##__;                      \
  typedef struct name##__* name
EZ_FORWARD_DECLARE_HANDLE(HDC);
EZ_FORWARD_DECLARE_HANDLE(HGLRC);
EZ_FORWARD_DECLARE_HANDLE(HINSTANCE);
EZ_FORWARD_DECLARE_HANDLE(HWND);
#undef EZ_FORWARD_DECLARE_HANDLE

typedef void* HANDLE;
typedef HINSTANCE HMODULE;
