//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/dynamic_lib.hpp"

#include <Windows.h>

bool dl_open(dynamic_lib_t* dl, const char* name) {
  *dl = (void*)LoadLibraryA(name);
  return *dl;
}

void dl_close(dynamic_lib_t* dl) { FreeLibrary((HMODULE)(*dl)); }

void* dl_get_proc(dynamic_lib_t* dl, const char* name) {
  return (void*)GetProcAddress((HMODULE)(*dl), name);
}
