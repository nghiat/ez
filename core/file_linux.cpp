//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/file.hpp"

#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

bool file_open(file_t* file, const char* path, file_t::mode_e mode) {
  file->path = path;
  int flags = 0;
  if (mode & file_t::MODE_READ)
    flags |= O_RDONLY;
  if (mode & file_t::MODE_WRITE)
    flags |= O_RDWR | O_CREAT | O_TRUNC;
  if (mode & file_t::MODE_APPEND)
    flags |= O_APPEND | O_RDWR;
  int modes = S_IRWXU;
  file->file = open(file->path, flags, modes);
  return file_is_valid(file);
}

void file_close(file_t* file) {
  if (!close(file->file)) {
    file->file = -1;
  }
}

void file_delete(file_t* file) { file_delete_path(file->path); }

void file_delete_path(const char* path) { unlink(path); }

void file_read(file_t* file, void* buffer, size_t size) {
  read(file->file, buffer, size);
}

void file_write(file_t* file, size_t size, void* o_buffer) {
  write(file->file, o_buffer, size);
}

void file_seek(file_t* file, file_t::from_e from, size_t distance) {
  int whence;
  switch (from) {
  case file_t::FROM_BEGIN:
    whence = SEEK_SET;
    break;
  case file_t::FROM_CURRENT:
    whence = SEEK_CUR;
    break;
  case file_t::FROM_END:
    whence = SEEK_END;
    break;
  }
  lseek(file->file, distance, whence);
}

bool file_is_valid(const file_t* file) { return file->file != -1; }

size_t file_get_size(const file_t* file) {
  struct stat st;
  stat(file->path, &st);
  return st.st_size;
}
