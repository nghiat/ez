//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/bit_stream.hpp"

bool bs_init(bit_stream_t* bs, uint8_t* data) {
  bs->data = data;
  return true;
}

uint64_t bs_read_lsb(bit_stream_t* bs, size_t num_of_bits) {
  uint64_t result = 0;
  size_t temp_idx = bs->index;
  for (size_t i = 0; i < num_of_bits; ++i, temp_idx++) {
    size_t bytes_num = temp_idx / 8;
    size_t bit_in_byte = temp_idx % 8;
    result |= ((bs->data[bytes_num] >> bit_in_byte) & 1) << i;
  }
  return result;
}

uint64_t bs_read_msb(bit_stream_t* bs, size_t num_of_bits) {
  uint64_t result = 0;
  size_t temp_idx = bs->index;
  for (size_t i = 0; i < num_of_bits; ++i, temp_idx++) {
    size_t bytes_num = temp_idx / 8;
    size_t bit_in_byte = temp_idx % 8;
    result |= ((bs->data[bytes_num] >> bit_in_byte) & 1)
              << (num_of_bits - 1 - i);
  }
  return result;
}

void bs_skip(bit_stream_t* bs, size_t num_of_bits) { bs->index += num_of_bits; }

uint64_t bs_consume_lsb(bit_stream_t* bs, size_t num_of_bits) {
  uint64_t result = bs_read_lsb(bs, num_of_bits);
  bs_skip(bs, num_of_bits);
  return result;
}

uint64_t bs_consume_msb(bit_stream_t* bs, size_t num_of_bits) {
  uint64_t result = bs_read_msb(bs, num_of_bits);
  bs_skip(bs, num_of_bits);
  return result;
}
