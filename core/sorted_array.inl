//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/sorted_array.hpp"

#include "core/allocator.hpp"
#include "core/dynamic_array.inl"
#include "core/pair.hpp"
#include <stdlib.h>

namespace {

// Returns true and |key| should be inserted before |o_it|.
// Returns false and |o_it| is the iterator contains |key|.
template <typename K, typename D, typename G>
bool sa_find_it_to_insert(const sorted_array_t<K, D, G>* sa,
                          const K& key,
                          sorted_array_it_t<K, D, G>* o_it) {
  using it_t = sorted_array_it_t<K, D, G>;
  it_t begin_it = begin(*sa);
  it_t end_it = end(*sa);
  while (begin_it < end_it) {
    it_t it = begin_it + (end_it - begin_it) / 2;
    if (G()(*it) == key) {
      *o_it = it;
      return false;
    } else if (G()(*it) < key) {
      begin_it = it + 1;
    } else {
      end_it = it;
    }
  }
  *o_it = begin_it;
  return true;
}

} // namespace

template <typename K, typename D, typename G>
bool sa_init(sorted_array_t<K, D, G>* sa, allocator_t* allocator) {
  return da_init(&sa->array, allocator);
}

template <typename K, typename D, typename G>
void sa_destroy(sorted_array_t<K, D, G>* sa) {
  da_destroy(&sa->array);
}

template <typename K, typename D, typename G>
void sa_reserve(sorted_array_t<K, D, G>* sa, int num) {
  da_reserve(&sa->array, num);
}

template <typename K, typename D, typename G>
size_t sa_length(const sorted_array_t<K, D, G>* sa) {
  return sa->array.length;
}

template <typename K, typename D, typename G>
sorted_array_it_t<K, D, G> begin(const sorted_array_t<K, D, G>& sa) {
  return begin(sa.array);
}

template <typename K, typename D, typename G>
sorted_array_it_t<K, D, G> end(const sorted_array_t<K, D, G>& sa) {
  return end(sa.array);
}

template <typename K, typename D, typename G>
void sa_remove_it(sorted_array_t<K, D, G>* sa, sorted_array_it_t<K, D, G> it) {
  da_remove_it(&sa->array, it);
}

template <typename K, typename D, typename G>
void sa_remove_key(sorted_array_t<K, D, G>* sa, const K& key) {
  sa_remove_it(sa, sa_find(sa, key));
}

template <typename K, typename D, typename G>
pair_t<sorted_array_it_t<K, D, G>, bool>
sa_insert_or_get(sorted_array_t<K, D, G>* sa, const K& key) {
  using it_t = sorted_array_it_t<K, D, G>;
  it_t it;
  if (!sa_find_it_to_insert(sa, key, &it))
    return pair_t<it_t, bool>{it, false};
  size_t dist_to_begin = it - begin(*sa);
  size_t dist_to_end = end(*sa) - it;
  da_extend_if_necessary(&sa->array);
  da_resize(&sa->array, sa_length(sa) + 1);
  if (dist_to_end)
    memmove(sa->array.p + dist_to_begin + 1,
            sa->array.p + dist_to_begin,
            sizeof(D) * dist_to_end);
  G()(sa->array.p[dist_to_begin]) = key;
  return pair_t<it_t, bool>{begin(sa->array) + dist_to_begin, true};
}

template <typename K, typename D, typename G>
bool operator==(const sorted_array_t<K, D, G>& lhs,
                const sorted_array_t<K, D, G>& rhs) {
  return lhs.array == rhs.array;
}

template <typename K, typename D, typename G>
bool operator!=(const sorted_array_t<K, D, G>& lhs,
                const sorted_array_t<K, D, G>& rhs) {
  return !(lhs == rhs);
}

template <typename K, typename D, typename G>
sorted_array_it_t<K, D, G> sa_find(const sorted_array_t<K, D, G>* sa,
                                   const K& key) {
  sorted_array_it_t<K, D, G> it;
  if (!sa_find_it_to_insert(sa, key, &it))
    return it;
  return end(*sa);
}
