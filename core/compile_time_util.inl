//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include <stddef.h>
#include <stdint.h>

// Get array lenght at compile time.
template <typename T, size_t Size>
constexpr size_t array_length(T (&)[Size]) {
  return Size;
}

// Compile time fourcc.FourCC
constexpr uint32_t fourcc(const char* cc) {
  return *cc | *(cc + 1) << 8 | *(cc + 2) << 16 | *(cc + 3) << 24;
}
