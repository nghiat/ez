//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/hash_table.hpp"

#include "core/algorithm/swap.inl"
#include "core/allocator.hpp"
#include "core/dynamic_array.inl"
#include "core/linear_allocator.hpp"
#include <string.h>

namespace {
// Probe starts from 1.
constexpr int g_ht_max_probe = 6;
}

template <typename K>
size_t fnv1_hash_t<K>::operator()(const K& key) {
  size_t hash = 0xcbf29ce484222325;
  for (size_t i = 0; i < sizeof(K); ++i) {
    hash = hash * 1099511628211;
    hash = hash ^ *((uint8_t*)&key + i);
  }
  return hash;
}

template <typename K, typename H>
void ht_it_init(hash_table_it_t<K, H>* it, hash_table_t<K, H>* ht, int idx) {
  it->ht = ht;
  it->idx = idx;
  auto key_it = begin(*ht->keys) + idx;
  auto key_end = end(*ht->keys);
  while (key_it != key_end && key_it->probe) {
    ++key_it;
    ++it->idx;
  }
}

template <typename K, typename H>
hash_table_it_t<K, H>& operator++(hash_table_it_t<K, H>& it) {
  ht_it_init(&it, it.ht, ++it->idx);
  return it;
}

template <typename K, typename H>
bool operator==(const hash_table_it_t<K, H>& lhs,
                const hash_table_it_t<K, H>& rhs) {
  return lhs.ht == rhs.ht && lhs.idx == rhs.idx;
}

template <typename K, typename H>
bool operator!=(const hash_table_it_t<K, H>& lhs,
                const hash_table_it_t<K, H>& rhs) {
  return !(lhs == rhs);
}

// template <typename K, typename H>
// D& operator*(hash_table_it_t<K, H>& it) {
//   return *(it.data_it);
// }

template <typename K, typename H>
bool ht_init(hash_table_t<K, H>* ht, allocator_t* allocator) {
  ht->length = 0;
  return da_init(&ht->keys, allocator);
}

template <typename K, typename H>
void ht_destroy(hash_table_t<K, H>* ht) {
  da_destroy(&ht->keys);
}

template <typename K, typename H>
hash_table_it_t<K, H> begin(const hash_table_t<K, H>& ht) {
  hash_table_it_t<K, H> it;
  ht_it_init(&it, &ht, 0);
  return it;
}

template <typename K, typename H>
hash_table_it_t<K, H> end(const hash_table_t<K, H>& ht) {
  return hash_table_it_t<K, H>{&ht, ht.keys.length};
}

template <typename K, typename H>
hash_table_it_t<K, H> ht_find(hash_table_t<K, H>* ht, const K& key) {
  if (ht->keys.length) {
    const size_t c_idx = H()(key) % ht->keys.length;
    size_t idx = c_idx;
    int count = 0;
    do {
      ht_key_t<K> curr_k = ht->keys.p[idx];
      if (curr_k.probe && curr_k.key == key) {
        return hash_table_it_t<K, H>{ht, idx};
      }
      idx = (++idx) % ht->keys.length;
      ++count;
    } while (idx != c_idx && count < g_ht_max_probe);
  }
  return end(*ht);
}

template <typename K, typename H>
void ht_rehash(hash_table_t<K, H>* ht) {
  size_t bucket_count = ht->keys.length;
  constexpr int c_rehashed = 10000;
  constexpr int c_to_be_rehash = 20000;
  static_assert(g_ht_max_probe < c_rehashed);
  for (size_t i = 0; i < bucket_count; ++i) {
    if (ht->keys.p[i].probe)
      ht->keys.p[i].probe += c_to_be_rehash;
  }
  for (size_t i = 0; i < bucket_count; ++i) {
    ht_key_t<K>& curr_k = ht->keys.p[i];
    if (curr_k.probe > c_to_be_rehash) {
      curr_k.probe = 1;
      size_t idx = curr_k.hash % bucket_count;
      while (ht->keys.p[idx].probe &&
             ht->keys.p[idx].probe < c_to_be_rehash) {
        ht_key_t<K>& target_k = ht->keys.p[idx];
        int32_t actual_probe = target_k.probe > c_rehashed
                                   ? target_k.probe - c_rehashed
                                   : target_k.probe;
        if (curr_k.probe > actual_probe) {
          algo_swap(curr_k, target_k);
          if (curr_k.probe != actual_probe) {
            target_k.probe += c_rehashed;
            curr_k.probe = actual_probe;
          }
        }
        ++curr_k.probe;
        idx = (++idx) % bucket_count;
      }
      ht_key_t<K>& target_k = ht->keys.p[idx];
      if (idx > i) {
        curr_k.probe += c_rehashed;
        --i;
      }
      algo_swap(curr_k, target_k);
    } else if (curr_k.probe > c_rehashed) {
      curr_k.probe -= c_rehashed;
    }
  }
}

template <typename K, typename H>
void ht_reserve(hash_table_t<K, H>* ht, int num) {
  int old_capacity = ht->keys.length;
  size_t new_capacity = 3 * num / 2;
  const size_t initial_capacity = 4;
  if (new_capacity < initial_capacity)
    new_capacity = initial_capacity;
  da_resize(&ht->keys, new_capacity);
  memset(begin(ht->keys) + old_capacity,
         0,
         (new_capacity - old_capacity) * sizeof(ht_key_t<K>));
  if (ht->length)
    ht_rehash(ht);
}

template <typename K, typename H>
void ht_reserve_if_necessary(hash_table_t<K, H>* ht, int num) {
  int old_capacity = ht->keys.length;
  if (5 * num / 4 < old_capacity)
    return;
  ht_reserve_if_necessary(ht, num);
}

template <typename K, typename H>
pair_t<hash_table_it_t<K, H>, bool>
ht_insert_or_get(hash_table_t<K, H>* ht, const K& key) {
  // TODO: reuse hash
  hash_table_it_t<K, H> it = ht_find(ht, key);
  if (it != end(*ht))
    return pair_t<hash_table_it_t<K, H>, bool>{it, false};
  ht_reserve_if_necessary(ht, ht->length + 1);
  ht_key_t<K> new_k{H()(key), 1, key};
  size_t idx = new_k.hash % ht->keys.length;
  while (ht->keys.p[idx].probe) {
    if (new_k.probe > ht->keys.p[idx].probe)
      algo_swap(new_k, ht->keys.p[idx]);
    ++new_k.probe;
    if (new_k.probe > g_ht_max_probe) {
      ht_reserve(ht, ht->keys.length);
      idx = new_k.hash % ht->keys.length;
      new_k.probe = 1;
    }
    idx = (++idx) % ht->keys.length;
  }
  ht->keys.p[idx] = new_k;
  ++ht->length;
  it.idx = idx;
  return pair_t<hash_table_it_t<K, H>, bool>{it, true};
}

template <typename K, typename H>
void ht_remove_it(hash_table_t<K, H>* ht,
                  hash_table_it_t<K, H> it) {
  if (it.idx > ht.keys.length) {
    DLOGW("Can't erase invalid iterator");
    return;
  }
  it.probe = 0;
  --ht->length;
}

template <typename K, typename H>
void ht_remove_key(hash_table_t<K, H>* ht, const K& key) {
  ht_remove_it(ht, ht_find(ht, key));
}

template <typename K, typename H>
bool operator==(const hash_table_t<K, H>& lhs,
                const hash_table_t<K, H>& rhs) {
  if (lhs.length != rhs.length)
    return false;
  hash_table_it_t<K, H> rhs_end_it = end(rhs);
  for (auto lhs_it = begin(lhs); lhs_it != end(lhs); ++lhs_it) {
    auto rhs_it = ht_find(&rhs, G()(*lhs_it));
    if (rhs_it == rhs_end_it || *lhs_it != *rhs_it)
      return false;
  }
  return true;
}

template <typename K, typename H>
bool operator!=(const hash_table_t<K, H>& lhs,
                const hash_table_t<K, H>& rhs) {
  return !(lhs == rhs);
}

// template <typename K, typename H>
// D* hash_table_it_t<K, H>::operator->() {
//   return data_it;
// }
