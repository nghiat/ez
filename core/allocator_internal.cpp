//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/allocator_internal.hpp"

#include "core/log.hpp"

allocation_header_t* get_allocation_header(void* p) {
  return (allocation_header_t*)p - 1;
}

uint8_t* align_forward(uint8_t* p, uint32_t alignment) {
  if (alignment == 1)
    return p;
  return (uint8_t*)(((size_t)p + alignment - 1) & ~(size_t)(alignment - 1));
}

bool check_aligned_alloc(const char* allocator_name,
                         uint32_t size,
                         uint32_t alignment) {
  if (!size || !alignment) {
    LOGD("Allocator \"%s\" can't alloc with 0 size or 0 alignment",
         allocator_name);
    return false;
  }

  // alignment has to be power of two.
  if (alignment & (alignment - 1)) {
    LOGD("Allocator \"%s\" can't alloc with non-power-of-two alignment",
         allocator_name);
    return false;
  }
  return true;
}

bool check_realloc(const char* allocator_name, void* p, uint32_t size) {
  if (!size || !p) {
    LOGD("Allocator \"%s\" can't realloc NULL pointer or 0 size",
         allocator_name);
    return false;
  }

#ifdef BUILD_DEBUG
  allocation_header_t* header = get_allocation_header(p);
  if (header->p != p) {
    LOGD("Allocator \"%s\" can't realloc pointer with invalid header",
         allocator_name);
    return false;
  }
#endif
  return true;
}
