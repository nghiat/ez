//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/mono_time.hpp"

#include <Windows.h>

namespace {
LARGE_INTEGER g_performance_freq;
}

bool mtime_init() { return QueryPerformanceFrequency(&g_performance_freq); }

mono_time_t s_to_mtime(double s) { return s * g_performance_freq.QuadPart; }

mono_time_t mtime_now() {
  LARGE_INTEGER pc;
  QueryPerformanceCounter(&pc);
  return pc.QuadPart;
}

double mtime_to_s(mono_time_t t) {
  return (double)t / g_performance_freq.QuadPart;
}

double mtime_to_ms(mono_time_t t) { return mtime_to_s(t) * 1000; }

double mtime_to_us(mono_time_t t) { return mtime_to_s(t) * 1000000; }
