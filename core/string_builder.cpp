//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/string_builder.hpp"
#include "core/string_builder.inl"

#include "core/allocator.hpp"
#include "core/core_allocators.hpp"
#include "core/dynamic_array.inl"
#include <string.h>

bool strb_init(string_builder_t* sb, allocator_t* allocator, size_t char_size) {
  la_init(&sb->temp_allocator, "strb_t_temp_allocator");
  sb->allocator = allocator;
  sb->char_size = char_size;
  return da_init(&sb->strings, &sb->temp_allocator);
}

void strb_destroy(string_builder_t* sb) {
  allocator_destroy(&sb->temp_allocator);
}

void strb_add_string(string_builder_t* sb, const void* str) {
  da_append(&sb->strings, str);
}

void* strb_build(string_builder_t* sb) {
  if (sb->char_size == 1)
    return strb_template_build<char>(sb);
  if (sb->char_size == 2)
    return strb_template_build<wchar_t>(sb);
  return nullptr;
}
