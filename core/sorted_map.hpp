//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/pair.hpp"
#include "core/sorted_array.hpp"

template <typename Key, typename Value>
using sorted_map_t =
    sorted_array_t<Key, pair_t<Key, Value>, get_key_t<Key, Value>>;

template <typename Key, typename Value>
using sorted_map_it_t =
    sorted_array_it_t<Key, pair_t<Key, Value>, get_key_t<Key, Value>>;

// The bool in returned value is false when |key| exists, true otherwise.
template <typename K, typename V>
pair_t<sorted_map_it_t<K, V>, bool>
sa_insert_or_get(sorted_map_t<K, V>* sm, const K& key, const V& value);

// The bool in returned value is false when |key| exists, true otherwise.
template <typename K, typename V>
pair_t<sorted_map_it_t<K, V>, bool>
sa_insert_or_set(sorted_map_t<K, V>* sm, const K& key, const V& value);
