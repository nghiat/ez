//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include <stdint.h>

struct allocator_t;
struct dfa_state_t;

enum class regex_op_e : uint16_t {
  APPEND = 256,
  ALTERNATION,
  STAR,
  PLUS,
};

union regex_node_val_t {
  char c;
  regex_op_e op;
};

struct regex_tree_node_t {
  regex_node_val_t v;
  regex_tree_node_t* l;
  regex_tree_node_t* r;
};

struct regex_t {
  dfa_state_t* first_state = nullptr;
};

bool regex_init(regex_t* re, allocator_t* allocator, const char* str);
void regex_destroy(regex_t* re);
bool regex_match(regex_t* re, const char* str);

regex_tree_node_t* regex_build_tree(allocator_t* allocator, const char* str);
