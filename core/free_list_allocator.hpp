//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include "allocator.hpp"

struct freeblock_t;

/// An allocator that keeps a linked list of blocks of available spaces.
/// When you request an allocation, it finds the smallest block that can keeps
/// the size of the allcation then shrinks that block. When you request a
/// freeation, it creates a new blocks and merges with nearby blocks if
/// they are contiguous.
struct free_list_allocator_t : allocator_t {
  uint8_t* start = nullptr;
  freeblock_t* first_block = nullptr;
};

bool fla_init(free_list_allocator_t* fla, const char* name, size_t size);
