//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/dynamic_array.hpp"
#include "core/linear_allocator.hpp"
#include "core/string.hpp"

struct string_builder_t {
  linear_allocator_t temp_allocator;
  allocator_t* allocator;
  dynamic_array_t<const void*> strings;
  size_t char_size;
};

bool strb_init(string_builder_t* sb, allocator_t* allocator, size_t char_size);
void strb_destroy(string_builder_t* sb);
void strb_add_string(string_builder_t* sb, const void* str);
void* strb_build(string_builder_t* sb);
