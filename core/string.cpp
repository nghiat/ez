//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/string.hpp"

#include "core/allocator.hpp"
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

namespace {

void* mem_copy(allocator_t* allocator, const void* src, size_t size) {
  void* des = allocator_alloc(allocator, size);
  memcpy(des, src, size);
  return des;
}

} // namespace

const char* str_find_substr(const char* str, const char* substr) {
  return strstr(str, substr);
}

char* str_find_substr(char* str, const char* substr) {
  return strstr(str, substr);
}

const wchar_t* str_find_substr(const wchar_t* str, const wchar_t* substr) {
  return wcsstr(str, substr);
}

wchar_t* str_find_substr(wchar_t* str, const wchar_t* substr) {
  return wcsstr(str, substr);
}

size_t str_get_len(const char* str) { return strlen(str); }

size_t str_get_len(const wchar_t* str) { return wcslen(str); }

char* str_copy(allocator_t* allocator, const char* src, size_t len) {
  return (char*)mem_copy(allocator, src, len);
}

wchar_t* str_copy(allocator_t* allocator, const wchar_t* src, size_t len) {
  return (wchar_t*)mem_copy(allocator, src, len * sizeof(wchar_t));
}

bool str_compare(const char* s1, const char* s2) { return !strcmp(s1, s2); }

bool str_compare(const wchar_t* s1, const wchar_t* s2) {
  return !wcscmp(s1, s2);
}
