//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/regex.hpp"

#include "core/algorithm/find.inl"
#include "core/algorithm/sort.inl"
#include "core/allocator.hpp"
#include "core/circular_queue.inl"
#include "core/core_allocators.hpp"
#include "core/dynamic_array.inl"
#include "core/free_list_allocator.hpp"
#include "core/hash_map.inl"
#include "core/hash_set.inl"
#include "core/linear_allocator.hpp"
#include "core/log.hpp"
#include "core/mono_time.hpp"
#include "core/sorted_map.inl"
#include "core/sorted_set.inl"
#include <ctype.h>

struct dfa_state_t {
  sorted_map_t<char, dfa_state_t*> s;
  bool is_final = false;
};

namespace {

struct nda_state_t {
  char c[2];
  // is_epsilon
  bool is_e[2] = {false, false};
  nda_state_t* s[2] = {nullptr, nullptr};
};

struct nda_t {
  nda_state_t* first;
  nda_state_t* last;
};

regex_tree_node_t* create_char_node(allocator_t* allocator,
                                    char c,
                                    regex_tree_node_t* l,
                                    regex_tree_node_t* r) {
  regex_tree_node_t* n =
      (regex_tree_node_t*)allocator_alloc(allocator, sizeof(regex_tree_node_t));
  n->v.c = c;
  n->l = l;
  n->r = r;
  return n;
}

regex_tree_node_t* create_op_node(allocator_t* allocator,
                                  regex_op_e op,
                                  regex_tree_node_t* l,
                                  regex_tree_node_t* r) {
  regex_tree_node_t* n =
      (regex_tree_node_t*)allocator_alloc(allocator, sizeof(regex_tree_node_t));
  n->v.op = op;
  n->l = l;
  n->r = r;
  return n;
}

regex_tree_node_t*
regex_build_tree_inside_square_bracket(allocator_t* allocator,
                                       const char** str) {
  regex_tree_node_t* root = nullptr;
  regex_tree_node_t** curr = &root;
  for (char cc = **str; cc != ']'; ++*str, cc = **str) {
    if (isalpha(cc)) {
      *curr = create_char_node(allocator, cc, nullptr, nullptr);
    } else {
      LOGF("Invalid char %c", cc);
    }
    char nc = *(*str + 1);
    if (nc == '-') {
      *str = *str + 2;
      nc = **str;
      for (char c = cc + 1; c <= nc; ++c) {
        root = create_op_node(allocator,
                              regex_op_e::ALTERNATION,
                              root,
                              create_char_node(allocator, c, nullptr, nullptr));
      }
      curr = &root;
      nc = *(*str + 1);
    }
    if (nc != ']') {
      root = create_op_node(allocator, regex_op_e::ALTERNATION, root, nullptr);
      curr = &(root->r);
    }
  }
  return root;
}

regex_tree_node_t* regex_build_tree_internal(allocator_t* allocator,
                                             const char** str) {
  char cc = **str;
  if (!cc)
    return nullptr;
  regex_tree_node_t* root = nullptr;
  regex_tree_node_t** curr = &root;
  while (cc) {
    switch (cc) {
    case '(':
      ++*str;
      *curr = regex_build_tree_internal(allocator, str);
      break;
    case '*':
      *curr = create_op_node(allocator, regex_op_e::STAR, *curr, nullptr);
      break;
    case '+':
      *curr = create_op_node(allocator, regex_op_e::PLUS, *curr, nullptr);
      break;
    case ')':
      // Handled below
      --*str;
      break;
    case '[':
      ++*str;
      *curr = regex_build_tree_inside_square_bracket(allocator, str);
      break;
    default:
      if (isalpha(cc)) {
        *curr = create_char_node(allocator, cc, nullptr, nullptr);
      } else {
        CHECKF(false, "aaaa");
      }
    }
    cc = *(++*str);
    if (cc) {
      switch (cc) {
      case '|':
        ++*str;
        root = create_op_node(allocator,
                              regex_op_e::ALTERNATION,
                              root,
                              regex_build_tree_internal(allocator, str));
        curr = &root;
        break;
      case '*':
      case '+':
        // Handled above
        break;
      case ')':
        return root;
      default:
        root = create_op_node(allocator, regex_op_e::APPEND, root, nullptr);
        curr = &(root->r);
      }
      cc = **str;
    }
  }
  return root;
}

nda_t build_nda(allocator_t* allocator,
                regex_tree_node_t* n,
                dynamic_array_t<nda_state_t*>* o_all_ndas) {
  /*
                            e
     l.first-...->l.last--->r.first-...->r.last
   */
  switch (n->v.op) {
  case regex_op_e::APPEND: {
    nda_t l = build_nda(allocator, n->l, o_all_ndas);
    nda_t r = build_nda(allocator, n->r, o_all_ndas);
    l.last->is_e[0] = true;
    l.last->s[0] = r.first;
    return {l.first, r.last};
  }

  case regex_op_e::ALTERNATION: {
    /*
           l.first-...->l.last
          /                      \
         /                        \
      s1-                          -s2
         \                        /
          \                      /
           r.first-...->r.last
     */
    nda_t l = build_nda(allocator, n->l, o_all_ndas);
    nda_t r = build_nda(allocator, n->r, o_all_ndas);
    nda_state_t* s1 = allocator_new<nda_state_t>(allocator);
    s1->is_e[0] = true;
    s1->s[0] = l.first;
    s1->is_e[1] = true;
    s1->s[1] = r.first;
    nda_state_t* s2 = allocator_new<nda_state_t>(allocator);
    l.last->is_e[0] = true;
    l.last->s[0] = s2;
    r.last->is_e[0] = true;
    r.last->s[0] = s2;
    da_append(o_all_ndas, s1);
    da_append(o_all_ndas, s2);
    return {s1, s2};
  }
  case regex_op_e::STAR: {
    /*
                ________________
        e       *              |  e
     s1--->l.first-...->l.last--->s2
     |_______________________________*
                      e
     */
    nda_t l = build_nda(allocator, n->l, o_all_ndas);
    l.last->is_e[0] = true;
    l.last->s[0] = l.first;
    nda_state_t* s1 = allocator_new<nda_state_t>(allocator);
    s1->is_e[0] = true;
    s1->s[0] = l.first;
    nda_state_t* s2 = allocator_new<nda_state_t>(allocator);
    l.last->is_e[1] = true;
    l.last->s[1] = s2;
    s1->is_e[1] = true;
    s1->s[1] = s2;
    da_append(o_all_ndas, s1);
    da_append(o_all_ndas, s2);
    return {s1, s2};
  }
  case regex_op_e::PLUS: {
    /*
              ________e________
        e     *               |
     s1--->l.first-...>l.last--->s2
                              e
     */
    nda_t l = build_nda(allocator, n->l, o_all_ndas);
    l.last->is_e[0] = true;
    l.last->s[0] = l.first;
    nda_state_t* s1 = allocator_new<nda_state_t>(allocator);
    s1->is_e[0] = true;
    s1->s[0] = l.first;
    nda_state_t* s2 = allocator_new<nda_state_t>(allocator);
    l.last->is_e[1] = true;
    l.last->s[1] = s2;
    da_append(o_all_ndas, s1);
    da_append(o_all_ndas, s2);
    return {s1, s2};
  }

  default: {
    /*
        c
     s1--->s2
     */
    if (!n->l && !n->r) {
      nda_state_t* s1 = allocator_new<nda_state_t>(allocator);
      s1->c[0] = n->v.c;
      nda_state_t* s2 = allocator_new<nda_state_t>(allocator);
      s1->s[0] = s2;
      da_append(o_all_ndas, s1);
      da_append(o_all_ndas, s2);
      return {s1, s2};
    }
    CHECKF(false, "Invalid node");
  }
  }
  return {nullptr, nullptr};
}

} // namespace

bool regex_init(regex_t* re, allocator_t* allocator, const char* str) {
  linear_allocator_t build_dfa_allocator;
  la_init(&build_dfa_allocator, "build_dfa_allocator");
  sorted_map_t<nda_state_t*, sorted_set_t<nda_state_t*>> e_closure_map;
  sa_init(&e_closure_map, &build_dfa_allocator);
  nda_t nda;
  mono_time_t t = mtime_now();
  size_t nda_count = 0;
  {
    linear_allocator_t build_nda_and_closure_allocator;
    la_init(&build_nda_and_closure_allocator, "nda_temp_allocator");
    regex_tree_node_t* root =
        regex_build_tree(&build_nda_and_closure_allocator, str);
    dynamic_array_t<nda_state_t*> all_ndas;
    da_init(&all_ndas, &build_nda_and_closure_allocator);
    nda = build_nda(&build_dfa_allocator, root, &all_ndas);
    nda_count = all_ndas.length;
    LOGI("build nfa: %f ms", mtime_to_ms(mtime_now() - t));
    t = mtime_now();
    int max_closure_length = 0;
    // Calcuate e-closure
    for (nda_state_t* s : all_ndas) {
      circular_queue_t<nda_state_t*> new_e_states_queue;
      cq_init(&new_e_states_queue, &build_nda_and_closure_allocator);
      cq_append(&new_e_states_queue, s);
      sorted_set_t<nda_state_t*> e_closure_of_s;
      sa_init(&e_closure_of_s, &build_dfa_allocator);
      sa_reserve(&e_closure_of_s, max_closure_length);
      sa_insert_or_get(&e_closure_of_s, s);
      while (new_e_states_queue.length) {
        nda_state_t* e_state = cq_pop(&new_e_states_queue);
        if (e_state->is_e[0] &&
            sa_insert_or_get(&e_closure_of_s, e_state->s[0]).second)
          cq_append(&new_e_states_queue, e_state->s[0]);
        if (e_state->is_e[1] &&
            sa_insert_or_get(&e_closure_of_s, e_state->s[1]).second)
          cq_append(&new_e_states_queue, e_state->s[1]);
      }
      if (max_closure_length < sa_length(&e_closure_of_s))
        max_closure_length = sa_length(&e_closure_of_s);
      sa_insert_or_get(&e_closure_map, s, e_closure_of_s);
      cq_destroy(&new_e_states_queue);
    }
    allocator_destroy(&build_nda_and_closure_allocator);
  }
  LOGI("build_e_closure: %f ms", mtime_to_ms(mtime_now() - t));
  t = mtime_now();
  linear_allocator_t minimize_dfa_allocator;
  la_init(&minimize_dfa_allocator, "minimize_dfa_allocator");
  sorted_set_t<dfa_state_t*> all_dfas;
  sa_init(&all_dfas, &minimize_dfa_allocator);
  sa_reserve(&all_dfas, nda_count);
  re->first_state = allocator_new<dfa_state_t>(&minimize_dfa_allocator);
  sa_insert_or_get(&all_dfas, re->first_state);
  sa_init(&re->first_state->s, allocator);
  sa_insert_or_get(&all_dfas, re->first_state);
  // Build DFA
  {
    int max_nda_in_set = 0;
    auto* first_ndas =
        allocator_new<sorted_set_t<nda_state_t*>>(&build_dfa_allocator);
    sa_init(first_ndas, &build_dfa_allocator);
    sa_reserve(first_ndas, 100);
    for (nda_state_t* s : sa_find(&e_closure_map, nda.first)->value)
      sa_insert_or_get(first_ndas, s);
    if (max_nda_in_set < sa_length(first_ndas))
      max_nda_in_set = sa_length(first_ndas);
    using dfa_and_ndas_t = pair_t<dfa_state_t*, sorted_set_t<nda_state_t*>*>;
    circular_queue_t<dfa_and_ndas_t> updated_pair_queue;
    cq_init(&updated_pair_queue, &build_dfa_allocator);
    cq_append(&updated_pair_queue, dfa_and_ndas_t{re->first_state, first_ndas});
    using nda_and_char_t = pair_t<nda_state_t*, char>;
    hash_map_t<nda_and_char_t, dfa_state_t*> nda_char_to_dfa;
    ht_init(&nda_char_to_dfa, &build_dfa_allocator);
    // |nda_count / 3| is optimal number after testing multiple value.
    ht_reserve(&nda_char_to_dfa, nda_count / 3);
    hash_map_t<dfa_state_t*, sorted_set_t<nda_state_t*>*> dfa_to_ndas;
    ht_init(&dfa_to_ndas, &build_dfa_allocator);
    ht_reserve(&dfa_to_ndas, nda_count);
    ht_insert_or_get(&dfa_to_ndas, re->first_state, first_ndas);
    while (updated_pair_queue.length) {
      dfa_and_ndas_t updated_pair = cq_pop(&updated_pair_queue);
      dfa_state_t* dfa = updated_pair.first;
      sorted_set_t<nda_state_t*>* ndas = updated_pair.second;
      hash_set_t<char> processed_chars;
      ht_init(&processed_chars, &build_dfa_allocator);
      ht_reserve(&processed_chars, 256);
      for (nda_state_t* s : *ndas) {
        for (int i = 0; i < 2; ++i) {
          if (s->s[i] && !s->is_e[i]) {
            char c = s->c[i];
            if (ht_find(&processed_chars, c) != end(processed_chars))
              continue;
            dfa_state_t* dfa_from_ndas_along_c = nullptr;
            sorted_set_t<nda_state_t*>* ndas_of_dfa = nullptr;
            for (nda_state_t* nda_has_c : *ndas) {
              for (int j = 0; j < 2; ++j) {
                if (nda_has_c->s[j] && !nda_has_c->is_e[j] &&
                    nda_has_c->c[j] == c) {
                  auto it =
                      ht_find(&nda_char_to_dfa, nda_and_char_t{nda_has_c, c});
                  if (it != end(nda_char_to_dfa)) {
                    dfa_from_ndas_along_c = it->value;
                    ndas_of_dfa =
                        ht_find(&dfa_to_ndas, dfa_from_ndas_along_c)->value;
                    break;
                  }
                }
              }
              if (dfa_from_ndas_along_c)
                break;
            }
            if (!dfa_from_ndas_along_c) {
              dfa_from_ndas_along_c =
                  allocator_new<dfa_state_t>(&minimize_dfa_allocator);
              sa_init(&dfa_from_ndas_along_c->s, allocator);
              sa_insert_or_get(&all_dfas, dfa_from_ndas_along_c);
              ndas_of_dfa = allocator_new<sorted_set_t<nda_state_t*>>(
                  &build_dfa_allocator);
              sa_init(ndas_of_dfa, &build_dfa_allocator);
              sa_reserve(ndas_of_dfa, max_nda_in_set);
              ht_insert_or_get(
                  &dfa_to_ndas, dfa_from_ndas_along_c, ndas_of_dfa);
            }
            size_t old_set_len = ndas_of_dfa->array.length;
            for (nda_state_t* nda_has_c : *ndas) {
              for (int j = 0; j < 2; ++j) {
                if (nda_has_c->s[j] && !nda_has_c->is_e[j] &&
                    nda_has_c->c[j] == c) {
                  if (ht_insert_or_get(&nda_char_to_dfa,
                                       nda_and_char_t{nda_has_c, c},
                                       dfa_from_ndas_along_c)
                          .second) {
                    for (nda_state_t* target_nda :
                         sa_find(&e_closure_map, nda_has_c->s[0])->value) {
                      sa_insert_or_get(ndas_of_dfa, target_nda);
                    }
                  }
                }
              }
            }
            if (max_nda_in_set < sa_length(first_ndas))
              max_nda_in_set = sa_length(first_ndas);
            sa_insert_or_get(&dfa->s, c, dfa_from_ndas_along_c);
            if (sa_find(ndas_of_dfa, nda.last) != end(*ndas_of_dfa))
              dfa_from_ndas_along_c->is_final = true;
            if (ndas_of_dfa->array.length != old_set_len) {
              cq_append(&updated_pair_queue,
                        pair_t<dfa_state_t*, sorted_set_t<nda_state_t*>*>{
                            dfa_from_ndas_along_c, ndas_of_dfa});
            }
            ht_insert_or_get(&processed_chars, c);
          }
        }
      }
      ht_destroy(&processed_chars);
    }
    allocator_destroy(&build_dfa_allocator);
  }
  LOGI("build_dfa: %f ms", mtime_to_ms(mtime_now() - t));
  t = mtime_now();
  // Minimize DFA
  {
    sorted_map_t<dfa_state_t*, sorted_set_t<dfa_state_t*>*> old_dfa_to_set;
    sa_init(&old_dfa_to_set, &minimize_dfa_allocator);
    sa_reserve(&old_dfa_to_set, sa_length(&all_dfas));
    auto final_dfas =
        allocator_new<sorted_set_t<dfa_state_t*>>(&minimize_dfa_allocator);
    sa_init(final_dfas, &minimize_dfa_allocator);
    sa_reserve(final_dfas, sa_length(&all_dfas));
    auto non_final_dfas =
        allocator_new<sorted_set_t<dfa_state_t*>>(&minimize_dfa_allocator);
    sa_init(non_final_dfas, &minimize_dfa_allocator);
    sa_reserve(non_final_dfas, sa_length(&all_dfas));
    for (dfa_state_t* s : all_dfas) {
      if (s->is_final) {
        da_append(&final_dfas->array, s);
      } else {
        da_append(&non_final_dfas->array, s);
      }
    }
    algo_merge_sort(final_dfas->array.p, final_dfas->array.length);
    algo_merge_sort(non_final_dfas->array.p, non_final_dfas->array.length);
    for (dfa_state_t* s : *final_dfas) {
      sa_insert_or_get(&old_dfa_to_set, s, final_dfas);
    }
    for (dfa_state_t* s : *non_final_dfas) {
      sa_insert_or_get(&old_dfa_to_set, s, non_final_dfas);
    }
    hash_map_t<sorted_set_t<dfa_state_t*>*, dfa_state_t*> set_to_minimized_dfa;
    ht_init(&set_to_minimized_dfa, &minimize_dfa_allocator);
    ht_reserve(&set_to_minimized_dfa, sa_length(&all_dfas));
    dfa_state_t* minimized_final_dfa = allocator_new<dfa_state_t>(allocator);
    sa_init(&minimized_final_dfa->s, allocator);
    ht_insert_or_get(&set_to_minimized_dfa, final_dfas, minimized_final_dfa);
    dfa_state_t* minimized_non_final_dfa = allocator_new<dfa_state_t>(allocator);
    sa_init(&minimized_non_final_dfa->s, allocator);
    ht_insert_or_get(&set_to_minimized_dfa, non_final_dfas, minimized_non_final_dfa);

    circular_queue_t<sorted_set_t<dfa_state_t*>*> split_dfa_sets_queue;
    cq_init(&split_dfa_sets_queue, &minimize_dfa_allocator);
    cq_append(&split_dfa_sets_queue, final_dfas);
    cq_append(&split_dfa_sets_queue, non_final_dfas);
    while (split_dfa_sets_queue.length) {
      sorted_set_t<dfa_state_t*>* split_set = cq_pop(&split_dfa_sets_queue);
      if (sa_length(split_set) == 1)
        continue;
      CHECKF(sa_length(split_set) != 0, "Invalid");
      dfa_state_t* first_state = *(begin(*split_set));
      hash_set_t<char> processed_chars;
      ht_init(&processed_chars, &minimize_dfa_allocator);
      ht_reserve(&processed_chars, 256);
      bool is_split = false;
      // We loop through |split_set| to find character to split it further.
      for (dfa_state_t* dfa : *split_set) {
        for (auto pair : dfa->s) {
          char c = pair.first;
          if (ht_find(&processed_chars, c) != end(processed_chars))
            continue;
          sorted_set_t<dfa_state_t*>* first_state_c_set = nullptr;
          {
            auto it = sa_find(&first_state->s, c);
            if (it != end(first_state->s))
              first_state_c_set = sa_find(&old_dfa_to_set, it->value)->value;
          }
          dynamic_array_t<dfa_state_t*> same_as_first;
          da_init(&same_as_first, &minimize_dfa_allocator);
          da_reserve(&same_as_first, sa_length(split_set));
          dynamic_array_t<dfa_state_t*> not_same_as_first;
          da_init(&not_same_as_first, &minimize_dfa_allocator);
          da_reserve(&not_same_as_first, sa_length(split_set));
          for (dfa_state_t* dfa_along_c : *split_set) {
            auto it = sa_find(&dfa_along_c->s, c);
            if (it != end(dfa_along_c->s)) {
              if (!first_state_c_set || sa_find(first_state_c_set, it->value) ==
                                            end(*first_state_c_set))
                da_append(&not_same_as_first, dfa_along_c);
              else
                da_append(&same_as_first, dfa_along_c);
            } else {
              if (!first_state_c_set)
                da_append(&same_as_first, dfa_along_c);
              else
                da_append(&not_same_as_first, dfa_along_c);
            }
          }
          if (!not_same_as_first.length) {
            da_destroy(&same_as_first);
            ht_insert_or_get(&processed_chars, c);
          } else {
            algo_merge_sort(same_as_first.p, same_as_first.length);
            algo_merge_sort(not_same_as_first.p, not_same_as_first.length);
            sorted_set_t<dfa_state_t*>* same_as_first_set =
                allocator_new<sorted_set_t<dfa_state_t*>>(
                    &minimize_dfa_allocator);
            same_as_first_set->array = same_as_first;
            sorted_set_t<dfa_state_t*>* not_same_as_first_set =
                allocator_new<sorted_set_t<dfa_state_t*>>(
                    &minimize_dfa_allocator);
            not_same_as_first_set->array = not_same_as_first;
            for (dfa_state_t* s : *same_as_first_set)
              sa_insert_or_set(&old_dfa_to_set, s, same_as_first_set);
            for (dfa_state_t* s : *not_same_as_first_set)
              sa_insert_or_set(&old_dfa_to_set, s, not_same_as_first_set);
            cq_append(&split_dfa_sets_queue, same_as_first_set);
            cq_append(&split_dfa_sets_queue, not_same_as_first_set);
            is_split = true;

            ht_remove_key(&set_to_minimized_dfa, split_set);
            dfa_state_t* same_as_first_set_dfa = allocator_new<dfa_state_t>(allocator);
            sa_init(&same_as_first_set_dfa->s, allocator);
            ht_insert_or_get(&set_to_minimized_dfa, same_as_first_set, same_as_first_set_dfa);
            dfa_state_t* not_same_as_first_set_dfa = allocator_new<dfa_state_t>(allocator);
            sa_init(&not_same_as_first_set_dfa->s, allocator);
            ht_insert_or_get(&set_to_minimized_dfa, not_same_as_first_set, not_same_as_first_set_dfa);
            sa_destroy(split_set);
            break;
          }
        }
        if (is_split)
          break;
      }
      ht_destroy(&processed_chars);
    }
    circular_queue_t<dfa_state_t*> old_dfa_queue;
    cq_init(&old_dfa_queue, &minimize_dfa_allocator);
    cq_append(&old_dfa_queue, re->first_state);
    sorted_set_t<dfa_state_t*> processed_old_dfas;
    sa_init(&processed_old_dfas, &minimize_dfa_allocator);
    sa_reserve(&processed_old_dfas, sa_length(&all_dfas));
    sa_insert_or_get(&processed_old_dfas, re->first_state);
    while (old_dfa_queue.length) {
      dfa_state_t* old_dfa = cq_pop(&old_dfa_queue);
      dfa_state_t* minimized_dfa =
          ht_find(&set_to_minimized_dfa,
                  sa_find(&old_dfa_to_set, old_dfa)->value)
              ->value;
      minimized_dfa->is_final = old_dfa->is_final;
      for (auto pair : old_dfa->s) {
        char c = pair.key;
        dfa_state_t* target_old_dfa = pair.value;
        sa_insert_or_get(&minimized_dfa->s, c).first->value =
            ht_find(&set_to_minimized_dfa,
                    sa_find(&old_dfa_to_set, target_old_dfa)->value)
                ->value;
        if (sa_find(&processed_old_dfas, target_old_dfa) ==
            end(processed_old_dfas)) {
          cq_append(&old_dfa_queue, target_old_dfa);
          sa_insert_or_get(&processed_old_dfas, target_old_dfa);
        }
      }
    }
    re->first_state = ht_find(&set_to_minimized_dfa,
                              sa_find(&old_dfa_to_set, re->first_state)->value)
                          ->value;
    allocator_destroy(&minimize_dfa_allocator);
  }
  LOGI("minimize_dfa: %f ms", mtime_to_ms(mtime_now() - t));
  return true;
}

void regex_destroy(regex_t* re) {}

bool regex_match(regex_t* re, const char* str) {
  dfa_state_t* s = re->first_state;
  while (*str) {
    auto it = sa_find(&s->s, *str);
    if (it == end(s->s))
      return false;
    s = it->value;
    ++str;
  }
  return s->is_final;
}

regex_tree_node_t* regex_build_tree(allocator_t* allocator, const char* str) {
  return regex_build_tree_internal(allocator, &str);
}
