//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include <stddef.h>

struct allocator_t;

template <typename T>
struct dynamic_array_t {
  T* p = nullptr;
  allocator_t* allocator = nullptr;
  size_t length = 0;
  size_t capacity = 0;
};

template <typename T>
using dynamic_array_it_t = T*;

template <typename T>
bool da_init(dynamic_array_t<T>* da, allocator_t* allocator);

template <typename T>
void da_destroy(dynamic_array_t<T>* da);

template <typename T>
dynamic_array_it_t<T> begin(const dynamic_array_t<T>& da);

template <typename T>
dynamic_array_it_t<T> end(const dynamic_array_t<T>& da);

template <typename T>
void da_reserve(dynamic_array_t<T>* da, size_t num);

template <typename T>
void da_resize(dynamic_array_t<T>* da, size_t num);

template <typename T>
void da_clone(dynamic_array_t<T>* da, const dynamic_array_t<T>* src);

template <typename T>
void da_clear(dynamic_array_t<T>* da);

template <typename T>
void da_remove_range(dynamic_array_t<T>* da, int pos, int length);

template <typename T>
void da_remove_at(dynamic_array_t<T>* da, size_t pos);

template <typename T>
void da_remove_it(dynamic_array_t<T>* da, dynamic_array_it_t<T> it);

template <typename T>
void da_extend_if_necessary(dynamic_array_t<T>* da);

template <typename T, typename... Args>
void da_insert_at(dynamic_array_t<T>* da, size_t index, Args&&... params);

template <typename T, typename... Args>
void da_append(dynamic_array_t<T>* da, Args&&... params);

template <typename T>
bool operator==(const dynamic_array_t<T>& lhs, const dynamic_array_t<T>& rhs);

template <typename T>
bool operator!=(const dynamic_array_t<T>& lhs, const dynamic_array_t<T>& rhs);
