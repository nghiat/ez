//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include <stddef.h>
#include <stdint.h>

// Provide methods to read bits value from a bytes array.
struct bit_stream_t {
  uint8_t* data = 0;
  size_t index = 0;
};

bool bs_init(bit_stream_t* bt, uint8_t* data);

// Get number from |num_of_bits| without moving the bit pointer.
uint64_t bs_read_lsb(bit_stream_t* bt, size_t num_of_bits);
uint64_t bs_read_msb(bit_stream_t* bt, size_t num_of_bits);

void bs_skip(bit_stream_t* bs, size_t num_of_bits);

// Same as |Read*| but also skip |num_of_bits|.
uint64_t bs_consume_lsb(bit_stream_t* bt, size_t num_of_bits);
uint64_t bs_consume_msb(bit_stream_t* bt, size_t num_of_bits);
