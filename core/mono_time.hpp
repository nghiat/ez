//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include <stdint.h>

typedef int64_t mono_time_t;

bool mtime_init();
mono_time_t mtime_now();
mono_time_t s_to_mtime(double s);
double mtime_to_s(mono_time_t t);
double mtime_to_ms(mono_time_t t);
double mtime_to_us(mono_time_t t);
