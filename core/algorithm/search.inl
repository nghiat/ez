//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include <stdint.h>
#include <string.h>

// Similar to std::search but in a separate file to avoid bloated header.
// If you want to search on POD type, use SearchFast below for faster search.
template <typename It1, typename It2>
constexpr It1 algo_search(It1 first1, It1 last1, It2 first2, It2 last2) {
  for (;; ++first1) {
    It1 it1 = first1;
    for (It2 it2 = first2;; ++it1, ++it2) {
      if (it2 == last2)
        return first1;
      if (it1 == last1)
        return last1;
      if (*it1 != *it2)
        break;
    }
  }
}

// Similar to core::Search but for POD type.
template <typename T>
constexpr const T* algo_search_fast(const T* first1,
                                    const T* last1,
                                    const T* first2,
                                    const T* last2) {
  const int64_t c_input_length = last1 - first1;
  const int64_t c_search_length = last2 - first2;
  if (c_input_length < c_search_length)
    return last1;

  const int64_t c_search_size = c_search_length * sizeof(T);
  const T* it = first1;
  while (true) {
    const T* first = (const T*)memchr(it, *first2, sizeof(T) * (last1 - it));
    if (!first)
      return last1;
    int64_t remaining_length = last1 - first;
    if (remaining_length < c_search_length)
      break;
    if (!memcmp(first, first2, c_search_size))
      return first;
    it = ++first;
  }
  return last1;
}
