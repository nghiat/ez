//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

// Similar to std::find but in a separate file to avoid bloated header.
template <typename It, typename T>
constexpr It algo_find(It first, It last, const T& value) {
  for (It it = first; it != last; ++it) {
    if (*it == value)
      return it;
  }
  return last;
}
