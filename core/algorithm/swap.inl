//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

template <typename T>
constexpr void algo_swap(T& first, T& second) {
  T temp = first;
  first = second;
  second = temp;
}
