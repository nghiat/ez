//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/linear_allocator.hpp"
#include <string.h>

template <typename T>
void algo_merge_sort(T* input, int length) {
  if (length < 2)
    return;
  linear_allocator_t la;
  la_init(&la, "algo_merge_sort_allocator");
  T* temp_arr_begin = (T*)allocator_alloc(&la, length * sizeof(T));
  T* const end = input + length;
  for (int step = 1; step  < length; step *= 2) {
    for (int i = 0; i < (length + step - 1) / step / 2; ++i) {
      T* temp_arr = temp_arr_begin;
      T* a1 = input + 2 * i * step;
      T* a1_begin = a1;
      T* a2 = a1 + step;
      T* a1_end = a1 + step;
      T* a2_end = a2 + step;
      if (a2_end >= end)
        a2_end = end;
      while (true) {
        if (*a1 < *a2) {
          *temp_arr = *a1;
          ++a1;
        }
        else {
          *temp_arr = *a2;
          ++a2;
        }
        ++temp_arr;
        if (a1 == a1_end) {
          memcpy(temp_arr, a2, (a2_end - a2) * sizeof(T));
          break;
        }
        if (a2 == a2_end) {
          memcpy(temp_arr, a1, (a1_end - a1) * sizeof(T));
          break;
        }
      }
      memcpy(a1_begin, temp_arr_begin, (a2_end - a1_begin) * sizeof(T));
    }
  }
  allocator_destroy(&la);
}
