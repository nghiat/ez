//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/platform.hpp"
#include "core/string.hpp"
#if defined(OS_WIN)
#  include "core/windows_lite.hpp"
#endif
#include <stddef.h>

struct file_t {
  enum mode_e {
    // open file if it exists, otherwise, create a new file.
    MODE_READ = 1 << 0,
    MODE_WRITE = 1 << 1,
    MODE_APPEND = 1 << 2,
  };

  enum from_e { FROM_BEGIN, FROM_CURRENT, FROM_END };
#if defined(OS_WIN)
  HANDLE file;
#else
  int file;
#endif
  const os_char_t* path;
};

bool file_open(file_t* file, const os_char_t* path, file_t::mode_e mode);
void file_close(file_t* file);

void file_delete(file_t* file);
void file_delete_path(const os_char_t* path);

void file_read(file_t* file, void* buffer, size_t size);
void file_write(file_t* file, size_t size, void* o_buffer);
void file_seek(file_t* file, file_t::from_e from, size_t distance);

bool file_is_valid(const file_t* file);

size_t file_get_size(const file_t* file);
