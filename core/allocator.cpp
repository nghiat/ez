//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/allocator.hpp"

void allocator_destroy(allocator_t* allocator) {
  allocator->fns->destroy(allocator);
}

void* allocator_alloc(allocator_t* allocator, uint32_t size) {
  return allocator_aligned_alloc(allocator, size, 1);
}

void* allocator_aligned_alloc(allocator_t* allocator,
                              uint32_t size,
                              uint32_t alignment) {
  return allocator->fns->aligned_alloc(allocator, size, alignment);
}

void* allocator_realloc(allocator_t* allocator, void* p, uint32_t size) {
  return allocator->fns->realloc(allocator, p, size);
}

void allocator_free(allocator_t* allocator, void* p) {
  allocator->fns->free(allocator, p);
}
