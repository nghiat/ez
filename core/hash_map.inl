//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/hash_map.hpp"

#include "core/hash_table.inl"

template <typename K, typename V, typename H>
pair_t<hash_map_it_t<K, V, H>, bool>
ht_insert_or_get(hash_map_t<K, V, H>* hm, const K& key, const V& value) {
  pair_t<hash_map_it_t<K, V, H>, bool> pair = ht_insert_or_get(hm, key);
  if (pair.second) {
    (*pair.first).value = value;
  }
  return pair;
}

template <typename K, typename V, typename H>
pair_t<hash_map_it_t<K, V, H>, bool>
ht_insert_or_set(hash_map_t<K, V, H>* hm, const K& key, const V& value) {
  pair_t<hash_map_it_t<K, V, H>, bool> pair = ht_insert_or_get(hm, key);
  (*pair.first).value = value;
  return pair;
}
