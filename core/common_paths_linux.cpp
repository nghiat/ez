//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/common_paths.hpp"
#include "core/common_paths.inl"

#include "core/allocator.hpp"
#include "core/core_allocators.hpp"
#include <linux/limits.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

bool common_paths_init() {
  char exe_path[PATH_MAX];
  ssize_t length = readlink("/proc/self/exe", exe_path, PATH_MAX);
  g_exe_path = str_copy(g_persistent_allocator, exe_path, length + 1);
  for (ssize_t i = length - 1; i >= 0; --i) {
    if (exe_path[i] == '/') {
      g_exe_dir = str_copy(g_persistent_allocator, g_exe_path, i + 2);
      g_exe_dir[i + 1] = '\0';
      break;
    }
  }
  return true;
}
