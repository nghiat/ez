//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/sorted_map.hpp"

#include "core/pair.hpp"
#include "core/sorted_array.inl"

template <typename K, typename V>
pair_t<sorted_map_it_t<K, V>, bool>
sa_insert_or_get(sorted_map_t<K, V>* sm, const K& key, const V& value) {
  pair_t<sorted_map_it_t<K, V>, bool> pair = sa_insert_or_get(sm, key);
  if (pair.second) {
    pair.first->value = value;
  }
  return pair;
}

template <typename K, typename V>
pair_t<sorted_map_it_t<K, V>, bool>
sa_insert_or_set(sorted_map_t<K, V>* sm, const K& key, const V& value) {
  pair_t<sorted_map_it_t<K, V>, bool> pair = sa_insert_or_get(sm, key);
  pair.first->value = value;
  return pair;
}
