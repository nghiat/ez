//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#pragma once

template <typename Key, typename Value>
struct pair_t {
  union {
    struct {
      Key key;
      Value value;
    };
    struct {
      Key first;
      Value second;
    };
  };
};

template <typename Key, typename Value>
struct get_key_t {
  Key& operator()(pair_t<Key, Value>& data) { return data.key; }

  const Key& operator()(const pair_t<Key, Value>& data) const {
    return data.key;
  }
};

template <typename Key>
struct get_self_t {
  Key& operator()(Key& data) { return data; }

  const Key& operator()(const Key& data) const { return data; }
};

template <typename K, typename V>
bool operator==(const pair_t<K, V>& lhs, const pair_t<K, V>& rhs) {
  return lhs.key == rhs.key && lhs.value == rhs.value;
}
