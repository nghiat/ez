//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/hash_table.hpp"
#include "core/pair.hpp"

template <typename Key, typename Hash = fnv1_hash_t<Key>>
using hash_set_t = hash_table_t<Key, Hash>;
