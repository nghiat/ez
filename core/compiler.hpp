//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#if defined(_MSC_VER)
#  define COMPILER_MSVC
#endif

#if defined(__GNUG__)
#  define COMPILER_GCC
#endif

#if defined(__clang__)
#  define COMPILER_CLANG
#endif

#if defined(COMPILER_MSVC) && defined(NDEBUG)
#  define EZ_INLINE __forceinline
#elif (defined(COMPILER_GCC) || defined(COMPILER_CLANG)) && defined(NDEBUG)
#  define EZ_INLINE inline __attribute__((__always_inline__))
#else
#  define EZ_INLINE inline
#endif
