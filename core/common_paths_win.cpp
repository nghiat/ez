//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/common_paths.hpp"
#include "core/common_paths.inl"

#include "core/core_allocators.hpp"
#include <Windows.h>

bool common_paths_init() {
  wchar_t exe_path[MAX_PATH];
  DWORD length = GetModuleFileName(NULL, exe_path, MAX_PATH);
  g_exe_path = str_copy(g_persistent_allocator, exe_path, length + 1);
  for (DWORD i = length - 1; i >= 0; --i) {
    if (exe_path[i] == L'\\') {
      g_exe_dir = str_copy(g_persistent_allocator, g_exe_path, i + 2);
      g_exe_dir[i + 1] = '\0';
      break;
    }
  }
  return true;
}
