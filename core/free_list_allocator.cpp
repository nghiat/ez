//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/free_list_allocator.hpp"

#include "core/allocator_internal.hpp"
#include "core/log.hpp"
#include <stdlib.h>
#include <string.h>

// freeblock_t and allocations use a shared memory region.
struct freeblock_t {
  /// |size| includes this struct.
  size_t size;
  freeblock_t* next;
};

static_assert(
    sizeof(allocation_header_t) >= sizeof(freeblock_t),
    "We assume that sizeof(allocation_header_t) >= sizeof(freeblock_t) so when "
    "freeating an allocation, we always have space for a freeblock_t");

namespace {

// Finds the smallest possible block that can fits the |requiredSize| with
// |alignment| and returns the pointer to the beginning of the allocation
// region.
uint8_t* find_best_fit_free_block(const free_list_allocator_t* fla,
                                  uint32_t size,
                                  uint32_t alignment,
                                  freeblock_t** o_fit_block,
                                  freeblock_t** o_prior_block) {
  uint8_t* p = nullptr;
  freeblock_t* curr_block = fla->first_block;
  freeblock_t* curr_prior_block = nullptr;
  *o_fit_block = nullptr;
  *o_prior_block = nullptr;
  while (curr_block) {
    uint8_t* curr_p = (uint8_t*)curr_block + sizeof(allocation_header_t);
    // Align the returned pointer if neccessary.
    curr_p = align_forward(curr_p, alignment);
    if (curr_block->size >= (curr_p - (uint8_t*)curr_block) + size) {
      if (!*(o_fit_block) || (curr_block->size < (*o_fit_block)->size)) {
        *o_fit_block = curr_block;
        *o_prior_block = curr_prior_block;
        p = curr_p;
      }
    }
    curr_prior_block = curr_block;
    curr_block = curr_block->next;
  }
  if (!(*o_fit_block))
    return nullptr;
  return p;
}

// Get the free_block before and after |p|.
void get_adjacent_blocks(const free_list_allocator_t* fla,
                         uint8_t* p,
                         freeblock_t** prior_block,
                         freeblock_t** next_block) {
  *prior_block = nullptr;
  *next_block = fla->first_block;
  while (*next_block && p > (uint8_t*)(*next_block)) {
    *prior_block = *next_block;
    *next_block = (*next_block)->next;
  }
}

// Links 2 consecutive separated blocks. If the |priorBlock| is null,
// set |first_free_block_| to |block|.
void link_separated_blocks(free_list_allocator_t* fla,
                           freeblock_t* block,
                           freeblock_t* prior_block) {
  if (prior_block)
    prior_block->next = block;
  else
    fla->first_block = block;
}

// Returns true if |size| is bigger than sizeof(allocation_header_t) which we
// assume that it is bigger than sizeof(freeblock_t).
bool is_enough_for_allocation_header(uint32_t size) {
  return size > sizeof(allocation_header_t);
}

bool is_allocaiton_adjacent_to_free_block(allocation_header_t* header,
                                          freeblock_t* block) {
  return block &&
         (uint8_t*)header + sizeof(allocation_header_t) + header->size ==
             (uint8_t*)block;
}

// Link two blocks together. If they are contiguous, merge them
// and assign *second to *first.
void link_and_merge_free_blocks(free_list_allocator_t* fla,
                                freeblock_t** first,
                                freeblock_t** second) {
  if (!*first) {
    fla->first_block = *second;
    return;
  }
  // Merge two blocks if they are contiguous.
  if ((uint8_t*)(*first) + (*first)->size == (uint8_t*)(*second)) {
    (*first)->size += (*second)->size;
    (*first)->next = (*second)->next;
    *second = *first;
  } else
    (*first)->next = *second;
}

// Add a freeblock_t to the freeblock_t linked list.
// If the freeblock_t is adjacent to the prior block or the next block, they
// will be merged
void add_and_merge_free_block(free_list_allocator_t* fla, freeblock_t* block) {
  freeblock_t* prior_block = nullptr;
  freeblock_t* next_block = fla->first_block;
  get_adjacent_blocks(fla, (uint8_t*)block, &prior_block, &next_block);
  if (prior_block)
    link_and_merge_free_blocks(fla, &prior_block, &block);
  else
    fla->first_block = block;

  if (next_block)
    link_and_merge_free_blocks(fla, &block, &next_block);
}

// Shrink |block| (remove |Size| on the left) and link the new |freeblock_t|
// with |prior_block| if it is possible. Returns true if the block can be
// shrunk (means that there is enough space for at least a allocation_header_t
// after shrinking), false otherwise.
bool shrink_free_block(free_list_allocator_t* fla,
                       freeblock_t* block,
                       freeblock_t* prior_block,
                       uint32_t size) {
  uint32_t block_size_after = block->size - size;
  if (!is_enough_for_allocation_header(block_size_after)) {
    if (prior_block)
      prior_block->next = block->next;
    else
      fla->first_block = block->next;
    fla->used_size += block->size;
    return false;
  }
  freeblock_t* shrunk_block = (freeblock_t*)((uint8_t*)block + size);
  *shrunk_block = freeblock_t{
      block_size_after, // size
      block->next       // next
  };
  link_separated_blocks(fla, shrunk_block, prior_block);
  fla->used_size += size;
  return true;
}

void realloc_smaller(free_list_allocator_t* fla,
                     uint8_t* p,
                     uint32_t size,
                     freeblock_t* prior_block,
                     freeblock_t* next_block) {
  allocation_header_t* header = get_allocation_header(p);
  uint32_t size_after_shrunk = header->size - size;
  // If there is a freeblock_t right after the allocation, we shift the
  // freeblock_t to the end of the new allocation.
  if (is_allocaiton_adjacent_to_free_block(header, next_block)) {
    header->size = size;
    freeblock_t* shifted_block = (freeblock_t*)(p + size);
    *shifted_block = freeblock_t{
        next_block->size + size_after_shrunk, // size
        next_block->next                      // next
    };
    link_and_merge_free_blocks(fla, &prior_block, &shifted_block);
    fla->used_size -= size_after_shrunk;
    return;
  }
  // There is not enough space for a new freeblock_t, nothing changes.
  if (!is_enough_for_allocation_header(size_after_shrunk)) {
    return;
  }
  // Else, we create a new freeblock_t.
  header->size = size;
  freeblock_t* new_block = (freeblock_t*)(p + size);
  new_block->size = size_after_shrunk;
  new_block->next = next_block;
  link_separated_blocks(fla, new_block, prior_block);
  fla->used_size -= size_after_shrunk;
}

uint8_t* realloc_bigger(free_list_allocator_t* fla,
                        uint8_t* p,
                        uint32_t size,
                        freeblock_t* prior_block,
                        freeblock_t* next_block) {
  allocation_header_t* header = get_allocation_header(p);
  // Check if |next_block| is adjacent to |p| so we may extend |p|.
  if (is_allocaiton_adjacent_to_free_block(header, next_block)) {
    if (header->size + next_block->size >= size) {
      uint32_t size_after_extended = header->size + next_block->size - size;
      if (is_enough_for_allocation_header(size_after_extended)) {
        freeblock_t* new_block = (freeblock_t*)(p + size);
        *new_block = freeblock_t{
            size_after_extended, // size
            next_block->next     // next
        };
        link_and_merge_free_blocks(fla, &prior_block, &new_block);
        fla->used_size += size - header->size;
        header->size = size;
      } else {
        fla->used_size += next_block->size;
        header->size += next_block->size;
        link_separated_blocks(fla, next_block->next, prior_block);
      }
      return p;
    }
  }
  // Else, find other freeblock_t.
  // If sizeof(allocation_header_t) is smaller than sizeof(freeblock_t), then
  // the |new_block| will overwrite the data of p
  allocation_header_t backup_header = *header;
  freeblock_t backup_prior_block;
  freeblock_t backup_next_block;
  if (prior_block)
    backup_prior_block = *prior_block;
  if (next_block)
    backup_next_block = *next_block;
  freeblock_t* backup_first_block = fla->first_block;
  freeblock_t* new_block = (freeblock_t*)backup_header.start;
  *new_block = freeblock_t{
      (size_t)(header->size + (p - header->start)), // size
      nullptr                                       // next
  };
  add_and_merge_free_block(fla, new_block);
  freeblock_t* fit_block;
  freeblock_t* prior_fit_block;
  uint8_t* returned_pointer = find_best_fit_free_block(
      fla, size, backup_header.alignment, &fit_block, &prior_fit_block);
  if (returned_pointer) {
    fla->used_size -= backup_header.size + (p - backup_header.start);
    memmove(returned_pointer, p, size);
    uint32_t padding_and_header = returned_pointer - (uint8_t*)fit_block;
    bool rv = shrink_free_block(
        fla, fit_block, prior_fit_block, padding_and_header + size);
    *get_allocation_header(returned_pointer) =
        allocation_header_t((uint8_t*)fit_block,
                            rv ? size : fit_block->size - padding_and_header,
                            backup_header.alignment,
                            returned_pointer);
    return returned_pointer;
  }
  // Restores |header|
  if (prior_block)
    *prior_block = backup_prior_block;
  if (next_block)
    *next_block = backup_next_block;
  fla->first_block = backup_first_block;
  *header = backup_header;
  LOGD("Free list allocator \"%s\" doesn't have enough space to alloc %d "
       "bytes",
       fla->name,
       size);
  return nullptr;
}

void fla_destroy(allocator_t* allocator) {
  free_list_allocator_t* fla = (free_list_allocator_t*)allocator;
  if (fla->start)
    free(fla->start);
}

void* fla_aligned_alloc(allocator_t* allocator,
                        uint32_t size,
                        uint32_t alignment) {
  free_list_allocator_t* fla = (free_list_allocator_t*)allocator;
  if (!check_aligned_alloc(fla->name, size, alignment))
    return nullptr;
  freeblock_t* fit_block;
  freeblock_t* prior_block;
  uint8_t* p =
      find_best_fit_free_block(fla, size, alignment, &fit_block, &prior_block);
  if (!p) {
    LOGD("Free list allocator \"%s\" doesn't have enough space to alloc %d "
         "bytes",
         fla->name,
         size);
    return nullptr;
  }
  uint32_t padding_and_header = p - (uint8_t*)fit_block;
  bool rv =
      shrink_free_block(fla, fit_block, prior_block, padding_and_header + size);
  *get_allocation_header(p) =
      allocation_header_t((uint8_t*)fit_block,
                          rv ? size : fit_block->size - padding_and_header,
                          alignment,
                          p);
  return p;
}

void* fla_realloc(allocator_t* allocator, void* p, uint32_t size) {
  free_list_allocator_t* fla = (free_list_allocator_t*)allocator;
  if (!check_realloc(fla->name, p, size)) {
    return nullptr;
  }

  allocation_header_t* header = get_allocation_header(p);
  // Remaining free space is surely not enough.
  if (size > header->size + (fla->size - fla->used_size))
    return nullptr;

  if (size == header->size) {
    return p;
  }

  freeblock_t* prior_block;
  freeblock_t* next_block;
  get_adjacent_blocks(fla, (uint8_t*)p, &prior_block, &next_block);
  // smaller size
  if (size < header->size) {
    realloc_smaller(fla, (uint8_t*)p, size, prior_block, next_block);
    return p;
  }

  return realloc_bigger(fla, (uint8_t*)p, size, prior_block, next_block);
}

void fla_free(allocator_t* allocator, void* p) {
  free_list_allocator_t* fla = (free_list_allocator_t*)allocator;
  allocation_header_t* header = get_allocation_header(p);
#ifdef BUILD_DEBUG
  if (header->p != p) {
    LOGD("Free list allocator %s can't free pointer %p because of invalid "
         "header",
         fla->name,
         p);
    return;
  }
#endif
  size_t freed_size = header->size + ((uint8_t*)p - header->start);
  fla->used_size -= freed_size;
  freeblock_t* new_block = (freeblock_t*)header->start;
  *new_block = freeblock_t{
      freed_size, // size
      nullptr     // next
  };
  add_and_merge_free_block(fla, new_block);
}

const allocator_fns_t gc_fla_allocator_fns = {&fla_destroy,
                                              &fla_aligned_alloc,
                                              &fla_realloc,
                                              &fla_free};

} // namespace

bool fla_init(free_list_allocator_t* fla, const char* name, size_t size) {
  fla->fns = &gc_fla_allocator_fns;
  fla->name = name;
  fla->size = size;
  fla->start = (uint8_t*)malloc(size);
  if (!fla->start) {
    LOGD("Can't init allocator \"%s\": Out of memory", name);
    return false;
  }
  fla->first_block = (freeblock_t*)(fla->start);
  fla->first_block->size = size;
  fla->first_block->next = nullptr;
  return true;
}
