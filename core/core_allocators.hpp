//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

class allocator_t;

// Allocate once and will never change.
extern allocator_t* g_persistent_allocator;

// General purpose allocator.
extern allocator_t* g_general_allocator;

bool core_allocators_init();
