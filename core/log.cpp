//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/log.hpp"

#include "core/file.hpp"
#include "core/linear_allocator.hpp"
#include <stdarg.h>
#include <stdlib.h>
#if defined(OS_WIN)
#  include <Windows.h>
#  ifndef ENABLE_VIRTUAL_TERMINAL_PROCESSING // For color print.
#    define ENABLE_VIRTUAL_TERMINAL_PROCESSING 0x0004
#  endif // ENABLE_VIRTUAL_TERMINAL_PROCESSING
#endif

namespace {

constexpr const char gc_ansi_red[] = "\x1b[31m";
constexpr const char gc_ansi_yellow[] = "\x1b[33m";
constexpr const char gc_ansi_blue[] = "\x1b[34m";
constexpr const char gc_ansi_white[] = "\x1b[37m";
constexpr const char gc_ansi_reset[] = "\x1b[0m";

struct Name_And_Color {
  const char* name;
  const char* color;
};

const Name_And_Color gc_names_and_colors[] = {{"INFO", gc_ansi_white},
                                              {"DEBUG", gc_ansi_blue},
                                              {"WARNING", gc_ansi_yellow},
                                              {"FATAL", gc_ansi_red}};
#if defined(BUILD_DEBUG)
const bool gc_is_debug_build = true;
#else
const bool gc_is_debug_build = false;
#endif

file_t g_log_file;
} // namespace

namespace internal {

void log(log_level_e level,
         const char* file,
         int line,
         const char* format,
         ...) {
  if (level == log_level_e::LEVEL_DEBUG && !gc_is_debug_build)
    return;

  linear_allocator_t allocator;
  la_init(&allocator, "log_temp_allocator");

  const char* filename = strrchr(file, '\\') ? strrchr(file, '\\') + 1 : file;
  filename = strrchr(filename, '/') ? strrchr(filename, '/') + 1 : filename;
  int char_num = snprintf(nullptr, 0, "%s:%d %s", filename, line, format) + 1;
  char* new_format = (char*)allocator_alloc(&allocator, char_num);
  snprintf(new_format, char_num, "%s:%d %s", filename, line, format);

  va_list argptr, argptr2;
  va_start(argptr, format);
  va_copy(argptr2, argptr);
  char_num = (vsnprintf(nullptr, 0, new_format, argptr) + 1) * 2;
  char* log_buffer = (char*)allocator_alloc(&allocator, char_num);
  va_start(argptr, format);
  char_num = vsnprintf(log_buffer, char_num, new_format, argptr);
  va_end(argptr);
  va_end(argptr2);

  if (level == log_level_e::LEVEL_FATAL) {
    format = "\nStackTraces:\n%s";
    const char* trace = get_stack_trace();
    int additional_char_num = snprintf(nullptr, 0, format, trace) * 2;
    allocator_realloc(&allocator, log_buffer, char_num + additional_char_num);
    additional_char_num =
        snprintf(log_buffer + char_num, additional_char_num, format, trace);
    char_num += additional_char_num;
  }
  // Log to stream
  {
    FILE* stream;
    if (level == log_level_e::LEVEL_INFO || level == log_level_e::LEVEL_DEBUG)
      stream = stdout;
    else
      stream = stderr;
    fprintf(stream,
            "%s%s: %s%s\n",
            gc_names_and_colors[(int)level].color,
            gc_names_and_colors[(int)level].name,
            log_buffer,
            gc_ansi_reset);
  }
  // Log to file
  {
    linear_allocator_t allocator2;
    la_init(&allocator2, "log_file_temp_allocator");
    format = "%s: %s\n";
    char_num = (snprintf(nullptr,
                         0,
                         format,
                         gc_names_and_colors[(int)level].name,
                         log_buffer) +
                1) *
               2;
    char* log_buffer_to_file = (char*)allocator_alloc(&allocator2, char_num);
    char_num = snprintf(log_buffer_to_file,
                        char_num,
                        format,
                        gc_names_and_colors[(int)level].name,
                        log_buffer);
    file_write(&g_log_file, char_num, log_buffer_to_file);
    allocator_destroy(&allocator2);
  }
  if (level == log_level_e::LEVEL_FATAL) {
#ifdef OS_WIN
    __debugbreak();
#endif
    exit(1);
  }
  allocator_destroy(&allocator);
}

} // namespace internal

bool log_init(const os_char_t* log_path) {
#if defined(OS_WIN)
  // init color print on Windows.
  HANDLE h_out = GetStdHandle(STD_OUTPUT_HANDLE);
  DWORD dw_mode = 0;
  GetConsoleMode(h_out, &dw_mode);
  dw_mode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
  SetConsoleMode(h_out, dw_mode);
#endif // OS_WIN
  file_open(&g_log_file, log_path, file_t::MODE_APPEND);
  return file_is_valid(&g_log_file);
}
