//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/file.hpp"

#include <Windows.h>

bool file_open(file_t* file, const wchar_t* path, file_t::mode_e mode) {
  file->path = path;
  DWORD access = 0;
  if (mode & file_t::MODE_READ)
    access |= GENERIC_READ;
  if (mode & file_t::MODE_WRITE)
    access |= GENERIC_READ | GENERIC_WRITE;
  if (mode & file_t::MODE_APPEND)
    access |= GENERIC_READ | FILE_APPEND_DATA;

  DWORD share_mode = 0;
  share_mode |= FILE_SHARE_READ;

  DWORD create_disposition = 0;
  if (mode & file_t::MODE_READ)
    create_disposition |= OPEN_EXISTING;
  if (mode & file_t::MODE_WRITE)
    create_disposition |= CREATE_ALWAYS;
  if (mode & file_t::MODE_APPEND)
    create_disposition |= OPEN_EXISTING;
  file->file = CreateFile(
      file->path, access, share_mode, nullptr, create_disposition, 0, nullptr);
  return file_is_valid(file);
}

void file_close(file_t* file) {
  if (file_is_valid(file)) {
    CloseHandle(file->file);
    file->file = INVALID_HANDLE_VALUE;
  }
}

void file_delete(file_t* file) { file_delete_path(file->path); }

void file_delete_path(const wchar_t* path) { DeleteFile(path); }

void file_read(file_t* file, void* buffer, size_t size) {
  DWORD read_bytes = 0;
  ReadFile(file->file, buffer, size, &read_bytes, nullptr);
}

void file_write(file_t* file, size_t size, void* o_buffer) {
  DWORD written_bytes = 0;
  WriteFile(file->file, o_buffer, size, &written_bytes, nullptr);
}

void file_seek(file_t* file, file_t::from_e from, size_t distance) {
  DWORD move_method;
  switch (from) {
  case file_t::FROM_BEGIN:
    move_method = FILE_BEGIN;
    break;
  case file_t::FROM_CURRENT:
    move_method = FILE_CURRENT;
    break;
  case file_t::FROM_END:
    move_method = FILE_END;
    break;
  }
  SetFilePointer(file->file, distance, nullptr, move_method);
}

bool file_is_valid(const file_t* file) {
  return file->file != INVALID_HANDLE_VALUE;
}

size_t file_get_size(const file_t* file) {
  return GetFileSize(file->file, nullptr);
}
