//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include <new>
#include <stdint.h>

struct allocator_t;

struct allocator_fns_t {
  void (*destroy)(allocator_t*);
  void* (*aligned_alloc)(allocator_t*, uint32_t size, uint32_t alignment);
  void* (*realloc)(allocator_t*, void*, uint32_t size);
  void (*free)(allocator_t*, void*);
};

struct allocator_t {
  const allocator_fns_t* fns = nullptr;
  const char* name = nullptr;
  /// Total size of the allocator in bytes.
  size_t size = 0;
  /// Allocations' sizes and supporting data of the allocator in bytes.
  size_t used_size = 0;
};

void allocator_destroy(allocator_t* allocator);
void* allocator_alloc(allocator_t* allocator, uint32_t size);
void* allocator_aligned_alloc(allocator_t* allocator,
                              uint32_t size,
                              uint32_t alignment);
void* allocator_realloc(allocator_t* allocator, void* p, uint32_t size);
void allocator_free(allocator_t* allocator, void* p);

template <typename T, typename... Args>
T* allocator_new(allocator_t* allocator, Args&&... args) {
  T* p = (T*)allocator_aligned_alloc(allocator, sizeof(T), alignof(T));
  new (p) T(args...);
  return p;
}
