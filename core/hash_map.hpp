//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/hash_table.hpp"
#include "core/pair.hpp"

template <typename Key, typename Value, typename Hash = fnv1_hash_t<Key>>
using hash_map_t =
    hash_table_t<Key, pair_t<Key, Value>, Hash, get_key_t<Key, Value>>;

template <typename Key, typename Value, typename Hash = fnv1_hash_t<Key>>
using hash_map_it_t =
    hash_table_it_t<Key, pair_t<Key, Value>, Hash, get_key_t<Key, Value>>;

// The bool in returned value is false when |key| exists, true otherwise.
template <typename K, typename V, typename H>
pair_t<hash_map_it_t<K, V, H>, bool>
ht_insert_or_get(hash_map_t<K, V, H>* hm, const K& key, const V& value);

// The bool in returned value is false when |key| exists, true otherwise.
template <typename K, typename V, typename H>
pair_t<hash_map_it_t<K, V, H>, bool>
ht_insert_or_set(hash_map_t<K, V, H>* hm, const K& key, const V& value);
