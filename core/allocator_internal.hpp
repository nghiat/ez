//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/build.hpp"
#include <stdint.h>

/// Here is the sketch for a general allocation.
/// start                    p         size        end
///  |                       |----------------------|
///  o_______________________o______________________o
///  |   |allocation_header_t| allocation | padding |
///    o                                       |
///    |                                       o
/// prefix padding                        suffix padding (the remaning space
///  (alignment)                          is not enough for another
///                                       Allocation_Header)
/// An Allocation_Header is placed before any pointer returned by alloc()
/// or aligned_alloc() to keep track of the allocation (prefix padding is
/// used for alignment). In a case when space between the current allocation
/// and the next allocation is not enough for any allocation then the current
/// allocation will acquire the remaining space (suffix padding).
struct allocation_header_t {
  allocation_header_t(uint8_t* start,
                      uint32_t size,
                      uint32_t alignment,
                      uint8_t* p)
      : start(start),
        size(size),
        alignment(alignment)
#ifdef BUILD_DEBUG
        ,
        p(p)
#endif
  {
  }

  uint8_t* start;
  uint32_t size;
  uint32_t alignment;
#ifdef BUILD_DEBUG
  uint8_t* p;
#endif
};

allocation_header_t* get_allocation_header(void* p);
uint8_t* align_forward(uint8_t* p, uint32_t alignment);
bool check_aligned_alloc(const char* allocator_name,
                         uint32_t size,
                         uint32_t alignment);
bool check_realloc(const char* allocator_name, void* p, uint32_t size);
