//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

// OS macros
#if defined(_WIN32) || defined(_WIN64)
#  define OS_WIN
#endif // defined(_WIN32) || defined(_WIN64)

#if defined(__linux__)
#  define OS_LINUX
#endif // defined(__linux__)
