//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/stack_trace.hpp"
#include <Windows.h>
#include <DbgHelp.h>
#include <mutex>
#include <stdio.h>

namespace {

char g_stack_trace_buffer[internal::gc_max_stack_trace_length];

std::mutex g_mutex;

} // namespace

bool stack_trace_init() {
  SymSetOptions(SYMOPT_DEFERRED_LOADS | SYMOPT_UNDNAME | SYMOPT_LOAD_LINES);
  SymInitialize(GetCurrentProcess(), NULL, TRUE);
  return true;
}

const char* get_stack_trace() {
  std::lock_guard<std::mutex> lk(g_mutex);
  void* frames[internal::gc_max_traces];
  int count = CaptureStackBackTrace(0, internal::gc_max_traces, frames, NULL);
  memset(g_stack_trace_buffer, 0, sizeof(g_stack_trace_buffer));
  char* current_buffer_pointer = g_stack_trace_buffer;
  int remaining_size = internal::gc_max_stack_trace_length;
  for (int i = 0; i < count; ++i) {
    DWORD64 displacement = 0;
    DWORD_PTR address = (DWORD_PTR)frames[i];
    char symbol_buffer[sizeof(SYMBOL_INFO) +
                       internal::gc_max_symbol_length * sizeof(TCHAR)];
    char traceBuffer[internal::gc_max_symbol_length];
    memset(symbol_buffer, 0, sizeof(symbol_buffer));
    memset(traceBuffer, 0, sizeof(traceBuffer));

    PSYMBOL_INFO symbol_info = (PSYMBOL_INFO)symbol_buffer;

    symbol_info->SizeOfStruct = sizeof(SYMBOL_INFO);
    symbol_info->MaxNameLen = internal::gc_max_symbol_length - 1;
    DWORD line_displacement = 0;
    IMAGEHLP_LINE line = {};
    line.SizeOfStruct = sizeof(IMAGEHLP_LINE64);
    HANDLE current_process = GetCurrentProcess();
    int trace_chars_written = 0;
    if (SymFromAddr(current_process, address, &displacement, symbol_info)) {
      trace_chars_written = snprintf(
          traceBuffer, internal::gc_max_symbol_length, "%s", symbol_info->Name);
    }
    if (SymGetLineFromAddr(
            current_process, address, &line_displacement, &line)) {
      snprintf(traceBuffer + trace_chars_written,
               internal::gc_max_symbol_length - trace_chars_written,
               "%s:%lu",
               line.FileName,
               line.LineNumber);
    }
    const char* format;
    if (i)
      format = "\n\t%s";
    else
      format = "\t%s";
    int stack_trace_chars_written =
        snprintf(current_buffer_pointer, remaining_size, format, traceBuffer);
    current_buffer_pointer += stack_trace_chars_written;
    remaining_size -= stack_trace_chars_written;
  }
  return g_stack_trace_buffer;
}
