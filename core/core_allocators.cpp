//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core_allocators.hpp"
#include "free_list_allocator.hpp"
#include "linear_allocator.hpp"

namespace {
linear_allocator_t g_persistent_allocator_;
free_list_allocator_t g_general_allocator_;
} // namespace

allocator_t* g_persistent_allocator = &g_persistent_allocator_;
allocator_t* g_general_allocator = &g_general_allocator_;

bool core_allocators_init() {
  bool rv = true;
  rv &= la_init(&g_persistent_allocator_, "persistent_allocator", 1024 * 1024);
  rv &= fla_init(&g_general_allocator_, "general_allocator", 10 * 1024 * 1024);
  return rv;
}
