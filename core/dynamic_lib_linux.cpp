//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#include "core/dynamic_lib.hpp"

#include <dlfcn.h>

bool dl_open(dynamic_lib_t* dl, const char* name) {
  *dl = dlopen(name, RTLD_LAZY | RTLD_LOCAL);
  return *dl;
}

void dl_close(dynamic_lib_t* dl) { dlclose(*dl); }

void* dl_get_proc(dynamic_lib_t* dl, const char* name) {
  return dlsym(*dl, name);
}
