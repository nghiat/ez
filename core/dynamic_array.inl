//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/dynamic_array.hpp"

#include "core/allocator.hpp"
#include "core/log.hpp"
#include <stdlib.h>
#include <string.h>

template <typename T>
bool da_init(dynamic_array_t<T>* da, allocator_t* allocator) {
  da->allocator = allocator;
  return true;
}

template <typename T>
void da_destroy(dynamic_array_t<T>* da) {
  if (da->p)
    allocator_free(da->allocator, da->p);
}

template <typename T>
dynamic_array_it_t<T> begin(const dynamic_array_t<T>& da) {
  return da.p;
}

template <typename T>
dynamic_array_it_t<T> end(const dynamic_array_t<T>& da) {
  return da.p + da.length;
}

template <typename T>
void da_reserve(dynamic_array_t<T>* da, size_t num) {
  if (num <= da->capacity)
    return;
  if (!da->p)
    da->p = (T*)allocator_alloc(da->allocator, num * sizeof(T));
  else
    da->p = (T*)allocator_realloc(da->allocator, da->p, num * sizeof(T));
  da->capacity = num;
}

template <typename T>
void da_resize(dynamic_array_t<T>* da, size_t num) {
  da_reserve(da, num);
  da->length = num;
}

template <typename T>
void da_clone(dynamic_array_t<T>* da, const dynamic_array_t<T>* src) {
  da_resize(da, src->length);
  memcpy(da->p, src->p, da->length * sizeof(T));
}

template <typename T>
void da_clear(dynamic_array_t<T>* da) {
  da->length = 0;
}

template <typename T>
void da_remove_range(dynamic_array_t<T>* da, int pos, int length) {
  if (pos < 0 || pos > da->length || pos + length < 0 ||
      pos + length > da->length) {
    LOGW("Can't remove invalid rage");
    return;
  }
  memmove(da->p + pos,
          da->p + pos + length,
          (da->length - pos - length) * sizeof(T));
  da->length -= length;
}

template <typename T>
void da_remove_at(dynamic_array_t<T>* da, size_t pos) {
  da_remove_range(da, pos, 1);
}

template <typename T>
void da_remove_it(dynamic_array_t<T>* da, dynamic_array_it_t<T> it) {
  da_remove_at(da, it - begin(*da));
}

template <typename T>
void da_extend_if_necessary(dynamic_array_t<T>* da) {
  if (da->length == da->capacity) {
    da_reserve(da, (da->capacity + 1) * 3 / 2);
  }
}

template <typename T, typename... Args>
void da_insert_at(dynamic_array_t<T>* da, size_t index, Args&&... params) {
  da_extend_if_necessary(da);
  if (index < da->length)
    memmove(da->p + index + 1, da->p + index, (da->length - index) * sizeof(T));
  new (da->p + index) T(params...);
  da->length += 1;
}

template <typename T, typename... Args>
void da_append(dynamic_array_t<T>* da, Args&&... params) {
  da_insert_at(da, da->length, params...);
}

template <typename T>
bool operator==(const dynamic_array_t<T>& lhs, const dynamic_array_t<T>& rhs) {
  if (lhs.length != rhs.length)
    return false;
  return !memcmp(lhs.p, rhs.p, sizeof(T) * lhs.length);
}

template <typename T>
bool operator!=(const dynamic_array_t<T>& lhs, const dynamic_array_t<T>& rhs) {
  return !(lhs == rhs);
}
