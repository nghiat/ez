//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/dynamic_array.hpp"
#include "core/pair.hpp"

template <typename Key, typename Data, typename Get_Key>
struct sorted_array_t {
  dynamic_array_t<Data> array;
};

template <typename Key, typename Data, typename Get_Key>
using sorted_array_it_t = dynamic_array_it_t<Data>;

template <typename K, typename D, typename G>
bool sa_init(sorted_array_t<K, D, G>* sa, allocator_t* allocator);

template <typename K, typename D, typename G>
void sa_destroy(sorted_array_t<K, D, G>* sa);

template <typename K, typename D, typename G>
void sa_reserve(sorted_array_t<K, D, G>* sa, int num);

template <typename K, typename D, typename G>
size_t sa_length(const sorted_array_t<K, D, G>* sa);

template <typename K, typename D, typename G>
sorted_array_it_t<K, D, G> begin(const sorted_array_t<K, D, G>& sa);

template <typename K, typename D, typename G>
sorted_array_it_t<K, D, G> end(const sorted_array_t<K, D, G>& sa);

template <typename K, typename D, typename G>
void sa_remove_it(sorted_array_t<K, D, G>* sa, sorted_array_it_t<K, D, G> it);

template <typename K, typename D, typename G>
void sa_remove_key(sorted_array_t<K, D, G>* sa, const K& key);

template <typename K, typename D, typename G>
pair_t<sorted_array_it_t<K, D, G>, bool>
sa_insert_or_get(sorted_array_t<K, D, G>* sa, const K& key);

template <typename K, typename D, typename G>
bool operator==(const sorted_array_t<K, D, G>& lhs,
                const sorted_array_t<K, D, G>& rhs);

template <typename K, typename D, typename G>
bool operator!=(const sorted_array_t<K, D, G>& lhs,
                const sorted_array_t<K, D, G>& rhs);

template <typename K, typename D, typename G>
sorted_array_it_t<K, D, G> sa_find(const sorted_array_t<K, D, G>* sa,
                                   const K& key);
