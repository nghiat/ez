//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/pair.hpp"
#include "core/sorted_array.hpp"

template <typename Key>
using sorted_set_t = sorted_array_t<Key, Key, get_self_t<Key>>;

template <typename Key>
using sorted_set_it_t = sorted_array_it_t<Key, Key, get_self_t<Key>>;
