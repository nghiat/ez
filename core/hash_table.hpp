//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/dynamic_array.hpp"
#include "core/pair.hpp"

template <typename Key>
struct fnv1_hash_t {
  size_t operator()(const Key& key);
};

template <typename Key>
struct ht_key_t {
  size_t hash;
  // Starts from 1.
  int32_t probe;
  Key key;
};

template <typename Key, typename Hash>
struct hash_table_t {
  dynamic_array_t<ht_key_t<Key>> keys;
  size_t length = 0;
};

template <typename Key, typename Hash>
struct hash_table_it_t {
  const hash_table_t<Key, Hash>* ht;
  size_t idx;
};

template <typename K, typename H>
void ht_it_init(hash_table_it_t<K, H>* it,
                hash_table_t<K, H>* ht,
                int idx);

template <typename K, typename H>
hash_table_it_t<K, H>& operator++(hash_table_it_t<K, H>& it);

template <typename K, typename H>
bool operator==(const hash_table_it_t<K, H>& lhs,
                const hash_table_it_t<K, H>& rhs);

template <typename K, typename H>
bool operator!=(const hash_table_it_t<K, H>& lhs,
                const hash_table_it_t<K, H>& rhs);

// template <typename K, typename H>
// D& operator*(hash_table_it_t<K, H>& it);

template <typename K, typename H>
bool ht_init(hash_table_t<K, H>* ht, allocator_t* allocator);

template <typename K, typename H>
void ht_destroy(hash_table_t<K, H>* ht);

template <typename K, typename H>
hash_table_it_t<K, H> begin(const hash_table_t<K, H>& ht);

template <typename K, typename H>
hash_table_it_t<K, H> end(const hash_table_t<K, H>& ht);

template <typename K, typename H>
hash_table_it_t<K, H> ht_find(hash_table_t<K, H>* ht, const K& key);

template <typename K, typename H>
void ht_rehash(hash_table_t<K, H>* ht);

template <typename K, typename H>
void ht_reserve(hash_table_t<K, H>* ht, int num);

template <typename K, typename H>
void ht_reserve_if_necessary(hash_table_t<K, H>* ht, int num);

// The bool in returned value is false when |key| exists, true otherwise.
template <typename K, typename H>
pair_t<hash_table_it_t<K, H>, bool>
ht_insert_or_get(hash_table_t<K, H>* ht, const K& key);

template <typename K, typename H>
void ht_remove_it(hash_table_t<K, H>* ht, hash_table_it_t<K, H> it);

template <typename K, typename H>
void ht_remove_key(hash_table_t<K, H>* ht, const K& key);

template <typename K, typename H>
bool operator==(const hash_table_t<K, H>& lhs,
                const hash_table_t<K, H>& rhs);

template <typename K, typename H>
bool operator!=(const hash_table_t<K, H>& lhs,
                const hash_table_t<K, H>& rhs);
