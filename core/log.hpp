//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/build.hpp"
#include "core/stack_trace.hpp"
#include "core/string.hpp"
#include <stdio.h>
#include <string.h>

namespace internal {

enum class log_level_e {
  // Should be used for messages with general meaning.
  LEVEL_INFO = 0,
  // Should be used for debug messages, and should be deleted asap.
  // DEBUG messages only appear in debug build.
  LEVEL_DEBUG = 1,
  // Potential errors.
  LEVEL_WARNING = 2,
  // Should be used for critical error.
  LEVEL_FATAL = 3,
};

void log(log_level_e level,
         const char* file,
         int line,
         const char* format,
         ...);

constexpr int gc_log_size = 1024 + gc_max_stack_trace_length;

} // namespace internal

bool log_init(const os_char_t* log_path);

// See log_level_e
#define LOGI(format, ...)                              \
  ::internal::log(::internal::log_level_e::LEVEL_INFO, \
                  __FILE__,                            \
                  __LINE__,                            \
                  format,                              \
                  ##__VA_ARGS__)
#define LOGD(format, ...)                               \
  ::internal::log(::internal::log_level_e::LEVEL_DEBUG, \
                  __FILE__,                             \
                  __LINE__,                             \
                  format,                               \
                  ##__VA_ARGS__)
#define LOGW(format, ...)                                 \
  ::internal::log(::internal::log_level_e::LEVEL_WARNING, \
                  __FILE__,                               \
                  __LINE__,                               \
                  format,                                 \
                  ##__VA_ARGS__)
#define LOGF(format, ...)                               \
  ::internal::log(::internal::log_level_e::LEVEL_FATAL, \
                  __FILE__,                             \
                  __LINE__,                             \
                  format,                               \
                  ##__VA_ARGS__)

#define EZ_STRINGIFY_INTERNAL(condition) #condition
#define EZ_STRINGIFY(condition) EZ_STRINGIFY_INTERNAL(condition)

// // Check if |condition| is false then exit the program and print out the log.
#define CHECKF(condition, format, ...)                      \
  {                                                         \
    if (!(condition)) {                                     \
      ::internal::log(::internal::log_level_e::LEVEL_FATAL, \
                      __FILE__,                             \
                      __LINE__,                             \
                      format,                               \
                      ##__VA_ARGS__);                       \
    }                                                       \
  }

// Same as CHECKF but is disabled in released build.
#if defined(BUILD_DEBUG)
#  define DCHECKF(condition, format, ...) \
    CHECKF(condition, format, ##__VA_ARGS__)
#else
#  define DCHECKF(confition, format, ...)
#endif
