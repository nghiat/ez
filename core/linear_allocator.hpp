//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/allocator.hpp"

struct la_page_t;

/// linear_allocator_t is an implementation of allocator_t.
/// Allocations and free have to be in LIFO order.
struct linear_allocator_t : allocator_t {
  static constexpr size_t sc_stack_page_size = 4096;
  uint8_t stack_page[sc_stack_page_size];
  size_t default_page_size;
  la_page_t* current_page = nullptr;
  uint8_t* top = nullptr;
};

bool la_init(linear_allocator_t* la,
             const char* name,
             size_t page_size = 1024 * 1024);
