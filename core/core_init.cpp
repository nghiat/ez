//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/core_init.hpp"

#include "core/common_paths.hpp"
#include "core/core_allocators.hpp"
#include "core/log.hpp"
#include "core/mono_time.hpp"
#include "core/stack_trace.hpp"
#include "core/string_builder.hpp"

bool core_init(const os_char_t* log_path) {
  bool rv = true;
  rv &= mtime_init();
  rv &= core_allocators_init();
  rv &= common_paths_init();
  {
    string_builder_t sb;
    strb_init(&sb, g_persistent_allocator, sizeof(os_char_t));
    strb_add_string(&sb, common_paths_get_exe_dir());
    strb_add_string(&sb, log_path);
    os_char_t* full_log_path = (os_char_t*)strb_build(&sb);
    strb_destroy(&sb);
    rv &= log_init(full_log_path);
  }
  rv &= stack_trace_init();
  return rv;
}
