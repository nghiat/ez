//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

typedef void* dynamic_lib_t;

bool dl_open(dynamic_lib_t* dl, const char* name);
void dl_close(dynamic_lib_t* dl);
void* dl_get_proc(dynamic_lib_t* dl, const char* name);
