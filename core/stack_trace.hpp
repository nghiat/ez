//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

namespace internal {

// Maximum number of traces.
constexpr int gc_max_traces = 64;

// Maximum char for each trace.
// Hopfully we don't use more than |gc_max_symbol_length| characters which means
// we have to use heap allocation.
constexpr int gc_max_symbol_length = 1024;

// Maximum char for all traces.
constexpr int gc_max_stack_trace_length = gc_max_traces * gc_max_symbol_length;

} // namespace internal

bool stack_trace_init();
const char* get_stack_trace();
