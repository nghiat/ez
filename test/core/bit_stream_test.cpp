//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/bit_stream.hpp"

#include "3rdparty/Catch2/catch.hpp"

TEST_CASE("core/bit_stream") {
  uint8_t data[2];
  data[0] = 0b01010101;
  data[1] = 0b10101010;
  bit_stream_t bs;
  bs_init(&bs, data);
  REQUIRE(bs_read_lsb(&bs, 1) == 1);
  REQUIRE(bs_read_msb(&bs, 1) == 1);
  REQUIRE(bs_read_lsb(&bs, 2) == 1);
  REQUIRE(bs_read_msb(&bs, 2) == 0b10);
  REQUIRE(bs_read_lsb(&bs, 3) == 0b101);
  REQUIRE(bs_read_msb(&bs, 3) == 0b101);
  bs_skip(&bs, 4);
  REQUIRE(bs_consume_lsb(&bs, 5) == 0b00101);
  REQUIRE(bs_consume_msb(&bs, 4) == 0b1010);
  REQUIRE(bs_read_lsb(&bs, 3) == 0b101);
  REQUIRE(bs_read_msb(&bs, 3) == 0b101);
}
