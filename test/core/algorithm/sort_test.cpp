//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/algorithm/sort.inl"

#include "3rdparty/Catch2/catch.hpp"
#include "core/linear_allocator.hpp"
#include "core/log.hpp"
#include "core/mono_time.hpp"
#include <algorithm>
#include <stdlib.h>

TEST_CASE("core/algorithm/sort") {
  linear_allocator_t la;
  la_init(&la, "core_algorithm_sort_test");
  {
    constexpr int count = 999999;
    int* arr1 = (int*)allocator_alloc(&la, count * sizeof(int));
    for (int i = 0; i < count; ++i)
      arr1[i] = rand();
    int* arr2 = (int*)allocator_alloc(&la, count * sizeof(int));
    memcpy(arr2, arr1, count * sizeof(int));
    mono_time_t t = mtime_now();
    std::sort(arr1, arr1 + count);
    LOGI("std::sort with %d ints: %f ms", count, mtime_to_ms(mtime_now() - t));
    t = mtime_now();
    algo_merge_sort(arr2, count);
    LOGI("algo_merge_sort with %d ints: %f ms",
         count,
         mtime_to_ms(mtime_now() - t));
    REQUIRE_FALSE(memcmp(arr1, arr2, count * sizeof(int)));
  }
}
