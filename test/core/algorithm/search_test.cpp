//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/algorithm/search.inl"

#include "3rdparty/Catch2/catch.hpp"

TEST_CASE("core/algorithm/search") {
  {
    int array1[] = {0, 1, 2, 3, 4, 5};
    int array2[] = {3, 4, 5};
    REQUIRE(
        algo_search(&array1[0], &array1[0] + 6, &array2[0], &array2[0] + 3) ==
        &array1[0] + 3);
    REQUIRE(algo_search_fast(
                &array1[0], &array1[0] + 6, &array2[0], &array2[0] + 3) ==
            &array1[0] + 3);
  }
}
