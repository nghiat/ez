//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/file.hpp"

#include "3rdparty/Catch2/catch.hpp"
#include "core/compile_time_util.inl"
#include "core/free_list_allocator.hpp"
#include <string.h>

TEST_CASE("core/file") {
  const os_char_t* c_txt_file_name = OS_LIT("test_f.txt");
  const char c_string1[] = "abc";
  const char c_string2[] = "def";
  const char c_string12[] = "abcdef";
  constexpr size_t c_string1_len = array_length(c_string1) - 1;
  constexpr size_t c_string2_len = array_length(c_string2) - 1;
  constexpr size_t c_string12_len = c_string1_len + c_string2_len;
  {
    {
      file_t f;
      file_open(&f, c_txt_file_name, file_t::MODE_WRITE);
      REQUIRE(file_is_valid(&f));
      file_write(&f, c_string1_len, (void*)c_string1);
      file_close(&f);
    }
    {
      file_t f;
      file_open(&f, c_txt_file_name, file_t::MODE_READ);
      REQUIRE(file_is_valid(&f));
      REQUIRE(file_get_size(&f) == c_string1_len);
      char buffer[c_string1_len] = {};
      file_read(&f, buffer, c_string1_len);
      REQUIRE_FALSE(memcmp(buffer, c_string1, c_string1_len));
      file_close(&f);
    }
    {
      file_t f;
      file_open(&f, c_txt_file_name, file_t::MODE_APPEND);
      file_write(&f, c_string2_len, (void*)c_string2);
      file_seek(&f, file_t::FROM_BEGIN, 0);
      char buffer[c_string12_len];
      file_read(&f, buffer, c_string12_len);
      REQUIRE_FALSE(memcmp(buffer, c_string12, c_string12_len));
      file_close(&f);
    }
    {
      // This should be the last test.
      file_t f;
      file_open(&f, c_txt_file_name, file_t::MODE_READ);
      file_close(&f);
      file_delete(&f);
      file_t same_file;
      file_open(&same_file, c_txt_file_name, file_t::MODE_READ);
      REQUIRE_FALSE(file_is_valid(&same_file));
      file_close(&same_file);
    }
  }
}
