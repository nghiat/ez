//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/common_paths.hpp"

#include "3rdparty/Catch2/catch.hpp"
#include "core/platform.hpp"
#include <string.h>

TEST_CASE("core/common_paths") {
  const os_char_t* exe_path = common_paths_get_exe_path();
#if defined(OS_WIN)
  const wchar_t c_exe_name[] = L"test.exe";
#else
  const char c_exe_name[] = "test";
#endif
  const os_char_t* p = str_find_substr(exe_path, c_exe_name);
  REQUIRE(p);
  const os_char_t* exe_dir = common_paths_get_exe_dir();
  REQUIRE(!memcmp(exe_path, exe_dir, str_get_len(exe_dir) * sizeof(os_char_t)));
}
