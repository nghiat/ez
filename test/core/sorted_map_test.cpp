//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyrigsa (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/sorted_map.inl"

#include "3rdparty/Catch2/catch.hpp"
#include "core/free_list_allocator.hpp"
#include <map>
#include <stdlib.h>

namespace core {
namespace test {

TEST_CASE("core/sorted_map") {
  free_list_allocator_t allocator;
  fla_init(&allocator, "sorted_map_test", 10240);
  {
    sorted_map_t<int, int> map;
    sa_init(&map, &allocator);
    REQUIRE(allocator.used_size == 0);
  }
  {
    sorted_map_t<int, int> map;
    sa_init(&map, &allocator);
    constexpr int c_count = 100;
    int sum = 0;
    for (int i = 0; i < c_count / 2; ++i)
      sa_insert_or_get(&map, i, i);
    for (int i = c_count - 1; i >= c_count / 2; --i)
      sa_insert_or_get(&map, i, i);
    int last = -1;
    bool ok = true;
    for (auto pair : map) {
      int k = pair.value;
      int v = pair.value;
      if (k <= last || v <= last) {
        ok = false;
        break;
      }
      last = k;
      sum += k + v;
    }
    sa_destroy(&map);
    REQUIRE(ok);
    REQUIRE(sum == c_count * (c_count - 1));
    REQUIRE(allocator.used_size == 0);
  }
  {
    sorted_map_t<int, int> map;
    sa_init(&map, &allocator);
    std::map<int, int> std_map;
    constexpr int c_count = 100;
    for (int i = 0; i < c_count; ++i) {
      int val = rand();
      sa_insert_or_get(&map, val, val);
      std_map[val] = val;
    }
    int correct_sum = 0;
    for (auto it : std_map)
      correct_sum += it.first + it.second;
    int sum = 0;
    for (auto it : map) {
      sum += it.key + it.value;
    }
    sa_destroy(&map);
    REQUIRE(sum == correct_sum);
  }
  {
    sorted_map_t<int, int> map;
    sa_init(&map, &allocator);
    sa_insert_or_get(&map, 1, 1);
    sa_insert_or_get(&map, 3, 3);
    sa_insert_or_get(&map, 2, 2);
    sa_insert_or_get(&map, 4, 4);
    sa_remove_key(&map, 2);
    sa_remove_key(&map, 1);
    sa_remove_key(&map, 4);
    sa_insert_or_get(&map, 1, -1);
    REQUIRE(sa_find(&map, 1)->value == -1);
    REQUIRE(sa_find(&map, 3)->value == 3);

    sa_remove_it(&map, sa_insert_or_get(&map, 5, 5).first);
    sa_remove_it(&map, sa_insert_or_get(&map, 6, 6).first);
    REQUIRE(sa_length(&map) == 2);
    auto it = begin(map);
    REQUIRE(it->value == -1);
    REQUIRE((++it)->value == 3);
    REQUIRE(sa_find(&map, 5) == end(map));
    REQUIRE(sa_find(&map, 6) == end(map));
    sa_destroy(&map);
  }
  allocator_destroy(&allocator);
}

} // namespace test
} // namespace core
