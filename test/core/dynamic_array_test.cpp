//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "dynamic_array_test.inl"

#include "core/allocator_internal.hpp"
#include "core/free_list_allocator.hpp"

TEST_CASE("core/dynamic_array") {
  free_list_allocator_t allocator;
  fla_init(&allocator, "dynamic_array_test", 1024 * 1024 * 1024);
  {
    dynamic_array_t<int8_t> i8_array;
    da_init(&i8_array, &allocator);
    for (int i = 0; i < 100; ++i) {
      da_append(&i8_array, i);
    }
    da_destroy(&i8_array);
    REQUIRE(allocator.used_size == 0);
  }

  {
    dynamic_array_t<int8_t> i8_array;
    da_init(&i8_array, &allocator);
    constexpr size_t small_elems_num = 5;
    constexpr size_t big_elems_num = 100;
    for (int i = 0; i < 1000; ++i) {
      da_reserve(&i8_array, small_elems_num);
      da_reserve(&i8_array, big_elems_num);
    }
    REQUIRE(i8_array.capacity == big_elems_num);
    da_destroy(&i8_array);
  }

  {
    test_unsigned_type<uint8_t>(&allocator);
    test_unsigned_type<uint16_t>(&allocator);
    test_unsigned_type<uint32_t>(&allocator);
    test_unsigned_type<uint64_t>(&allocator);
    test_unsigned_type<float>(&allocator);
    test_unsigned_type<double>(&allocator);

    test_signed_type<int8_t>(&allocator);
    test_signed_type<int16_t>(&allocator);
    test_signed_type<int32_t>(&allocator);
    test_signed_type<int64_t>(&allocator);
  }

  {
    dynamic_array_t<uint8_t> array;
    da_init(&array, &allocator);
    REQUIRE(array.length == 0);
    constexpr size_t c_count = 10;
    for (size_t i = 0; i < c_count; ++i)
      da_append(&array, (uint8_t)i);
    REQUIRE(array.length == c_count);
    REQUIRE(array.capacity == c_count);
    REQUIRE(array.length);
    da_destroy(&array);
  }

  {
    dynamic_array_t<int> array;
    da_init(&array, &allocator);
    for (int i = 0; i < 1000; ++i)
      da_append(&array, i);

    da_remove_at(&array, 500);
    REQUIRE(array.length == 999);
    REQUIRE(array.p[499] == 499);
    REQUIRE(array.p[500] == 501);

    da_remove_range(&array, 0, 3);
    REQUIRE(array.length == 996);
    REQUIRE(array.p[0] == 3);

    da_remove_at(&array, 0);
    REQUIRE(array.length == 995);
    REQUIRE(array.p[0] == 4);
    da_destroy(&array);
  }

  {
    constexpr size_t c_count = 10;
    dynamic_array_t<uint8_t> array;
    da_init(&array, &allocator);
    for (size_t i = 0; i < c_count; ++i)
      da_append(&array, i);
    constexpr size_t c_sum = (c_count - 1) * c_count / 2;
    {
      size_t sum = 0;
      for (uint8_t num : array)
        sum += num;
      REQUIRE(sum == c_sum);
    }

    // Last of this test case.
    {
      bool ok = true;
      for (uint8_t& num : array)
        num = 0;
      for (uint8_t num : array)
        if (num)
          ok = false;
      REQUIRE(ok);
    }
    da_destroy(&array);
  }
  {
    dynamic_array_t<uint8_t> array;
    da_init(&array, &allocator);
    constexpr size_t cNum = 8;
    size_t old_used_size = allocator.used_size;
    da_resize(&array, cNum);
    REQUIRE(array.length == 8);
    REQUIRE(allocator.used_size ==
            old_used_size + cNum + sizeof(allocation_header_t));
    da_destroy(&array);
  }
}
