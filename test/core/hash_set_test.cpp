//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/hash_set.inl"

#include "3rdparty/Catch2/catch.hpp"
#include "core/free_list_allocator.hpp"
#include <stdlib.h>
#include <unordered_set>

namespace core {
namespace test {

TEST_CASE("core/hash_set") {
  free_list_allocator_t allocator;
  fla_init(&allocator, "hash_set_test_allocator", 10240);
  {
    hash_set_t<int8_t> i8_set;
    ht_init(&i8_set, &allocator);
    hash_set_t<int16_t> i16_set;
    ht_init(&i16_set, &allocator);
    hash_set_t<int32_t> i32_set;
    ht_init(&i32_set, &allocator);
    hash_set_t<int64_t> i64_set;
    ht_init(&i64_set, &allocator);
    hash_set_t<uint8_t> u8_set;
    ht_init(&u8_set, &allocator);
    hash_set_t<uint16_t> u16_set;
    ht_init(&u16_set, &allocator);
    hash_set_t<uint32_t> u32_set;
    ht_init(&u32_set, &allocator);
    hash_set_t<uint64_t> u64_set;
    ht_init(&u64_set, &allocator);
    hash_set_t<float> float_set;
    ht_init(&float_set, &allocator);
    hash_set_t<double> double_set;
    ht_init(&double_set, &allocator);
    REQUIRE(allocator.used_size == 0);
    ht_destroy(&i8_set);
    ht_destroy(&i16_set);
    ht_destroy(&i32_set);
    ht_destroy(&i64_set);
    ht_destroy(&u8_set);
    ht_destroy(&u16_set);
    ht_destroy(&u32_set);
    ht_destroy(&u64_set);
    ht_destroy(&float_set);
    ht_destroy(&double_set);
  }
  {
    hash_set_t<int> set;
    ht_init(&set, &allocator);
    constexpr int c_count = 100;
    int sum = 0;
    for (int i = 0; i < c_count; ++i)
      ht_insert_or_get(&set, i);
    for (int i : set) {
      sum += i;
    }
    ht_destroy(&set);
    REQUIRE(sum == c_count * (c_count - 1) / 2);
    REQUIRE(allocator.used_size == 0);
  }
  {
    hash_set_t<int> set;
    std::unordered_set<int> std_set;
    ht_init(&set, &allocator);
    constexpr int c_count = 100;
    for (int i = 0; i < c_count; ++i) {
      int val = rand();
      ht_insert_or_get(&set, val);
      std_set.insert(val);
    }
    int correct_sum = 0;
    for (int i : std_set)
      correct_sum += i;
    int sum = 0;
    for (int i : set) {
      sum += i;
    }
    ht_destroy(&set);
    REQUIRE(sum == correct_sum);
  }
  {
    hash_set_t<int> set;
    ht_init(&set, &allocator);
    ht_insert_or_get(&set, 1);
    ht_insert_or_get(&set, 3);
    ht_insert_or_get(&set, 2);
    ht_insert_or_get(&set, 4);
    ht_remove_key(&set, 2);
    ht_remove_key(&set, 1);
    ht_remove_key(&set, 4);
    ht_insert_or_get(&set, 1);
    REQUIRE(ht_find(&set, 1) != end(set));
    REQUIRE(ht_find(&set, 3) != end(set));

    ht_remove_it(&set, ht_insert_or_get(&set, 5).first);
    ht_remove_it(&set, ht_insert_or_get(&set, 6).first);
    REQUIRE(set.length == 2);
    REQUIRE(ht_find(&set, 5) == end(set));
    REQUIRE(ht_find(&set, 6) == end(set));
    ht_destroy(&set);
  }
  allocator_destroy(&allocator);
}

} // namespace test
} // namespace core
