//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/mono_time.hpp"

#include "3rdparty/Catch2/catch.hpp"
#include "core/platform.hpp"

#if defined(OS_WIN)
#  include <Windows.h>
#  define sleep(ms) Sleep(ms)
#else
#  include <unistd.h>
#  define sleep(ms) usleep(ms * 1000)
#endif

TEST_CASE("core/mono_time") {
  mono_time_t t1 = mtime_now();
  sleep(1000);
  int64_t t2 = mtime_now();
  int64_t delta = t2 - t1;
  REQUIRE(mtime_to_s(delta) >= 1.0);
  REQUIRE(mtime_to_ms(delta) >= 1000.0);
  REQUIRE(mtime_to_ms(delta) <= 1100.0);
  REQUIRE(mtime_to_us(delta) >= 1000 * 1000.0);
  REQUIRE(mtime_to_us(delta) <= 1100 * 1000.0);
}
