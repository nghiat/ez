//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/regex.hpp"

#include "3rdparty/Catch2/catch.hpp"
#include "core/core_allocators.hpp"
#include "core/linear_allocator.hpp"

TEST_CASE("core/regex") {
  linear_allocator_t allocator;
  la_init(&allocator, "regex_test_allocator");
  {
    regex_tree_node_t* n = regex_build_tree(&allocator, "(((a)))");
    REQUIRE(n->v.c == 'a');
  }
  {
    regex_tree_node_t* n = regex_build_tree(&allocator, "ab|cd");
    REQUIRE(n->v.op == regex_op_e::ALTERNATION);
    REQUIRE(n->l->v.op == regex_op_e::APPEND);
    REQUIRE(n->l->l->v.c == 'a');
    REQUIRE(n->l->r->v.c == 'b');
    REQUIRE(n->r->v.op == regex_op_e::APPEND);
    REQUIRE(n->r->l->v.c == 'c');
    REQUIRE(n->r->r->v.c == 'd');
  }
  {
    regex_tree_node_t* n = regex_build_tree(&allocator, "a|b|c|d");
    REQUIRE(n->v.op == regex_op_e::ALTERNATION);
    REQUIRE(n->l->v.c == 'a');
    REQUIRE(n->r->v.op == regex_op_e::ALTERNATION);
    REQUIRE(n->r->l->v.c == 'b');
    REQUIRE(n->r->r->v.op == regex_op_e::ALTERNATION);
    REQUIRE(n->r->r->l->v.c == 'c');
    REQUIRE(n->r->r->r->v.c == 'd');
  }
  {
    regex_tree_node_t* n = regex_build_tree(&allocator, "ab|cd|ef");
    REQUIRE(n->v.op == regex_op_e::ALTERNATION);
    REQUIRE(n->l->v.op == regex_op_e::APPEND);
    REQUIRE(n->l->l->v.c == 'a');
    REQUIRE(n->l->r->v.c == 'b');
    REQUIRE(n->r->v.op == regex_op_e::ALTERNATION);
    REQUIRE(n->r->l->v.op == regex_op_e::APPEND);
    REQUIRE(n->r->l->l->v.c == 'c');
    REQUIRE(n->r->l->r->v.c == 'd');
    REQUIRE(n->r->r->v.op == regex_op_e::APPEND);
    REQUIRE(n->r->r->l->v.c == 'e');
    REQUIRE(n->r->r->r->v.c == 'f');
  }
  {
    regex_tree_node_t* n = regex_build_tree(&allocator, "a(b|c)d");
    REQUIRE(n->v.op == regex_op_e::APPEND);
    REQUIRE(n->l->v.op == regex_op_e::APPEND);
    REQUIRE(n->l->l->v.c == 'a');
    REQUIRE(n->l->r->v.op == regex_op_e::ALTERNATION);
    REQUIRE(n->l->r->l->v.c == 'b');
    REQUIRE(n->l->r->r->v.c == 'c');
    REQUIRE(n->r->v.c == 'd');
  }
  {
    regex_tree_node_t* n = regex_build_tree(&allocator, "(ab|cd)ef|gh");
    REQUIRE(n->v.op == regex_op_e::ALTERNATION);
    REQUIRE(n->l->v.op == regex_op_e::APPEND);
    REQUIRE(n->l->l->v.op == regex_op_e::APPEND);
    REQUIRE(n->l->l->l->v.op == regex_op_e::ALTERNATION);
    REQUIRE(n->l->l->l->l->v.op == regex_op_e::APPEND);
    REQUIRE(n->l->l->l->l->l->v.c == 'a');
    REQUIRE(n->l->l->l->l->r->v.c == 'b');
    REQUIRE(n->l->l->l->r->v.op == regex_op_e::APPEND);
    REQUIRE(n->l->l->l->r->l->v.c == 'c');
    REQUIRE(n->l->l->l->r->r->v.c == 'd');
    REQUIRE(n->l->l->r->v.c == 'e');
    REQUIRE(n->l->r->v.c == 'f');
    REQUIRE(n->r->v.op == regex_op_e::APPEND);
    REQUIRE(n->r->l->v.c == 'g');
    REQUIRE(n->r->r->v.c == 'h');
  }
  {
    regex_tree_node_t* n = regex_build_tree(&allocator, "a(b|c)*");
    REQUIRE(n->v.op == regex_op_e::APPEND);
    REQUIRE(n->l->v.c == 'a');
    REQUIRE(n->r->v.op == regex_op_e::STAR);
    REQUIRE(n->r->l->v.op == regex_op_e::ALTERNATION);
    REQUIRE(n->r->l->l->v.c == 'b');
    REQUIRE(n->r->l->r->v.c == 'c');
  }
  {
    regex_tree_node_t* n = regex_build_tree(&allocator, "(a(b|c))*");
    REQUIRE(n->v.op == regex_op_e::STAR);
    REQUIRE(n->l->v.op == regex_op_e::APPEND);
    REQUIRE(n->l->l->v.c == 'a');
    REQUIRE(n->l->r->v.op == regex_op_e::ALTERNATION);
    REQUIRE(n->l->r->l->v.c == 'b');
    REQUIRE(n->l->r->r->v.c == 'c');
  }
  {
    regex_tree_node_t* n = regex_build_tree(&allocator, "a*b*");
    REQUIRE(n->v.op == regex_op_e::APPEND);
    REQUIRE(n->l->v.op == regex_op_e::STAR);
    REQUIRE(n->l->l->v.c == 'a');
    REQUIRE(n->r->v.op == regex_op_e::STAR);
    REQUIRE(n->r->l->v.c == 'b');
  }
  {
    regex_tree_node_t* n = regex_build_tree(&allocator, "a[bc-ef]");
    REQUIRE(n->v.op == regex_op_e::APPEND);
    REQUIRE(n->l->v.c == 'a');
    REQUIRE(n->r->v.op == regex_op_e::ALTERNATION);
    REQUIRE(n->r->r->v.c == 'f');
    REQUIRE(n->r->l->v.op == regex_op_e::ALTERNATION);
    REQUIRE(n->r->l->r->v.c == 'e');
    REQUIRE(n->r->l->l->v.op == regex_op_e::ALTERNATION);
    REQUIRE(n->r->l->l->r->v.c == 'd');
    REQUIRE(n->r->l->l->l->v.op == regex_op_e::ALTERNATION);
    REQUIRE(n->r->l->l->l->r->v.c == 'c');
    REQUIRE(n->r->l->l->l->l->v.c == 'b');
  }
  // {
  //   regex_tree_node_t* n =
  //       regex_build_tree(&allocator, "((a))*((b|(cd))|(e)*)");
  //   REQUIRE(n->v == regex_op_e::APPEND);
  //   REQUIRE(n->l->v == regex_op_e::STAR);
  //   REQUIRE(n->l->l->v == 'a');
  //   REQUIRE(n->r->v == regex_op_e::ALTERNATION);
  //   REQUIRE(n->r->r->v == regex_op_e::STAR);
  //   REQUIRE(n->r->r->l->v == 'e');
  //   REQUIRE(n->r->l->v == regex_op_e::ALTERNATION);
  //   REQUIRE(n->r->l->l->v == 'b');
  //   REQUIRE(n->r->l->r->v == regex_op_e::APPEND);
  //   REQUIRE(n->r->l->r->l->v == 'c');
  //   REQUIRE(n->r->l->r->r->v == 'd');
  // }

  {
  // /*
  //                             e
  //                    <-------------------
  //                    |        b         |
  //                    |     n4--->n5     |
  //                    |  e /        \ e  |
  //     a     e     e  |   /          \   | e
  //  no--->n1--->n2--->n3--            --n8--->n9
  //               |        \          /        |
  //               |       e \   c    / e       |
  //               |          n6--->n7          |
  //               ----------------------------->
  //                             e
  // */
    regex_t re;
    regex_init(&re, g_general_allocator, "a(b|c)*");
    REQUIRE(regex_match(&re, "ab"));
    REQUIRE(regex_match(&re, "ac"));
    REQUIRE(regex_match(&re, "abb"));
    REQUIRE(regex_match(&re, "acc"));
    REQUIRE(regex_match(&re, "abc"));
    REQUIRE(regex_match(&re, "acb"));
    REQUIRE_FALSE(regex_match(&re, "aa"));
    REQUIRE_FALSE(regex_match(&re, "aba"));
  }

  {
    regex_t re;
    regex_init(&re, g_general_allocator, "a(b|c)*b");
    REQUIRE(regex_match(&re, "ab"));
    REQUIRE(regex_match(&re, "abb"));
    REQUIRE(regex_match(&re, "acb"));
    REQUIRE_FALSE(regex_match(&re, "aa"));
    REQUIRE_FALSE(regex_match(&re, "ac"));
    REQUIRE_FALSE(regex_match(&re, "abc"));
  }

  {
    regex_t re;
    regex_init(&re, g_general_allocator, "ac+");
    REQUIRE(regex_match(&re, "ac"));
    REQUIRE(regex_match(&re, "acc"));
    REQUIRE_FALSE(regex_match(&re, "a"));
  }
}
