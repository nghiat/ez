//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/dynamic_array.inl"

#include "3rdparty/Catch2/catch.hpp"
#include <limits>

template <typename T>
constexpr T max_for_testing() {
  return std::numeric_limits<T>::max() < 100000 ? std::numeric_limits<T>::max()
                                                : 100000;
}

template <typename T>
void test_unsigned_type(allocator_t* allocator) {
  constexpr T c_elems_num = max_for_testing<T>();
  dynamic_array_t<T> array;
  da_init(&array, allocator);
  dynamic_array_t<T> array_reversed;
  da_init(&array_reversed, allocator);
  // < not <= to prevent overflow in the last ++i.
  for (T i = 0; i < c_elems_num; ++i) {
    da_append(&array, i);
    da_insert_at(&array_reversed, 0, c_elems_num - 1 - i);
  }
  bool ok = true;
  for (size_t i = 0; i < c_elems_num; ++i) {
    if (array.p[i] != i || array_reversed.p[i] != i) {
      ok = false;
      break;
    }
  }
  REQUIRE(ok);
  da_destroy(&array);
  da_destroy(&array_reversed);
}

template <typename T>
void test_signed_type(allocator_t* allocator) {
  constexpr T c_max_positive = max_for_testing<T>();
  dynamic_array_t<T> array;
  da_init(&array, allocator);
  dynamic_array_t<T> array_reversed;
  da_init(&array_reversed, allocator);
  // < not <= to prevent overflow in the last ++i.
  // And we start from -c_max_positive + 1 so the range is symmetry around 0.
  for (int64_t i = -c_max_positive + 1; i < c_max_positive; ++i) {
    da_append(&array, i);
    da_insert_at(&array_reversed, 0, -i);
  }

  bool ok = true;
  for (int64_t i = -c_max_positive + 1; i < c_max_positive; ++i) {
    size_t current_idx = (size_t)(i + c_max_positive - 1);
    if (array.p[current_idx] != i || array_reversed.p[current_idx] != i) {
      ok = false;
      break;
    }
  }
  REQUIRE(ok);
  da_destroy(&array);
  da_destroy(&array_reversed);
}
