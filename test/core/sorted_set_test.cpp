//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyrigsa (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/sorted_set.inl"

#include "3rdparty/Catch2/catch.hpp"
#include "core/free_list_allocator.hpp"
#include <set>
#include <stdlib.h>

namespace core {
namespace test {

TEST_CASE("core/sorted_set") {
  free_list_allocator_t allocator;
  fla_init(&allocator, "sorted_set_test", 10240);
  {
    sorted_set_t<int> set;
    sa_init(&set, &allocator);
    REQUIRE(allocator.used_size == 0);
    constexpr int c_count = 200;
    int sum = 0;
    for (int i = 0; i < c_count / 2; ++i)
      sa_insert_or_get(&set, i);
    for (int i = c_count - 1; i >= c_count / 2; --i)
      sa_insert_or_get(&set, i);
    int last = -1;
    bool ok = true;
    for (int i : set) {
      if (i <= last) {
        ok = false;
        break;
      }
      last = i;
      sum += i;
    }
    sa_destroy(&set);
    REQUIRE(ok);
    REQUIRE(sum == c_count * (c_count - 1) / 2);
    REQUIRE(allocator.used_size == 0);
  }
  {
    sorted_set_t<int> set;
    sa_init(&set, &allocator);
    std::set<int> std_set;
    constexpr int c_count = 200;
    for (int i = 0; i < c_count; ++i) {
      int val = rand();
      sa_insert_or_get(&set, val);
      std_set.insert(val);
    }
    int correct_sum = 0;
    for (int i : std_set)
      correct_sum += i;
    int sum = 0;
    for (int i : set) {
      sum += i;
    }
    sa_destroy(&set);
    REQUIRE(sum == correct_sum);
  }
  allocator_destroy(&allocator);
}

} // namespace test
} // namespace core
