//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/linear_allocator.hpp"

#include "3rdparty/Catch2/catch.hpp"

TEST_CASE("core/linear_allocator") {
  SECTION("Multiple 1 byte allocation") {
    // In this test we alloc multiple 1 bytes and assign different values for
    // each bytes.
    linear_allocator_t allocator;
    la_init(&allocator, "linear_allocator_t", 10240);
    constexpr size_t c_num = 64;
    uint8_t* array[c_num];
    for (size_t i = 0; i < c_num; ++i) {
      array[i] = (uint8_t*)allocator_alloc(&allocator, 1);
      *array[i] = i;
    }
    uint16_t sum = 0;
    for (size_t i = 0; i < c_num; ++i) {
      sum += *array[i];
    }
    size_t correct_sum = c_num * (c_num - 1) / 2;
    REQUIRE(sum == correct_sum);
    allocator_destroy(&allocator);
  }

  SECTION("alloc nullptr") {
    linear_allocator_t allocator;
    la_init(&allocator, "linear_allocator_t", 1024);
    size_t current_used_size = allocator.used_size;
    REQUIRE_FALSE(allocator_alloc(&allocator, 0));
    REQUIRE(allocator.used_size == current_used_size);
    allocator_destroy(&allocator);
  }

  SECTION("Zero size") {
    linear_allocator_t allocator;
    la_init(&allocator, "linear_allocator_t", 128);
    REQUIRE_FALSE(allocator_alloc(&allocator, 0));
    REQUIRE_FALSE(allocator_aligned_alloc(&allocator, 0, 2));
    allocator_destroy(&allocator);
  }

  SECTION("Aligned Allocation") {
    constexpr size_t alignment = 512;
    linear_allocator_t allocator;
    la_init(&allocator, "linear_allocator_t", 2048);
    allocator_alloc(&allocator, 1);
    void* aligned_pointer =
        allocator_aligned_alloc(&allocator, alignment, alignment);
    REQUIRE(aligned_pointer);
    REQUIRE_FALSE((size_t)aligned_pointer % alignment);
    allocator_destroy(&allocator);
  }

  SECTION("Non power of two alignment and zero alignment") {
    linear_allocator_t allocator;
    la_init(&allocator, "linear_allocator_t", 128);
    REQUIRE_FALSE(allocator_aligned_alloc(&allocator, 1, 0));
    REQUIRE_FALSE(allocator_aligned_alloc(&allocator, 1, 3));
    allocator_destroy(&allocator);
  }

  SECTION("alloc & free") {
    linear_allocator_t allocator;
    la_init(&allocator, "linear_allocator_t", 1024);
    size_t initial_used_size = allocator.used_size;
    int64_t* qword1 = (int64_t*)allocator_alloc(&allocator, sizeof(int64_t));
    int64_t* qword2 = (int64_t*)allocator_alloc(&allocator, sizeof(int64_t));
    *qword1 = 1111;
    *qword2 = 2222;
    REQUIRE(*qword1 == 1111);
    REQUIRE(*qword2 == 2222);

    allocator_free(&allocator, qword2);
    int64_t* qword3 = (int64_t*)allocator_alloc(&allocator, sizeof(int64_t));
    *qword3 = 3333;
    REQUIRE(qword2 == qword3);
    REQUIRE(*qword1 == 1111);
    REQUIRE(*qword3 == 3333);

    allocator_free(&allocator, qword3);
    int64_t* qword4 =
        (int64_t*)allocator_aligned_alloc(&allocator, sizeof(int64_t), 64);
    *qword4 = 4444;
    REQUIRE(*qword1 == 1111);
    REQUIRE(*qword4 == 4444);

    allocator_free(&allocator, qword4);
    int64_t* qword5 =
        (int64_t*)allocator_aligned_alloc(&allocator, sizeof(int64_t), 64);
    *qword5 = 5555;
    REQUIRE(qword4 == qword5);
    REQUIRE(*qword1 == 1111);
    REQUIRE(*qword5 == 5555);

    // Cleaup
    allocator_free(&allocator, qword5);
    allocator_free(&allocator, qword1);
    REQUIRE(allocator.used_size == initial_used_size);
    allocator_destroy(&allocator);
  }

  SECTION("realloc") {
    linear_allocator_t allocator;
    la_init(&allocator, "linear_allocator_t4", 1024);
    void* not_top_of_stack_pointer = allocator_alloc(&allocator, 128);
    void* top_of_stack_pointer = allocator_alloc(&allocator, 128);
    /// Invalid argument.
    REQUIRE_FALSE(allocator_realloc(&allocator, nullptr, 0));
    REQUIRE_FALSE(allocator_realloc(&allocator, top_of_stack_pointer, 0));
    /// Not the top of the stack pointer.
    top_of_stack_pointer =
        allocator_realloc(&allocator, not_top_of_stack_pointer, 256);
    REQUIRE(top_of_stack_pointer);

    // Same size.
    REQUIRE(allocator_realloc(&allocator, top_of_stack_pointer, 128) ==
            top_of_stack_pointer);
    REQUIRE(allocator_realloc(&allocator, top_of_stack_pointer, 256) ==
            top_of_stack_pointer);
    allocator_destroy(&allocator);
  }
}
