//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/string_builder.hpp"

#include "3rdparty/Catch2/catch.hpp"
#include "core/free_list_allocator.hpp"
#include "core/string.hpp"

TEST_CASE("core/string_builder") {
  free_list_allocator_t allocator;
  fla_init(&allocator, "string_builder_test", 1024);
  {
    string_builder_t sb;
    strb_init(&sb, &allocator, 1);
    strb_add_string(&sb, "abc");
    strb_add_string(&sb, "def");
    strb_add_string(&sb, "");
    char* str = (char*)strb_build(&sb);
    REQUIRE(str_get_len(str) == 6);
    REQUIRE(str_compare(str, "abcdef"));
    strb_destroy(&sb);
  }
  {
    string_builder_t sb;
    strb_init(&sb, &allocator, 2);
    strb_add_string(&sb, L"abc");
    strb_add_string(&sb, L"def");
    strb_add_string(&sb, L"");
    wchar_t* str = (wchar_t*)strb_build(&sb);
    REQUIRE(str_get_len(str) == 6);
    REQUIRE(str_compare(str, L"abcdef"));
    strb_destroy(&sb);
  }
  allocator_destroy(&allocator);
}
