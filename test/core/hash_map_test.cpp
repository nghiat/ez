//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/hash_map.inl"

#include "3rdparty/Catch2/catch.hpp"
#include "core/free_list_allocator.hpp"
#include <stdlib.h>
#include <unordered_map>

namespace core {
namespace test {

TEST_CASE("core/hash_map") {
  free_list_allocator_t allocator;
  fla_init(&allocator, "hash_map_test_allocator", 102400);
  {
    hash_map_t<int8_t, int8_t> i8_map;
    ht_init(&i8_map, &allocator);
    hash_map_t<int16_t, int16_t> i16_map;
    ht_init(&i16_map, &allocator);
    hash_map_t<int32_t, int32_t> i32_map;
    ht_init(&i32_map, &allocator);
    hash_map_t<int64_t, int64_t> i64_map;
    ht_init(&i64_map, &allocator);
    hash_map_t<uint8_t, uint8_t> u8_map;
    ht_init(&u8_map, &allocator);
    hash_map_t<uint16_t, uint16_t> u16_map;
    ht_init(&u16_map, &allocator);
    hash_map_t<uint32_t, uint32_t> u32_map;
    ht_init(&u32_map, &allocator);
    hash_map_t<uint64_t, uint64_t> u64_map;
    ht_init(&u64_map, &allocator);
    hash_map_t<float, float> float_map;
    ht_init(&float_map, &allocator);
    hash_map_t<double, double> double_map;
    ht_init(&double_map, &allocator);
    REQUIRE(allocator.used_size == 0);
    ht_destroy(&i8_map);
    ht_destroy(&i16_map);
    ht_destroy(&i32_map);
    ht_destroy(&i64_map);
    ht_destroy(&u8_map);
    ht_destroy(&u16_map);
    ht_destroy(&u32_map);
    ht_destroy(&u64_map);
    ht_destroy(&float_map);
    ht_destroy(&double_map);
  }

  {
    {
      constexpr uint8_t c_count = UINT8_MAX - 1;
      hash_map_t<uint8_t, uint8_t> u8_map;
      ht_init(&u8_map, &allocator);
      for (uint8_t i = 0; i < c_count; ++i) {
        ht_insert_or_get(&u8_map, i, i);
      }
      bool ok = true;
      for (uint8_t i = 0; i < c_count; ++i) {
        auto it = ht_find(&u8_map, i);
        if (!(it != end(u8_map) && it->value == i)) {
          ok = false;
          break;
        }
      }
      REQUIRE(ok);
      REQUIRE(u8_map.length == c_count);
      REQUIRE(ht_find(&u8_map, (uint8_t)(c_count + 1)) == end(u8_map));
      ht_destroy(&u8_map);
    }

    REQUIRE(allocator.used_size == 0);
  }
  {
    hash_map_t<int, int> map;
    ht_init(&map, &allocator);
    constexpr int c_count = 100;
    int sum = 0;
    for (int i = 0; i < c_count; ++i)
      ht_insert_or_get(&map, i, i);
    for (auto it : map) {
      sum += it.key + it.value;
    }
    ht_destroy(&map);
    REQUIRE(sum == c_count * (c_count - 1));
    REQUIRE(allocator.used_size == 0);
  }
  {
    hash_map_t<int, int> map;
    std::unordered_map<int, int> std_map;
    ht_init(&map, &allocator);
    constexpr int c_count = 5000;
    for (int i = 0; i < c_count; ++i) {
      int val = rand();
      ht_insert_or_get(&map, val, val);
      std_map[val] = val;
    }
    int correct_sum = 0;
    for (auto it : std_map)
      correct_sum += it.first + it.second;
    int sum = 0;
    for (auto it : map) {
      sum += it.key + it.value;
    }
    ht_destroy(&map);
    REQUIRE(sum == correct_sum);
  }
  {
    hash_map_t<int, int> map;
    ht_init(&map, &allocator);
    ht_insert_or_get(&map, 1, 1);
    ht_insert_or_get(&map, 3, 3);
    ht_insert_or_get(&map, 2, 2);
    ht_insert_or_get(&map, 4, 4);
    ht_remove_key(&map, 2);
    ht_remove_key(&map, 1);
    ht_remove_key(&map, 4);
    ht_insert_or_get(&map, 1, -1);
    REQUIRE(ht_find(&map, 1)->value == -1);
    REQUIRE(ht_find(&map, 3)->value == 3);

    ht_remove_it(&map, ht_insert_or_get(&map, 5, 5).first);
    ht_remove_it(&map, ht_insert_or_get(&map, 6, 6).first);
    REQUIRE(map.length == 2);
    REQUIRE(ht_find(&map, 5) == end(map));
    REQUIRE(ht_find(&map, 6) == end(map));
    ht_destroy(&map);
  }
  allocator_destroy(&allocator);
}

} // namespace test
} // namespace core
