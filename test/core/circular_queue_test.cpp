//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/circular_queue.inl"

#include "3rdparty/Catch2/catch.hpp"
#include "core/free_list_allocator.hpp"

void require_sum_equal(const circular_queue_t<int>* q, int v) {
  int sum = 0;
  for (int i : *q)
    sum += i;
  REQUIRE(sum == v);
}

TEST_CASE("core/circular_queue") {
  free_list_allocator_t allocator;
  fla_init(&allocator, "circular_queue_test", 1024);
  {
    circular_queue_t<int> q;
    cq_init(&q, &allocator);
    cq_append(&q, 1);
    cq_append(&q, 2);

    require_sum_equal(&q, 3);
    cq_pop(&q);
    require_sum_equal(&q, 2);
    cq_pop(&q);
    require_sum_equal(&q, 0);

    cq_append(&q, 1);
    cq_append(&q, 2);
    cq_append(&q, 3);
    cq_append(&q, 4);

    require_sum_equal(&q, 10);
    cq_pop(&q);
    require_sum_equal(&q, 9);
    cq_pop(&q);
    require_sum_equal(&q, 7);
    cq_pop(&q);
    require_sum_equal(&q, 4);
    cq_pop(&q);
    require_sum_equal(&q, 0);
    cq_destroy(&q);
  }
  REQUIRE(allocator.used_size == 0);
}
