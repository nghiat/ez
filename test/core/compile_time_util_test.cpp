//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/compile_time_util.inl"

#include "3rdparty/Catch2/catch.hpp"

TEST_CASE("core/compile_time_util") {
  {
    int array1[] = {1, 2, 3, 4, 5};
    const char str[] = "12345";
    REQUIRE(array_length(array1) == 5);
    REQUIRE(array_length(str) == 6);
  }
}
