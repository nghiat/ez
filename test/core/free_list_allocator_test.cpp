//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/free_list_allocator.hpp"

#include "3rdparty/Catch2/catch.hpp"
#include "core/allocator_internal.hpp"

TEST_CASE("core/free_list_allocator") {
  SECTION("Array of increasing values") {
    free_list_allocator_t allocator;
    fla_init(&allocator, "free_list_allocator_t", 10240);
    constexpr size_t c_num = 64;
    uint8_t* array[c_num];
    for (size_t i = 0; i < c_num; ++i) {
      array[i] = (uint8_t*)allocator_alloc(&allocator, 1);
      *array[i] = i;
    }
    uint16_t sum = 0;
    for (size_t i = 0; i < c_num; ++i) {
      sum += *array[i];
    }
    REQUIRE(sum == c_num * (c_num - 1) / 2);
    allocator_destroy(&allocator);
  }

  SECTION("Not enough memory") {
    free_list_allocator_t allocator;
    fla_init(&allocator, "free_list_allocator_t", 128);
    REQUIRE_FALSE(allocator_alloc(&allocator, 128));
    REQUIRE_FALSE(allocator_aligned_alloc(&allocator, 128, 2));
    allocator_destroy(&allocator);
  }

  SECTION("Zero size") {
    free_list_allocator_t allocator;
    fla_init(&allocator, "free_list_allocator_t", 128);
    REQUIRE_FALSE(allocator_alloc(&allocator, 0));
    REQUIRE_FALSE(allocator_aligned_alloc(&allocator, 0, 2));
    REQUIRE(allocator.used_size == 0);
    allocator_destroy(&allocator);
  }

  SECTION("Aligned allocation") {
    constexpr size_t alignment = 512;
    free_list_allocator_t allocator;
    fla_init(&allocator, "free_list_allocator_t", 2048);
    allocator_alloc(&allocator, 1);
    void* aligned_pointer =
        allocator_aligned_alloc(&allocator, alignment, alignment);
    REQUIRE(aligned_pointer);
    REQUIRE_FALSE((size_t)(aligned_pointer) % alignment);
    allocator_destroy(&allocator);
  }

  SECTION("Non power of two alignment and zero alignment") {
    free_list_allocator_t allocator;
    fla_init(&allocator, "free_list_allocator_t", 128);
    REQUIRE_FALSE(allocator_aligned_alloc(&allocator, 1, 0));
    REQUIRE_FALSE(allocator_aligned_alloc(&allocator, 1, 3));
    REQUIRE(allocator.used_size == 0);
    allocator_destroy(&allocator);
  }

  SECTION("alloc and free many times") {
    free_list_allocator_t allocator;
    fla_init(&allocator, "free_list_allocator_t", 128);
    bool ok = true;
    for (int i = 0; i < 10000; i++) {
      void* p = nullptr;
      p = allocator_alloc(&allocator, 1);
      if (!p) {
        ok = false;
        break;
      }
      allocator_free(&allocator, p);
    }
    REQUIRE(ok);
    allocator_destroy(&allocator);
  }

  SECTION("alloc & free & check values") {
    free_list_allocator_t allocator;
    fla_init(&allocator, "free_list_allocator_t", 1024);
    int64_t* qword1 = (int64_t*)allocator_alloc(&allocator, sizeof(int64_t));
    int64_t* qword2 = (int64_t*)allocator_alloc(&allocator, sizeof(int64_t));
    *qword1 = 1111;
    *qword2 = 2222;
    REQUIRE(*qword1 == 1111);
    REQUIRE(*qword2 == 2222);

    allocator_free(&allocator, qword2);
    int64_t* qword3 = (int64_t*)allocator_alloc(&allocator, sizeof(int64_t));
    *qword3 = 3333;
    REQUIRE(qword2 == qword3);
    REQUIRE(*qword1 == 1111);
    REQUIRE(*qword3 == 3333);

    allocator_free(&allocator, qword3);
    int64_t* qword4 =
        (int64_t*)allocator_aligned_alloc(&allocator, sizeof(int64_t), 64);
    *qword4 = 4444;
    REQUIRE(*qword1 == 1111);
    REQUIRE(*qword4 == 4444);

    allocator_free(&allocator, qword4);
    int64_t* qword5 =
        (int64_t*)allocator_aligned_alloc(&allocator, sizeof(int64_t), 64);
    *qword5 = 5555;
    REQUIRE(qword4 == qword5);
    REQUIRE(*qword1 == 1111);
    REQUIRE(*qword5 == 5555);
    allocator_destroy(&allocator);
  }

  SECTION("Not enough size for allocation + header") {
    free_list_allocator_t allocator;
    fla_init(&allocator, "free_list_allocator_t", 1024);
    void* p = allocator_alloc(&allocator, 1024);
    REQUIRE_FALSE(p);
    void* aligned_pointer = allocator_aligned_alloc(&allocator, 1024, 8);
    REQUIRE_FALSE(aligned_pointer);
    allocator_destroy(&allocator);
  }

  SECTION("alloc and free with increasing size") {
    free_list_allocator_t allocator;
    fla_init(&allocator, "free_list_allocator_t", 1024);
    void* prev_pointer = allocator_alloc(&allocator, 1);
    const size_t c_max_alloc_size = 100;
    for (size_t i = 2; i <= c_max_alloc_size; ++i) {
      void* p = allocator_alloc(&allocator, i);
      allocator_free(&allocator, prev_pointer);
      prev_pointer = p;
    }
    REQUIRE(prev_pointer);
    allocator_destroy(&allocator);
  }

  SECTION("Enough size for FreeBlock but not "
          "AllocationHeader") {
    constexpr size_t c_allocator_size = 1024;
    free_list_allocator_t allocator;
    fla_init(&allocator, "free_list_allocator_t", c_allocator_size);
    allocator_alloc(&allocator, c_allocator_size - sizeof(allocation_header_t));
    REQUIRE_FALSE(allocator_alloc(&allocator, 1));
    allocator_destroy(&allocator);
  }

  SECTION("free after allocating all space") {
    constexpr size_t c_allocator_size = 1024;
    free_list_allocator_t allocator;
    fla_init(&allocator, "free_list_allocator_t", c_allocator_size);
    void* p = allocator_alloc(&allocator,
                              c_allocator_size - sizeof(allocation_header_t));
    allocator_free(&allocator, p);
    REQUIRE(allocator_alloc(&allocator,
                            c_allocator_size - sizeof(allocation_header_t)));
    allocator_destroy(&allocator);
  }

  SECTION("realloc") {
    constexpr size_t c_allocator_size = 1024;
    {
      // Invalid arguments.
      free_list_allocator_t allocator;
      fla_init(&allocator, "free_list_allocator_t", c_allocator_size);
      void* allocation = allocator_alloc(&allocator, 64);
      REQUIRE_FALSE(allocator_realloc(&allocator, nullptr, 0));
      REQUIRE_FALSE(allocator_realloc(&allocator, allocation, 0));
      allocator_destroy(&allocator);
    }

    {
      // Same size
      free_list_allocator_t allocator;
      fla_init(&allocator, "free_list_allocator_t", c_allocator_size);
      void* allocation = allocator_alloc(&allocator, 64);
      size_t prevUsedSize = allocator.used_size;
      REQUIRE(allocation == allocator_realloc(&allocator, allocation, 64));
      REQUIRE(allocator.used_size == prevUsedSize);
      allocator_destroy(&allocator);
    }

    {
      // Smaller size and the remaining memory can be merged into the following
      // memory region
      free_list_allocator_t allocator;
      fla_init(&allocator, "free_list_allocator_t", c_allocator_size);
      constexpr size_t oldAllocationSize = 64;
      void* allocation = allocator_alloc(&allocator, oldAllocationSize);
      size_t prevUsedSize = allocator.used_size;
      constexpr size_t newAllocationSize = 32;
      REQUIRE(allocation ==
              allocator_realloc(&allocator, allocation, newAllocationSize));
      REQUIRE(allocator.used_size ==
              prevUsedSize - (oldAllocationSize - newAllocationSize));
      allocator_destroy(&allocator);
    }

    {
      // Smaller size and the remaining memory can't be merged into the
      // following memory region and can't create a new free block
      free_list_allocator_t allocator;
      fla_init(&allocator, "free_list_allocator_t", c_allocator_size);
      constexpr size_t oldAllocationSize =
          c_allocator_size - sizeof(allocation_header_t);
      void* allocation = allocator_alloc(&allocator, oldAllocationSize);
      size_t prevUsedSize = allocator.used_size;
      constexpr size_t newAllocationSize = oldAllocationSize - 1;
      REQUIRE(allocation ==
              allocator_realloc(&allocator, allocation, newAllocationSize));
      REQUIRE(allocator.used_size == prevUsedSize);
      allocator_destroy(&allocator);
    }

    {
      // Smaller size and the remaining memory can't be merged into the
      // following memory region but enough for a new FreeBlock
      free_list_allocator_t allocator;
      fla_init(&allocator, "free_list_allocator_t", c_allocator_size);
      constexpr size_t oldAllocationSize =
          c_allocator_size - sizeof(allocation_header_t);
      void* allocation = allocator_alloc(&allocator, oldAllocationSize);
      size_t prevUsedSize = allocator.used_size;
      constexpr size_t newAllocationSize =
          oldAllocationSize - sizeof(allocation_header_t) - 1;
      REQUIRE(allocation ==
              allocator_realloc(&allocator, allocation, newAllocationSize));
      REQUIRE(allocator.used_size ==
              prevUsedSize - sizeof(allocation_header_t) - 1);
      allocator_destroy(&allocator);
    }

    {
      // Bigger size but have enough free memory follows previous allocation
      free_list_allocator_t allocator;
      fla_init(&allocator, "free_list_allocator_t", c_allocator_size);
      void* allocation = allocator_alloc(&allocator, 64);
      REQUIRE(allocation == allocator_realloc(&allocator, allocation, 256));
      allocator_destroy(&allocator);
    }

    {
      // realloc multiple times back and forth
      free_list_allocator_t allocator;
      fla_init(&allocator, "free_list_allocator_t", c_allocator_size);
      constexpr size_t allocationSize1 = 64;
      constexpr size_t allocationSize2 = 256;
      constexpr int allocationLoop = 1000;
      bool isOK = true;
      void* allocation = allocator_alloc(&allocator, allocationSize1);
      for (int i = 0; i < allocationLoop; ++i) {
        allocation = allocator_realloc(&allocator, allocation, allocationSize2);
        if (!allocation) {
          isOK = false;
          break;
        }
        allocation = allocator_realloc(&allocator, allocation, allocationSize1);
        if (!allocation) {
          isOK = false;
          break;
        }
      }
      REQUIRE(isOK);
      allocator_destroy(&allocator);
    }

    {
      // Have to find another region
      free_list_allocator_t allocator;
      fla_init(&allocator, "free_list_allocator_t", c_allocator_size);
      void* firstAllocation = allocator_alloc(
          &allocator, c_allocator_size / 2 - sizeof(allocation_header_t));
      void* secondAllocation = allocator_alloc(
          &allocator, c_allocator_size / 2 - sizeof(allocation_header_t));
      allocator_free(&allocator, firstAllocation);
      REQUIRE(
          allocator_realloc(&allocator,
                            secondAllocation,
                            c_allocator_size - sizeof(allocation_header_t)));
      allocator_destroy(&allocator);
    }

    {
      // Data integrity when reallocating to a new allocaiton that right before
      // the old allocation
      free_list_allocator_t allocator;
      fla_init(&allocator, "free_list_allocator_t", 256 + 65);
      void* first_alloc = allocator_alloc(&allocator, 1);
      void* second_alloc = allocator_alloc(&allocator, 256);
      for (int i = 0; i < 256; ++i)
        *((uint8_t*)second_alloc + i) = i;
      allocator_free(&allocator, first_alloc);
      second_alloc = allocator_realloc(&allocator, second_alloc, 256 + 1);
      bool ok = true;
      for (int i = 0; i < 256; ++i)
        if (*((uint8_t*)second_alloc + i) != i) {
          ok = false;
          break;
        }
      REQUIRE(ok);
      allocator_destroy(&allocator);
    }
    {
      // Try after failed realloc.
      free_list_allocator_t allocator;
      fla_init(&allocator, "free_list_allocator_t", 256);
      uint8_t* p1 = (uint8_t*)allocator_alloc(&allocator, 1);
      *p1 = 123;
      void* p2 = allocator_alloc(&allocator, 128);
      REQUIRE(p1);
      REQUIRE(p2);
      REQUIRE_FALSE(allocator_realloc(&allocator, p1, 128));
      REQUIRE(*p1 == 123);
      REQUIRE(allocator_realloc(&allocator, p1, 32));
      allocator_destroy(&allocator);
    }
  }
}
