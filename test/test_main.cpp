//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#define CATCH_CONFIG_RUNNER
#include "3rdparty/Catch2/catch.hpp"
#include "core/core_init.hpp"

int main(int argc, char** argv) {
  core_init(OS_LIT("test.log"));
  int result = Catch::Session().run(argc, argv);
  return result;
}
