//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "math/mat4.inl"

#include "3rdparty/Catch2/catch.hpp"

TEST_CASE("math/mat4") {
  {
    mat4 default_mat4;
    bool ok = true;
    for (int i = 0; i < 4; ++i)
      for (int j = 0; j < 4; ++j)
        if (default_mat4.v[i].a[j])
          ok = false;
    REQUIRE(ok);
  }
  {
    mat4 m{0.f, 1.f, 2.f, 3.f,
           4.f, 5.f, 6.f, 7.f,
           8.f, 9.f, 10.f, 11.f,
           12.f, 13.f, 14.f, 15.f};
    mat4 m2 = m;
    bool ok = true;
    for (int i = 0; i < 4; ++i)
      for (int j = 0; j < 4; ++j)
        if (m2.v[i].a[j] != (float)(i * 4 + j))
          ok = false;
    REQUIRE(ok);
  }
  {
    mat4 m{0.f, 1.f, 2.f, 3.f,
           4.f, 5.f, 6.f, 7.f,
           8.f, 9.f, 10.f, 11.f,
           12.f, 13.f, 14.f, 15.f};
    REQUIRE(m * m == mat4{56.f, 62.f, 68.f, 74.f,
                          152.f, 174.f, 196.f, 218.f,
                          248.f, 286.f, 324.f, 362.f,
                          344.f, 398.f, 452.f, 506.f});
  }

  {
    mat4 m{0.f, 1.f, 2.f, 3.f,
           4.f, 5.f, 6.f, 7.f,
           8.f, 9.f, 10.f, 11.f,
           12.f, 13.f, 14.f, 15.f};
    vec4 v{1.f, 2.f, 3.f, 4.f};
    REQUIRE(v * m == vec4{80.f, 90.f, 100.f, 110.f});
  }
}
