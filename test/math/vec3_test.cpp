//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "math/vec3.inl"

#include "3rdparty/Catch2/catch.hpp"

TEST_CASE("math/vec3") {
  {
    vec3 v;
    bool ok = true;
    for (int i = 0; i < 3; ++i)
      if (v.a[i])
        ok = false;
    REQUIRE(ok);
  }
  {
    vec3 x{1.f, 0.f, 0.f};
    vec3 y{0.f, 1.f, 0.f};
    vec3 z{0.f, 0.f, 1.f};
    REQUIRE(vec3_cross(x, y) == z);
    REQUIRE(vec3_cross(y, z) == x);
    REQUIRE(vec3_cross(z, x) == y);
  }
}
