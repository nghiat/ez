//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#pragma once

#include "math/mat4.inl"
#include "math/vec3.inl"
#include <math.h>

EZ_INLINE mat4 mat4_look_at_rh(const vec3& eye, const vec3& target, const vec3& up) {
  vec3 z_axis = vec3_normalize((eye - target));
  vec3 x_axis = vec3_normalize(vec3_cross(up, z_axis));
  vec3 y_axis = vec3_cross(z_axis, x_axis);

  return mat4{vec4{x_axis.x, y_axis.x, z_axis.x, 0},
              vec4{x_axis.y, y_axis.y, z_axis.y, 0},
              vec4{x_axis.z, y_axis.z, z_axis.z, 0},
              vec4{-vec3_dot(x_axis, eye),
                   -vec3_dot(y_axis, eye),
                   -vec3_dot(z_axis, eye),
                   1.f}};
}

EZ_INLINE mat4 mat4_perspective(float fovy, float aspect, float z_near, float z_far) {
  mat4 result;
  result.a[1][1] = 1 / tanf(fovy / 2);
  result.a[0][0] = result.v[1].a[1] / aspect;
  result.a[2][2] = -(z_far + z_near) / (z_far - z_near);
  result.a[2][3] = -2.f * z_near * z_far / (z_far - z_near);
  result.a[3][2] = -1.f;
  return result;
}

EZ_INLINE mat4 mat4_rotate(const vec3& axis, float angle) {
  vec3 n = vec3_normalize(axis);
  float x = n.x;
  float y = n.y;
  float z = n.z;
  float c = cosf(angle);
  float s = sinf(angle);
  return mat4{vec4{c + x * x * (1 - c),
                   x * y * (1 - c) - z * s,
                   x * z * (1 - c) + y * s,
                   0.f},
              vec4{y * x * (1 - c) + z * s,
                   c + y * y * (1 - c),
                   y * x * (1 - c) - x * s,
                   0.f},
              vec4{z * x * (1 - c) - y * s,
                   z * y * (1 - c) + x * s,
                   c + x * x * (1 - c),
                   0.f},
              vec4{}};
}

EZ_INLINE mat4 mat4_scale(float sx, float sy, float sz) {
  mat4 m;
  m.a[0][0] = sx;
  m.a[1][1] = sy;
  m.a[2][2] = sz;
  m.a[3][3] = 1.f;
  return m;
}

EZ_INLINE mat4 mat4_translate(const vec3& v) {
  return mat4{vec4{1.f, 0.f, 0.f, 0.f},
              vec4{0.f, 1.f, 0.f, 0.f},
              vec4{0.f, 0.f, 1.f, 0.f},
              vec4{v.x, v.y, v.z, 1.f}};
}
