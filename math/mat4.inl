//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#pragma once

#include "math/mat4.hpp"

#include "core/compiler.hpp"
#include "math/vec4.inl"
#include <string.h>

EZ_INLINE vec4 operator*(const vec4& v, const mat4& m) {
  vec4 result;
  mat4 temp;
  for (int j = 0; j < 4; ++j) {
    temp.a[j][0] = v.a[j] * m.a[j][0];
    temp.a[j][1] = v.a[j] * m.a[j][1];
    temp.a[j][2] = v.a[j] * m.a[j][2];
    temp.a[j][3] = v.a[j] * m.a[j][3];
  }
  result.x = temp.a[0][0] + temp.a[1][0] + temp.a[2][0] + temp.a[3][0];
  result.y = temp.a[0][1] + temp.a[1][1] + temp.a[2][1] + temp.a[3][1];
  result.z = temp.a[0][2] + temp.a[1][2] + temp.a[2][2] + temp.a[3][2];
  result.w = temp.a[0][3] + temp.a[1][3] + temp.a[2][3] + temp.a[3][3];
  return result;
}

EZ_INLINE bool operator==(const mat4&lhs, const mat4& rhs) {
  return !memcmp(lhs.a, rhs.a, 16 * sizeof(float));
}

EZ_INLINE mat4 operator*(const mat4& lhs, const mat4& rhs) {
  mat4 result;
  for (int i = 0; i < 4; ++i) {
    result.v[i] = lhs.v[i] * rhs;
  }
  return result;
}
