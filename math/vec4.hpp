//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#pragma once

struct vec4 {
  union {
    struct {
      float x;
      float y;
      float z;
      float w;
    };
    float a[4] = {};
  };
};
