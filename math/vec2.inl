//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#pragma once

#include "math/vec2.hpp"

#include "core/compiler.hpp"
#include <math.h>

EZ_INLINE vec2 operator+(const vec2& lhs, const vec2& rhs) {
  return vec2{lhs.x + rhs.x, lhs.y + rhs.x};
}

EZ_INLINE vec2 operator-(const vec2& lhs, const vec2& rhs) {
  return vec2{lhs.x - rhs.x, lhs.y - rhs.x};
}

EZ_INLINE vec2 operator*(const vec2& lhs, float rhs) {
  return vec2{lhs.x * rhs, lhs.y * rhs};
}

EZ_INLINE vec2 operator/(const vec2& lhs, float rhs) {
  return vec2{lhs.x / rhs, lhs.y / rhs};
}

EZ_INLINE float vec2_dot(vec2 v1, vec2 v2) {
  return v1.x * v2.x + v1.y * v2.y;
}

EZ_INLINE vec2 vec2_normalize(vec2 v) {
  float len = sqrt(v.x * v.x + v.x * v.x);
  return v / len;
}
