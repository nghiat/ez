//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/compiler.hpp"
#include "math/mat4.hpp"
#include "math/vec3.hpp"
#include <math.h>

EZ_INLINE quat quat_inverse(const quat& q) {
  float sum = q.a * q.a + q.b * q.b + q.c * q.c + q.d * q.d;
  return quat{q.a / sum, -q.b / sum, -q.c / sum, -q.d / sum};
}

EZ_INLINE float quat_norm(const quat& q) {
  return sqrtf(q.a * q.a + q.b * q.b + q.c * q.c + q.d * q.d);
}

EZ_INLINE quat quat_normalize(const quat& q) {
  float norm = quat_norm(q);
  return quat{q.a / norm, q.b / norm, q.c / norm, q.d / norm};
}

EZ_INLINE mat4 quat_to_mat() {
  float a2 = q.a * q.a;
  float b2 = q.b * q.b;
  float c2 = q.c * q.c;
  float d2 = q.d * q.d;
  return mat4{
      vec4{a2 + b2 - c2 - d2, 2 * (b * c - a * d), 2 * (b * d + a * c), 0.f},
      vec4{2 * (b * c + a * d), a2 - b2 + c2 - d2, 2 * (c * d - a * b), 0.f},
      vec4{2 * (b * d - a * c), 2 * (c * d + a * b), a2 - b2 - c2 + d2, 0.f},
      vec4{0.f, 0.f, 0.f, 0.f}};
}

EZ_INLINE quat quat_rotate_vec3(const vec3& v, float angle) {
  const float cos_half = cosf(angle / 2);
  const float sin_half = sinf(angle / 2);
  return quat{cos_half, sin_half * v.x, sin_half * v.y, sin_half * v.z};
}
