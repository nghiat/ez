//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#pragma once

#include "math/vec4.hpp"

#include "core/compiler.hpp"
#include "math/vec3.hpp"
#include <math.h>
#include <string.h>

EZ_INLINE vec4 operator+(const vec4& lhs, const vec4& rhs) {
  return vec4{lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.z};
}

EZ_INLINE vec4 operator-(const vec4& lhs, const vec4& rhs) {
  return vec4{lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z, lhs.w - rhs.w};
}

EZ_INLINE vec4 operator*(const vec4& lhs, float rhs) {
  return vec4{lhs.x * rhs, lhs.y * rhs, lhs.z * rhs, lhs.w * rhs};
}

EZ_INLINE vec4 operator/(const vec4& lhs, float rhs) {
  return vec4{lhs.x / rhs, lhs.y / rhs, lhs.z / rhs, lhs.w / rhs};
}

EZ_INLINE bool operator==(const vec4& lhs, const vec4& rhs) {
  return !memcmp(lhs.a, rhs.a, 4 * sizeof(float));
}

EZ_INLINE float vec4_dot(const vec4& lhs, const vec4& rhs) {
  return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z + lhs.w * rhs.w;
}

EZ_INLINE float vec4_len(const vec4& v) {
  return sqrt(v.x * v.x + v.y * v.y + v.z * v.z + v.w * v.w);
}

EZ_INLINE vec4 vec4_normalize(const vec4& v) { return v / vec4_len(v); }

EZ_INLINE vec3 vec4_to_vec3(const vec4& v) {
  return vec3{v.x / v.w, v.y / v.w, v.z / v.w};
}
