//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#pragma once

#include "math/vec3.hpp"

#include "core/compiler.hpp"
#include <math.h>

EZ_INLINE bool operator==(const vec3& lhs, const vec3& rhs) {
  return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z;
}

EZ_INLINE bool operator!=(const vec3& lhs, const vec3& rhs) {
  return !(lhs == rhs);
}

EZ_INLINE vec3 operator+(const vec3& lhs, const vec3& rhs) {
  return vec3{lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z};
}

EZ_INLINE vec3& operator+=(vec3& lhs, const vec3& rhs) {
  lhs.x += rhs.x;
  lhs.y += rhs.y;
  lhs.z += rhs.z;
  return lhs;
}

EZ_INLINE vec3 operator-(const vec3& v) {
  return vec3{-v.x, -v.y, -v.z};
}

EZ_INLINE vec3 operator-(const vec3& lhs, const vec3& rhs) {
  return vec3{lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z};
}

EZ_INLINE vec3& operator-=(vec3& lhs, const vec3& rhs) {
  lhs.x -= rhs.x;
  lhs.y -= rhs.y;
  lhs.z -= rhs.z;
  return lhs;
}

EZ_INLINE vec3 operator*(const vec3 lhs, float rhs) {
  return vec3{lhs.x * rhs, lhs.y * rhs, lhs.z * rhs};
}

EZ_INLINE vec3 operator/(const vec3 lhs, float rhs) {
  return vec3{lhs.x / rhs, lhs.y / rhs, lhs.z / rhs};
}


EZ_INLINE vec3 vec3_cross(const vec3& v1, const vec3& v2) {
  return vec3{v1.y * v2.z - v1.z * v2.y,
              v1.z * v2.x - v1.x * v2.z,
              v1.x * v2.y - v1.y * v2.x};
}

EZ_INLINE float vec3_dot(const vec3& v1, const vec3& v2) {
  return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

EZ_INLINE float vec3_length(const vec3& v) {
  return sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

EZ_INLINE vec3 vec3_normalize(const vec3& v) {
  return v / vec3_length(v);
}
