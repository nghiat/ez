//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/compiler.hpp"
#include "math/mat4.hpp"
#include "math/vec3.hpp"
#include <math.h>

struct quat {
  float a = 0.f;
  float b = 0.f;
  float c = 0.f;
  float d = 0.f;
};
