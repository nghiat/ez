//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2018             //
//----------------------------------------------------------------------------//

#pragma once

#include "math/vec4.hpp"

// Row-major
struct mat4 {
  union {
    vec4 v[4] = {};
    float a[4][4];
  };
};
