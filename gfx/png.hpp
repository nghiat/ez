//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/dynamic_array.hpp"
#include "core/string.hpp"

namespace gfx {

class PNG {
public:
  bool init(const os_char_t* path);
  void destroy();

  uint32_t get_width() const { return width_; }

  uint32_t get_height() const { return height_; }

  const uint8_t* pointer() const { return data_.p; }

private:
  dynamic_array_t<uint8_t> data_;
  uint32_t width_;
  uint32_t height_;
  uint32_t bit_depth_;
  uint32_t bit_per_pixel_;
};

} // namespace gfx
