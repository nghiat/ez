//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include <GL/glx.h>
#include "3rdparty/GL/GL/glcorearb.h"
#include "3rdparty/GL/GL/glxext.h"

extern PFNGLXCREATECONTEXTATTRIBSARBPROC glx_create_context_attribs_arb;
