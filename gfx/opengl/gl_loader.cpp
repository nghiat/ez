//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "gfx/opengl/gl_loader.hpp"

#include "gfx/opengl/gl.hpp"

PFNGLATTACHSHADERPROC gl_attach_shader;
PFNGLBINDBUFFERPROC gl_bind_buffer;
PFNGLBINDTEXTUREPROC gl_bind_texture;
PFNGLBINDVERTEXARRAYPROC gl_bind_vertex_array;
PFNGLBLENDFUNCPROC gl_blend_func;
PFNGLBUFFERDATAPROC gl_buffer_data;
PFNGLBUFFERSUBDATAPROC gl_buffer_sub_data;
PFNGLCLEARPROC gl_clear;
PFNGLCLEARCOLORPROC gl_clear_color;
PFNGLCOMPILESHADERPROC gl_compile_shader;
PFNGLCREATEPROGRAMPROC gl_create_program;
PFNGLCREATESHADERPROC gl_create_shader;
PFNGLDISABLEVERTEXATTRIBARRAYPROC gl_disable_vertex_attrib_array;
PFNGLDRAWARRAYSPROC gl_draw_arrays;
PFNGLENABLEPROC gl_enable;
PFNGLENABLEVERTEXATTRIBARRAYPROC gl_enable_vertex_attrib_array;
PFNGLGENBUFFERSPROC gl_gen_buffers;
PFNGLGENTEXTURESPROC gl_gen_textures;
PFNGLGENVERTEXARRAYSPROC gl_gen_vertex_arrays;
PFNGLGETUNIFORMLOCATIONPROC gl_get_uniform_location;
PFNGLLINKPROGRAMPROC gl_link_program;
PFNGLSHADERSOURCEPROC gl_shader_source;
PFNGLTEXIMAGE2DPROC gl_tex_image_2d;
PFNGLTEXPARAMETERIPROC gl_tex_parameteri;
PFNGLUNIFORM1FPROC gl_uniform_1f;
PFNGLUNIFORM2FPROC gl_uniform_2f;
PFNGLUNIFORM3FPROC gl_uniform_3f;
PFNGLUNIFORM4FPROC gl_uniform_4f;
PFNGLUNIFORM1IPROC gl_uniform_1i;
PFNGLUNIFORM2IPROC gl_uniform_2i;
PFNGLUNIFORM3IPROC gl_uniform_3i;
PFNGLUNIFORM4IPROC gl_uniform_4i;
PFNGLUNIFORM1UIPROC gl_uniform_1ui;
PFNGLUNIFORM2UIPROC gl_uniform_2ui;
PFNGLUNIFORM3UIPROC gl_uniform_3ui;
PFNGLUNIFORM4UIPROC gl_uniform_4ui;
PFNGLUNIFORM1FVPROC gl_uniform_1fv;
PFNGLUNIFORM2FVPROC gl_uniform_2fv;
PFNGLUNIFORM3FVPROC gl_uniform_3fv;
PFNGLUNIFORM4FVPROC gl_uniform_4fv;
PFNGLUNIFORM1IVPROC gl_uniform_1iv;
PFNGLUNIFORM2IVPROC gl_uniform_2iv;
PFNGLUNIFORM3IVPROC gl_uniform_3iv;
PFNGLUNIFORM4IVPROC gl_uniform_4iv;
PFNGLUNIFORM1UIVPROC gl_uniform_1uiv;
PFNGLUNIFORM2UIVPROC gl_uniform_2uiv;
PFNGLUNIFORM3UIVPROC gl_uniform_3uiv;
PFNGLUNIFORM4UIVPROC gl_uniform_4uiv;
PFNGLUNIFORMMATRIX2FVPROC gl_uniform_matrix_2fv;
PFNGLUNIFORMMATRIX3FVPROC gl_uniform_matrix_3fv;
PFNGLUNIFORMMATRIX4FVPROC gl_uniform_matrix_4fv;
PFNGLUNIFORMMATRIX2X3FVPROC gl_uniform_matrix_2x3fv;
PFNGLUNIFORMMATRIX3X2FVPROC gl_uniform_matrix_3x2fv;
PFNGLUNIFORMMATRIX2X4FVPROC gl_uniform_matrix_2x4fv;
PFNGLUNIFORMMATRIX4X2FVPROC gl_uniform_matrix_4x2fv;
PFNGLUNIFORMMATRIX3X4FVPROC gl_uniform_matrix_3x4fv;
PFNGLUNIFORMMATRIX4X3FVPROC gl_uniform_matrix_4x3fv;
PFNGLUSEPROGRAMPROC gl_use_program;
PFNGLVERTEXATTRIBPOINTERPROC gl_vertex_attrib_pointer;

namespace gfx {

void load_core_gl_functions() {
  gl_attach_shader = (PFNGLATTACHSHADERPROC)get_gl_proc("glAttachShader");
  gl_bind_buffer = (PFNGLBINDBUFFERPROC)get_gl_proc("glBindBuffer");
  gl_bind_texture = (PFNGLBINDTEXTUREPROC)get_gl_proc("glBindTexture");
  gl_bind_vertex_array =
      (PFNGLBINDVERTEXARRAYPROC)get_gl_proc("glBindVertexArray");
  gl_blend_func = (PFNGLBLENDFUNCPROC)get_gl_proc("glBlendFunc");
  gl_buffer_data = (PFNGLBUFFERDATAPROC)get_gl_proc("glBufferData");
  gl_buffer_sub_data = (PFNGLBUFFERSUBDATAPROC)get_gl_proc("glBufferSubData");
  gl_clear = (PFNGLCLEARPROC)get_gl_proc("glClear");
  gl_clear_color = (PFNGLCLEARCOLORPROC)get_gl_proc("glClearColor");
  gl_compile_shader = (PFNGLCOMPILESHADERPROC)get_gl_proc("glCompileShader");
  gl_create_program = (PFNGLCREATEPROGRAMPROC)get_gl_proc("glCreateProgram");
  gl_create_shader = (PFNGLCREATESHADERPROC)get_gl_proc("glCreateShader");
  gl_disable_vertex_attrib_array = (PFNGLDISABLEVERTEXATTRIBARRAYPROC)get_gl_proc(
      "glDisableVertexAttribArray");
  gl_draw_arrays = (PFNGLDRAWARRAYSPROC)get_gl_proc("glDrawArrays");
  gl_enable = (PFNGLENABLEPROC)get_gl_proc("glEnable");
  gl_enable_vertex_attrib_array = (PFNGLENABLEVERTEXATTRIBARRAYPROC)get_gl_proc(
      "glEnableVertexAttribArray");
  gl_gen_buffers = (PFNGLGENBUFFERSPROC)get_gl_proc("glGenBuffers");
  gl_gen_textures = (PFNGLGENTEXTURESPROC)get_gl_proc("glGenTextures");
  gl_gen_vertex_arrays =
      (PFNGLGENVERTEXARRAYSPROC)get_gl_proc("glGenVertexArrays");
  gl_get_uniform_location =
      (PFNGLGETUNIFORMLOCATIONPROC)get_gl_proc("glGetUniformLocation");
  gl_link_program = (PFNGLLINKPROGRAMPROC)get_gl_proc("glLinkProgram");
  gl_shader_source = (PFNGLSHADERSOURCEPROC)get_gl_proc("glShaderSource");
  gl_tex_image_2d = (PFNGLTEXIMAGE2DPROC)get_gl_proc("glTexImage2D");
  gl_tex_parameteri = (PFNGLTEXPARAMETERIPROC)get_gl_proc("glTexParameteri");
  gl_uniform_1f = (PFNGLUNIFORM1FPROC)get_gl_proc("glUniform1f");
  gl_uniform_2f = (PFNGLUNIFORM2FPROC)get_gl_proc("glUniform2f");
  gl_uniform_3f = (PFNGLUNIFORM3FPROC)get_gl_proc("glUniform3f");
  gl_uniform_4f = (PFNGLUNIFORM4FPROC)get_gl_proc("glUniform4f");
  gl_uniform_1i = (PFNGLUNIFORM1IPROC)get_gl_proc("glUniform1i");
  gl_uniform_2i = (PFNGLUNIFORM2IPROC)get_gl_proc("glUniform2i");
  gl_uniform_3i = (PFNGLUNIFORM3IPROC)get_gl_proc("glUniform3i");
  gl_uniform_4i = (PFNGLUNIFORM4IPROC)get_gl_proc("glUniform4i");
  gl_uniform_1ui = (PFNGLUNIFORM1UIPROC)get_gl_proc("glUniform1ui");
  gl_uniform_2ui = (PFNGLUNIFORM2UIPROC)get_gl_proc("glUniform2ui");
  gl_uniform_3ui = (PFNGLUNIFORM3UIPROC)get_gl_proc("glUniform3ui");
  gl_uniform_4ui = (PFNGLUNIFORM4UIPROC)get_gl_proc("glUniform4ui");
  gl_uniform_1fv = (PFNGLUNIFORM1FVPROC)get_gl_proc("glUniform1fv");
  gl_uniform_2fv = (PFNGLUNIFORM2FVPROC)get_gl_proc("glUniform2fv");
  gl_uniform_3fv = (PFNGLUNIFORM3FVPROC)get_gl_proc("glUniform3fv");
  gl_uniform_4fv = (PFNGLUNIFORM4FVPROC)get_gl_proc("glUniform4fv");
  gl_uniform_1iv = (PFNGLUNIFORM1IVPROC)get_gl_proc("glUniform1iv");
  gl_uniform_2iv = (PFNGLUNIFORM2IVPROC)get_gl_proc("glUniform2iv");
  gl_uniform_3iv = (PFNGLUNIFORM3IVPROC)get_gl_proc("glUniform3iv");
  gl_uniform_4iv = (PFNGLUNIFORM4IVPROC)get_gl_proc("glUniform4iv");
  gl_uniform_1uiv = (PFNGLUNIFORM1UIVPROC)get_gl_proc("glUniform1uiv");
  gl_uniform_2uiv = (PFNGLUNIFORM2UIVPROC)get_gl_proc("glUniform2uiv");
  gl_uniform_3uiv = (PFNGLUNIFORM3UIVPROC)get_gl_proc("glUniform3uiv");
  gl_uniform_4uiv = (PFNGLUNIFORM4UIVPROC)get_gl_proc("glUniform4uiv");
  gl_uniform_matrix_2fv =
      (PFNGLUNIFORMMATRIX2FVPROC)get_gl_proc("glUniformMatrix2fv");
  gl_uniform_matrix_3fv =
      (PFNGLUNIFORMMATRIX3FVPROC)get_gl_proc("glUniformMatrix3fv");
  gl_uniform_matrix_4fv =
      (PFNGLUNIFORMMATRIX4FVPROC)get_gl_proc("glUniformMatrix4fv");
  gl_uniform_matrix_2x3fv =
      (PFNGLUNIFORMMATRIX2X3FVPROC)get_gl_proc("glUniformMatrix2x3fv");
  gl_uniform_matrix_3x2fv =
      (PFNGLUNIFORMMATRIX3X2FVPROC)get_gl_proc("glUniformMatrix3x2fv");
  gl_uniform_matrix_2x4fv =
      (PFNGLUNIFORMMATRIX2X4FVPROC)get_gl_proc("glUniformMatrix2x4fv");
  gl_uniform_matrix_4x2fv =
      (PFNGLUNIFORMMATRIX4X2FVPROC)get_gl_proc("glUniformMatrix4x2fv");
  gl_uniform_matrix_3x4fv =
      (PFNGLUNIFORMMATRIX3X4FVPROC)get_gl_proc("glUniformMatrix3x4fv");
  gl_uniform_matrix_4x3fv =
      (PFNGLUNIFORMMATRIX4X3FVPROC)get_gl_proc("glUniformMatrix4x3fv");
  gl_use_program = (PFNGLUSEPROGRAMPROC)get_gl_proc("glUseProgram");
  gl_vertex_attrib_pointer =
      (PFNGLVERTEXATTRIBPOINTERPROC)get_gl_proc("glVertexAttribPointer");
}

} // namespace gfx
