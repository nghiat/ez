//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#ifndef __gl_h_
#  define __gl_h_
#  include "3rdparty/GL/GL/glcorearb.h"

extern PFNGLATTACHSHADERPROC gl_attach_shader;
extern PFNGLBINDBUFFERPROC gl_bind_buffer;
extern PFNGLBINDTEXTUREPROC gl_bind_texture;
extern PFNGLBINDVERTEXARRAYPROC gl_bind_vertex_array;
extern PFNGLBLENDFUNCPROC gl_blend_func;
extern PFNGLBUFFERDATAPROC gl_buffer_data;
extern PFNGLBUFFERSUBDATAPROC gl_buffer_sub_data;
extern PFNGLCLEARPROC gl_clear;
extern PFNGLCLEARCOLORPROC gl_clear_color;
extern PFNGLCOMPILESHADERPROC gl_compile_shader;
extern PFNGLCREATEPROGRAMPROC gl_create_program;
extern PFNGLCREATESHADERPROC gl_create_shader;
extern PFNGLDISABLEVERTEXATTRIBARRAYPROC gl_disable_vertex_attrib_array;
extern PFNGLDRAWARRAYSPROC gl_draw_arrays;
extern PFNGLENABLEPROC gl_enable;
extern PFNGLENABLEVERTEXATTRIBARRAYPROC gl_enable_vertex_attrib_array;
extern PFNGLGENBUFFERSPROC gl_gen_buffers;
extern PFNGLGENTEXTURESPROC gl_gen_textures;
extern PFNGLGENVERTEXARRAYSPROC gl_gen_vertex_arrays;
extern PFNGLGETUNIFORMLOCATIONPROC gl_get_uniform_location;
extern PFNGLLINKPROGRAMPROC gl_link_program;
extern PFNGLSHADERSOURCEPROC gl_shader_source;
extern PFNGLTEXIMAGE2DPROC gl_tex_image_2d;
extern PFNGLTEXPARAMETERIPROC gl_tex_parameteri;
extern PFNGLUNIFORM1FPROC gl_uniform_1f;
extern PFNGLUNIFORM2FPROC gl_uniform_2f;
extern PFNGLUNIFORM3FPROC gl_uniform_3f;
extern PFNGLUNIFORM4FPROC gl_uniform_4f;
extern PFNGLUNIFORM1IPROC gl_uniform_1i;
extern PFNGLUNIFORM2IPROC gl_uniform_2i;
extern PFNGLUNIFORM3IPROC gl_uniform_3i;
extern PFNGLUNIFORM4IPROC gl_uniform_4i;
extern PFNGLUNIFORM1UIPROC gl_uniform_1ui;
extern PFNGLUNIFORM2UIPROC gl_uniform_2ui;
extern PFNGLUNIFORM3UIPROC gl_uniform_3ui;
extern PFNGLUNIFORM4UIPROC gl_uniform_4ui;
extern PFNGLUNIFORM1FVPROC gl_uniform_1fv;
extern PFNGLUNIFORM2FVPROC gl_uniform_2fv;
extern PFNGLUNIFORM3FVPROC gl_uniform_3fv;
extern PFNGLUNIFORM4FVPROC gl_uniform_4fv;
extern PFNGLUNIFORM1IVPROC gl_uniform_1iv;
extern PFNGLUNIFORM2IVPROC gl_uniform_2iv;
extern PFNGLUNIFORM3IVPROC gl_uniform_3iv;
extern PFNGLUNIFORM4IVPROC gl_uniform_4iv;
extern PFNGLUNIFORM1UIVPROC gl_uniform_1uiv;
extern PFNGLUNIFORM2UIVPROC gl_uniform_2uiv;
extern PFNGLUNIFORM3UIVPROC gl_uniform_3uiv;
extern PFNGLUNIFORM4UIVPROC gl_uniform_4uiv;
extern PFNGLUNIFORMMATRIX2FVPROC gl_uniform_matrix_2fv;
extern PFNGLUNIFORMMATRIX3FVPROC gl_uniform_matrix_3fv;
extern PFNGLUNIFORMMATRIX4FVPROC gl_uniform_matrix_4fv;
extern PFNGLUNIFORMMATRIX2X3FVPROC gl_uniform_matrix_2x3fv;
extern PFNGLUNIFORMMATRIX3X2FVPROC gl_uniform_matrix_3x2fv;
extern PFNGLUNIFORMMATRIX2X4FVPROC gl_uniform_matrix_2x4fv;
extern PFNGLUNIFORMMATRIX4X2FVPROC gl_uniform_matrix_4x2fv;
extern PFNGLUNIFORMMATRIX3X4FVPROC gl_uniform_matrix_3x4fv;
extern PFNGLUNIFORMMATRIX4X3FVPROC gl_uniform_matrix_4x3fv;
extern PFNGLUSEPROGRAMPROC gl_use_program;
extern PFNGLVERTEXATTRIBPOINTERPROC gl_vertex_attrib_pointer;

#endif // __gl_h
