//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "gfx/opengl/gl_loader.hpp"

#include "core/dynamic_lib.hpp"
#include "gfx/opengl/wgl.hpp"

PFNWGLCREATECONTEXTATTRIBSARBPROC wgl_create_context_attribs_arb;

namespace gfx {
namespace {
dynamic_lib_t g_gl_lib;
}

void* get_gl_proc(const char* name) {
  void* func_p = (void*)wglGetProcAddress(name);
  if (func_p == 0 || (func_p == (void*)0x1) || (func_p == (void*)0x2) ||
      (func_p == (void*)0x3) || (func_p == (void*)-1)) {
    func_p = dl_get_proc(&g_gl_lib, name);
  }
  return func_p;
}

void load_platform_gl_extensions() {
  dl_open(&g_gl_lib, "opengl32.dll");

  wgl_create_context_attribs_arb =
      (PFNWGLCREATECONTEXTATTRIBSARBPROC)get_gl_proc(
          "wglCreateContextAttribsARB");
}

} // namespace gfx
