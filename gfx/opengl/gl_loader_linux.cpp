//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "gfx/opengl/gl_loader.hpp"

#include "core/dynamic_lib.hpp"
#include "gfx/opengl/glx.hpp"

PFNGLXCREATECONTEXTATTRIBSARBPROC glx_create_context_attribs_arb;

namespace gfx {
namespace {

typedef void* (*glx_get_proc_t)(const char*);
glx_get_proc_t g_glx_get_proc = nullptr;

} // namespace

void* get_gl_proc(const char* name) { return g_glx_get_proc(name); }

void load_platform_gl_extensions() {
  dynamic_lib_t gl_lib;
  dl_open(&gl_lib, "libGL.so.1");
  g_glx_get_proc =
      (glx_get_proc_t)dl_get_proc(&gl_lib, "glXGetProcAddressARB");

  glx_create_context_attribs_arb =
      (PFNGLXCREATECONTEXTATTRIBSARBPROC)dl_get_proc(
          &gl_lib, "glXCreateContextAttribsARB");
}

} // namespace gfx
