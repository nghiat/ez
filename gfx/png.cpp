//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "gfx/png.hpp"

#include "core/algorithm/search.inl"
#include "core/allocator.hpp"
#include "core/bit_stream.hpp"
#include "core/compile_time_util.inl"
#include "core/core_allocators.hpp"
#include "core/dynamic_array.inl"
#include "core/file.hpp"
#include "core/log.hpp"
#include "core/platform.hpp"
#include <stdlib.h>

#if defined(OS_WIN)
#  define bswap32(x) _byteswap_ulong(x)
#elif defined(OS_LINUX)
#  include <byteswap.h>
#  define bswap32(x) bswap_32(x)
#endif

namespace gfx {
namespace {

// From 0 - 15
constexpr int gc_max_code_len = 16;
constexpr int gc_max_code = 286 + 30;

constexpr int c_png_sig_len = 8;
const uint8_t c_png_signature[c_png_sig_len] =
    {137, 80, 78, 71, 13, 10, 26, 10};

constexpr int c_len_bases[] = {3,  4,  5,  6,   7,   8,   9,   10,  11, 13,
                               15, 17, 19, 23,  27,  31,  35,  43,  51, 59,
                               67, 83, 99, 115, 131, 163, 195, 227, 258};

constexpr int c_len_extra_bits[] = {0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2,
                                    2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0};

constexpr int c_dist_bases[] = {
    1,    2,    3,    4,    5,    7,    9,    13,    17,    25,
    33,   49,   65,   97,   129,  193,  257,  385,   513,   769,
    1025, 1537, 2049, 3073, 4097, 6145, 8193, 12289, 16385, 24577};

constexpr int c_dist_extra_bits[] = {0, 0, 0,  0,  1,  1,  2,  2,  3,  3,
                                     4, 4, 5,  5,  6,  6,  7,  7,  8,  8,
                                     9, 9, 10, 10, 11, 11, 12, 12, 13, 13};

// Structure saves codes of specific length.
// |codes| contains codes of that length.
// |min| is the smallest codes of that length.
// |count| is the number of codes.
struct Codes_For_Len {
  uint16_t codes[gc_max_code];
  uint16_t min;
  uint16_t count;
};

// |cfl| is a pointer to array of where the index is the length and value is the
// codes for that length.
struct Alphabet {
  Codes_For_Len* cfl;
  uint8_t min_len;
  uint8_t max_len;
};

// Build an alphabet based on lengths of  codes from 0 to |num| - 1.
void build_alphabet(uint8_t* lens, int num, Alphabet* alphabet) {
  uint8_t len_counts[gc_max_code_len] = {};
  for (int i = 0; i < num; ++i) {
    if (lens[i]) {
      alphabet->cfl[lens[i]].codes[len_counts[lens[i]]] = i;
      len_counts[lens[i]]++;
    }
  }
  for (int i = 0; i < gc_max_code_len; ++i) {
    if (len_counts[i]) {
      alphabet->min_len = i;
      break;
    }
  }
  for (uint8_t i = gc_max_code_len - 1; i > 0; ++i) {
    if (len_counts[i]) {
      alphabet->max_len = i;
      break;
    }
  }
  int smallest_code = 0;
  int last_non_zero_count = 0;
  for (uint8_t i = 0; i < 16; ++i) {
    if (len_counts[i]) {
      smallest_code = (smallest_code + last_non_zero_count) << 1;
      alphabet->cfl[i].min = smallest_code;
      alphabet->cfl[i].count = len_counts[i];
      last_non_zero_count = len_counts[i];
    }
  }
}

int decode(bit_stream_t* bs, const Alphabet* c_alphabet) {
  int code = bs_consume_msb(bs, c_alphabet->min_len);
  for (uint8_t i = c_alphabet->min_len; i <= c_alphabet->max_len; ++i) {
    int delta_to_min = code - c_alphabet->cfl[i].min;
    if (delta_to_min < c_alphabet->cfl[i].count) {
      return c_alphabet->cfl[i].codes[delta_to_min];
    }
    code = code << 1 | bs_consume_msb(bs, 1);
  }
  LOGW("Can't decode");
  return -1;
}

void decode_len_and_dist(int len_code,
                         bit_stream_t* bs,
                         const Alphabet* c_dist_alphabet,
                         uint8_t** o_deflated) {
  int len_idx = len_code % 257;
  int len_base = c_len_bases[len_idx];
  int len_extra_bits = c_len_extra_bits[len_idx];
  int len = len_base + bs_consume_lsb(bs, len_extra_bits);
  int dist_idx =
      c_dist_alphabet ? decode(bs, c_dist_alphabet) : bs_consume_lsb(bs, 5);
  int dist_base = c_dist_bases[dist_idx];
  int dist_extra_bits = c_dist_extra_bits[dist_idx];
  int dist = dist_base + bs_consume_lsb(bs, dist_extra_bits);
  uint8_t* copy = *o_deflated - dist;
  for (int i = 0; i < len; ++i)
    *(*o_deflated)++ = *copy++;
}

int paeth(int a, int b, int c) {
  int p = a + b - c;
  int pa = abs(p - a);
  int pb = abs(p - b);
  int pc = abs(p - c);
  if (pa <= pb && pa <= pc)
    return a;
  else if (pb <= pc)
    return b;
  return c;
}

} // namespace

bool PNG::init(const os_char_t* path) {
  LOGI("Decoding: " OS_PCT, path);
  bool rv = true;
  file_t file;
  dynamic_array_t<uint8_t> data;
  dynamic_array_t<uint8_t> deflated_data;

  da_init(&data_, g_general_allocator);
  file_open(&file, path, file_t::MODE_READ);
  const size_t file_size = file_get_size(&file);
  da_init(&data, g_general_allocator);
  da_resize(&data, file_size);
  file_read(&file, data.p, file_size);
  if (algo_search_fast(begin(data),
                       end(data),
                       &c_png_signature[0],
                       &c_png_signature[0] + c_png_sig_len) == end(data)) {
    LOGW("Invalid png Signature");
    rv = false;
    goto out;
  }
  for (auto it = begin(data) + c_png_sig_len; it != end(data);) {
    int data_len = bswap32(*((int*)it));
    it += 4;
    const uint8_t* chunk_it = it;
    const int chunk_type = *((int*)it);
    it += 4;
    uint8_t* p = it;
    it += data_len;
    // uint8_t* cRC = it;
    it += 4;
    switch (chunk_type) {
    case fourcc("IHDR"): {
      if (data_len != 13)
        LOGW("  Invalid IHDR length");
      width_ = bswap32(*(int*)p);
      p += 4;
      height_ = bswap32(*(int*)p);
      p += 4;
      bit_depth_ = *p++;
      uint8_t color_type = *p++;
      switch (color_type) {
      case 0:
        bit_per_pixel_ = 1;
        break;
      case 2:
      case 3:
        bit_per_pixel_ = 3;
        break;
      case 4:
        bit_per_pixel_ = 2;
      case 6:
        bit_per_pixel_ = 2;
        break;
      default:
        LOGW("  Invalid color type");
        rv = false;
        goto out;
      }
      uint8_t compression_method = *p++;
      if (compression_method) {
        LOGW("  Invalid compression method");
        rv = false;
        goto out;
      }
      uint8_t filter_method = *p++;
      if (filter_method) {
        LOGW("  Invalid filter method");
        rv = false;
        goto out;
      }
      const uint8_t interlace_method = *p++;
      if (interlace_method > 1) {
        LOGW("  Invalid interlace method");
        rv = false;
        goto out;
      }
      break;
    }
    case fourcc("PLTE"):
      break;
    case fourcc("IDAT"): {
      bit_stream_t bs;
      bs_init(&bs, p);
      // 2 bytes of zlib header.
      uint32_t zlib_compress_method = bs_consume_lsb(&bs, 4);
      if (zlib_compress_method != 8) {
        LOGW("  Invalid zlib compression method");
        rv = false;
        goto out;
      }
      uint32_t zlib_compress_info = bs_consume_lsb(&bs, 4);
      if ((p[0] * 256 + p[1]) % 31) {
        LOGW("  Invalid FCHECK bits");
        rv = false;
        goto out;
      }
      bs_skip(&bs, 5);
      uint8_t fdict = bs_consume_lsb(&bs, 1);
      uint8_t flevel = bs_consume_lsb(&bs, 2);

      // 3 header bits
      const uint8_t bfinal = bs_consume_lsb(&bs, 1);
      const uint8_t ctype = bs_consume_lsb(&bs, 2);
      da_init(&deflated_data, g_general_allocator);
      da_resize(&deflated_data, (width_ + 1) * height_ * bit_depth_);
      uint8_t* deflated_p = deflated_data.p;
      if (ctype == 1) {
        // Fixed Huffman.
        for (;;) {
          int code;
          code = bs_consume_msb(&bs, 7);
          if (code >= 0 && code <= 23) {
            code += 256;
            if (code == 256)
              break;
          } else {
            code = code << 1 | bs_consume_msb(&bs, 1);
            if (code >= 48 && code <= 191) {
              *deflated_p++ = code - 48;
              continue;
            } else if (code >= 192 && code <= 199) {
              code += 88;
            } else {
              code = code << 1 | bs_consume_msb(&bs, 1);
              if (code >= 400 && code <= 511) {
                *deflated_p++ = code - 256;
                continue;
              } else {
                LOGW("  Can't decode fixed Huffman");
                rv = false;
                goto out;
              }
            }
          }
          decode_len_and_dist(code, &bs, nullptr, &deflated_p);
        }
      } else if (ctype == 2) {
        // Dynamic Huffman.
        int hlit = bs_consume_lsb(&bs, 5) + 257;
        int hdist = bs_consume_lsb(&bs, 5) + 1;
        int hclen = bs_consume_lsb(&bs, 4) + 4;
        uint8_t len_of_len[19] = {};
        const int c_len_alphabet[] = {
            16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15};
        for (int i = 0; i < hclen; ++i) {
          len_of_len[c_len_alphabet[i]] = bs_consume_lsb(&bs, 3);
        }
        Codes_For_Len code_lens_cfl[8];
        Alphabet code_len_alphabet = {code_lens_cfl, 0, 0};
        build_alphabet(len_of_len, 19, &code_len_alphabet);
        int index = 0;
        uint8_t lit_and_dist_lens[gc_max_code];
        while (index < hlit + hdist) {
          int code_len = decode(&bs, &code_len_alphabet);
          if (code_len < 16) {
            lit_and_dist_lens[index++] = code_len;
          } else {
            uint8_t repeat_num;
            uint8_t repeated_val = 0;
            if (code_len == 16) {
              repeat_num = bs_consume_lsb(&bs, 2) + 3;
              repeated_val = lit_and_dist_lens[index - 1];
            } else if (code_len == 17) {
              repeat_num = bs_consume_lsb(&bs, 3) + 3;
            } else if (code_len == 18) {
              repeat_num = bs_consume_lsb(&bs, 7) + 11;
            }
            memset(lit_and_dist_lens + index, repeated_val, repeat_num);
            index += repeat_num;
          }
          if (index > hlit + hdist) {
            LOGW("Can't decode literal and length alphabet, overflowed");
            rv = false;
            goto out;
          }
        }
        if (!lit_and_dist_lens[256]) {
          LOGW("  Symbol 256 can't have length of 0");
          rv = false;
          goto out;
        }
        Codes_For_Len lit_or_len_cfl[gc_max_code_len];
        Alphabet lit_or_len_alphabet = {lit_or_len_cfl, 0, 0};
        build_alphabet(lit_and_dist_lens, hlit, &lit_or_len_alphabet);
        Codes_For_Len dist_cfl[gc_max_code_len];
        Alphabet dist_alphabet = {dist_cfl, 0, 0};
        build_alphabet(lit_and_dist_lens + hlit, hdist, &dist_alphabet);
        for (;;) {
          int lit_or_len_code = decode(&bs, &lit_or_len_alphabet);
          if (lit_or_len_code == 256)
            break;
          if (lit_or_len_code < 256) {
            *deflated_p++ = lit_or_len_code;
            continue;
          }
          decode_len_and_dist(
              lit_or_len_code, &bs, &dist_alphabet, &deflated_p);
        }
      }
      da_resize(&data_, width_ * height_ * 4);
      int bytes_per_deflated_row = 4 * width_ + 1;
      int bytes_per_data_row = 4 * width_;
      for (int i = 0; i < height_; ++i) {
        const uint8_t filter_method =
            deflated_data.p[i * bytes_per_deflated_row];
        const int data_offset = i * bytes_per_data_row;
        const int deflated_offset = i * bytes_per_deflated_row + 1;
        uint8_t* a = &data_.p[i * bytes_per_data_row];
        uint8_t* b = nullptr;
        if (i)
          b = &data_.p[(i - 1) * bytes_per_data_row];
        uint8_t* c = nullptr;
        if (i)
          c = &data_.p[(i - 1) * bytes_per_data_row];
        switch (filter_method) {
        case 0: {
          memcpy(data_.p + data_offset,
                 deflated_data.p + deflated_offset,
                 bytes_per_data_row);
        } break;
        case 1: {
          for (int j = 0; j < 4; ++j)
            data_.p[data_offset + j] = deflated_data.p[deflated_offset + j];
          for (int j = 4; j < bytes_per_data_row; ++j)
            data_.p[data_offset + j] =
                deflated_data.p[deflated_offset + j] + *a++;
        } break;
        case 2: {
          if (!i) {
            for (int j = 0; j < bytes_per_data_row; ++j)
              data_.p[data_offset + j] = deflated_data.p[deflated_offset + j];
          } else {
            for (int j = 0; j < bytes_per_data_row; ++j)
              data_.p[data_offset + j] =
                  deflated_data.p[deflated_offset + j] + *b++;
          }
        } break;
        case 3: {
          if (!i) {
            for (int j = 0; j < 4; ++j)
              data_.p[data_offset + j] = deflated_data.p[deflated_offset + j];
            for (int j = 4; j < bytes_per_data_row; ++j)
              data_.p[data_offset + j] =
                  deflated_data.p[deflated_offset + j] + *a++ / 2;
          } else {
            for (int j = 0; j < 4; ++j)
              data_.p[data_offset + j] =
                  deflated_data.p[deflated_offset + j] + *b++ / 2;
            for (int j = 4; j < bytes_per_data_row; ++j)
              data_.p[data_offset + j] = deflated_data.p[deflated_offset + j] +
                                         ((int)(*a++) + (int)(*b++)) / 2;
          }
        } break;
        case 4: {
          if (!i) {
            for (int j = 0; j < bytes_per_data_row; ++j)
              data_.p[data_offset + j] = deflated_data.p[deflated_offset + j];
          } else {
            for (int j = 0; j < 4; ++j)
              data_.p[data_offset + j] =
                  deflated_data.p[deflated_offset + j] + *b++;
            for (int j = 4; j < bytes_per_data_row; ++j)
              data_.p[data_offset + j] = deflated_data.p[deflated_offset + j] +
                                         paeth(*(a++), *(b++), *(c++));
          }
        } break;
        default:
          LOGW("  Invalid filter method");
          goto out;
        }
      }
      LOGI("Successfully decoded: " OS_PCT, path);
    } break;
    case fourcc("IEND"):
      break;
    default:
      LOGI("Chunk %c%c%c%c ignored",
           *chunk_it,
           *(chunk_it + 1),
           *(chunk_it + 2),
           *(chunk_it + 3));
    }
  }
out:
  file_close(&file);
  da_destroy(&data);
  da_destroy(&deflated_data);
  return rv;
}

void PNG::destroy() { da_destroy(&data_); }

} // namespace gfx
