//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#pragma once

#include "core/dynamic_array.hpp"
#include "core/mono_time.hpp"
#include "math/vec2.hpp"
#include "math/vec3.hpp"

struct Color3 {
  vec3 points[6];
};

struct Particle {
  vec2 points[6];
};

struct Tex_Coord {
  vec2 points[6];
};

struct particle_system_t {
  dynamic_array_t<Particle> particles;
  dynamic_array_t<Tex_Coord> tex_coords;
  dynamic_array_t<Color3> colors;
  dynamic_array_t<vec2> velocities;
  dynamic_array_t<mono_time_t> emission_times;
  mono_time_t last_span_time;
  mono_time_t particle_duration;
  mono_time_t emission_delta;
  int particles_per_emission;
  int max_particles_num;
};

bool ps_init(particle_system_t* ps,
             mono_time_t particle_duration,
             mono_time_t emission_delta,
             int particles_per_emission);
void ps_destroy(particle_system_t* ps);
void ps_update(particle_system_t* ps, int64_t delta);
