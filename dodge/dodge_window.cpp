//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "dodge/dodge_window.hpp"

#include "core/common_paths.hpp"
#include "core/core_allocators.hpp"
#include "core/log.hpp"
#include "core/mono_time.hpp"
#include "core/string_builder.hpp"
#include "gfx/opengl/gl_loader.hpp"
#include "gfx/png.hpp"
#include "math/transform.inl"
#include "math/vec3.inl"
#define _USE_MATH_DEFINES
#include <math.h>

namespace {

void update_mvp(dodge_window_t* w) {
  w->mvp = w->model_mat * w->view_mat * w->projection_mat;
}

void update_movement(dodge_window_t* w) {
  bool need_update_view = false;
  vec3 eye_forward = vec3_normalize(w->target - w->eye);
  vec3 eye_right = vec3_normalize(vec3_cross(eye_forward, w->up));
  if (w->key_down[key_code_e::KC_W]) {
    vec3 forward = eye_forward * 0.05f;
    w->target += forward;
    w->eye += forward;
    need_update_view = true;
  }
  if (w->key_down[key_code_e::KC_S]) {
    vec3 backward = -eye_forward * 0.05f;
    w->target += backward;
    w->eye += backward;
    need_update_view = true;
  }
  if (w->key_down[key_code_e::KC_D]) {
    vec3 right = eye_right * 0.02f;
    w->target += right;
    w->eye += right;
    need_update_view = true;
  }
  if (w->key_down[key_code_e::KC_A]) {
    vec3 left = -eye_right * 0.02f;
    w->target += left;
    w->eye += left;
    need_update_view = true;
  }
  if (need_update_view) {
    w->view_mat = mat4_look_at_rh(w->eye, w->target, w->up);
    update_mvp(w);
  }
}

bool dodge_window_init_internal(window_t* w) {
  dodge_window_t* dw = (dodge_window_t*)w;
  ps_init(&dw->ps, s_to_mtime(20.0), s_to_mtime(1.0), 20);
  dw->last_frame_time = mtime_now();
  dw->model_mat = mat4_scale(.01f, .01f, .01f);
  dw->eye = vec3{0.f, 0.f, 1.f};
  dw->target = vec3{0.f, 0.f, 0.f};
  dw->up = vec3{0.f, 1.f, 0.f};
  dw->view_mat = mat4_look_at_rh(dw->eye, dw->target, dw->up);
  dw->projection_mat = mat4_perspective(M_PI / 2.f, 4.f / 3, 1.f, 100.f);
  update_mvp(dw);
  gfx::load_core_gl_functions();
  mono_time_t t = mtime_now();
  string_builder_t sb;
  strb_init(&sb, g_general_allocator, sizeof(os_char_t));
  strb_add_string(&sb, common_paths_get_exe_dir());
  strb_add_string(&sb, OS_LIT("assets/particle.png"));
  gfx::PNG particle;
  particle.init((const os_char_t*)(strb_build(&sb)));
  LOGI("PNG %f ms", mtime_to_ms(mtime_now() - t));
  strb_destroy(&sb);
  gl_gen_vertex_arrays(1, &dw->vao);
  gl_bind_vertex_array(dw->vao);
  gl_gen_buffers(1, &dw->vbo);
  gl_bind_buffer(GL_ARRAY_BUFFER, dw->vbo);

  gl_buffer_data(GL_ARRAY_BUFFER,
                 dw->ps.max_particles_num *
                     (sizeof(Particle) + sizeof(Tex_Coord) + sizeof(Color3)),
                 nullptr,
                 GL_STATIC_DRAW);
  GLuint vs_id = gl_create_shader(GL_VERTEX_SHADER);
  GLuint fs_id = gl_create_shader(GL_FRAGMENT_SHADER);
  const char* vs_str = R"(
  #version 330 core
  layout(location = 0) in vec2 particle;
  layout(location = 1) in vec2 a_uv;
  layout(location = 2) in vec3 a_color;
  uniform mat4 u_mvp;
  out vec2 uv;
  out vec3 color;
  void main() {
    uv = a_uv;
    color = a_color;
    gl_Position = u_mvp * vec4(particle, 0.0, 1.0);
  }
  )";
  const char* fs_str = R"(
  #version 330 core
  in vec2 uv;
  in vec3 color;
  out vec4 color_with_alpha;
  uniform sampler2D u_texture;
  void main() {
    color_with_alpha.w = texture(u_texture, uv).w;
    color_with_alpha.xyz = color;
  }
  )";
  gl_shader_source(vs_id, 1, &vs_str, NULL);
  gl_compile_shader(vs_id);
  gl_shader_source(fs_id, 1, &fs_str, NULL);
  gl_compile_shader(fs_id);
  dw->program = gl_create_program();
  gl_attach_shader(dw->program, vs_id);
  gl_attach_shader(dw->program, fs_id);
  gl_link_program(dw->program);

  // Texutre
  gl_gen_textures(1, &dw->texture);
  gl_bind_texture(GL_TEXTURE_2D, dw->texture);
  gl_tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  gl_tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  gl_tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  gl_tex_parameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  gl_tex_image_2d(GL_TEXTURE_2D,
                  0,
                  GL_RGBA,
                  particle.get_width(),
                  particle.get_height(),
                  0,
                  GL_RGBA,
                  GL_UNSIGNED_BYTE,
                  particle.pointer());
  gl_enable(GL_BLEND);
  gl_blend_func(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  dw->mvp_uniform = gl_get_uniform_location(dw->program, "u_mvp");
  return true;
}
void dodge_window_destroy(window_t* w) { ps_destroy(&((dodge_window_t*)w)->ps); }

void dodge_window_loop(window_t* w) {
  dodge_window_t* dw = (dodge_window_t*)w;
  mono_time_t curr_time = mtime_now();
  mono_time_t delta = curr_time - dw->last_frame_time;
  update_movement(dw);
  dw->last_frame_time = curr_time;
  ps_update(&dw->ps, delta);
  gl_clear(GL_COLOR_BUFFER_BIT);
  gl_clear_color(.0f, .0f, .0f, .1f);
  gl_use_program(dw->program);
  gl_uniform_matrix_4fv(dw->mvp_uniform, 1, GL_FALSE, &dw->mvp.a[0][0]);
  gl_enable_vertex_attrib_array(0);
  gl_enable_vertex_attrib_array(1);
  gl_enable_vertex_attrib_array(2);
  gl_bind_buffer(GL_ARRAY_BUFFER, dw->vbo);
  gl_buffer_sub_data(GL_ARRAY_BUFFER,
                     0,
                     dw->ps.particles.length * sizeof(Particle),
                     dw->ps.particles.p);
  gl_buffer_sub_data(GL_ARRAY_BUFFER,
                     dw->ps.particles.length * sizeof(Particle),
                     dw->ps.particles.length * sizeof(Tex_Coord),
                     dw->ps.tex_coords.p);
  gl_buffer_sub_data(GL_ARRAY_BUFFER,
                     dw->ps.particles.length *
                         (sizeof(Particle) + sizeof(Tex_Coord)),
                     dw->ps.particles.length * sizeof(Color3),
                     dw->ps.colors.p);
  gl_vertex_attrib_pointer(0, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
  gl_vertex_attrib_pointer(1,
                           2,
                           GL_FLOAT,
                           GL_FALSE,
                           0,
                           (void*)(dw->ps.particles.length * sizeof(Particle)));
  gl_vertex_attrib_pointer(2,
                           3,
                           GL_FLOAT,
                           GL_FALSE,
                           0,
                           (void*)(dw->ps.particles.length *
                                   (sizeof(Particle) + sizeof(Tex_Coord))));
  gl_draw_arrays(GL_TRIANGLES, 0, dw->ps.particles.length * 6);
  gl_disable_vertex_attrib_array(0);
}

void dodge_window_on_mouse_down(window_t* w, int x, int y) {
  dodge_window_t* dw = (dodge_window_t*)w;
  dw->is_mouse_down = true;
  dw->old_x = x;
  dw->old_y = y;
}

void dodge_window_on_mouse_up(window_t* w, int x, int y) {
  ((dodge_window_t*)w)->is_mouse_down = false;
}

void dodge_window_on_mouse_move(window_t* w, int x, int y) {
  dodge_window_t* dw = (dodge_window_t*)w;
  if (dw->is_mouse_down) {
    int delta_x = dw->old_x - x;
    int delta_y = dw->old_y - y;
    float angle_x = delta_x * 0.002f;
    float angle_y = delta_y * 0.002f;
    vec3 eye_forward = vec3_normalize(dw->target - dw->eye);
    vec3 eye_right = vec3_cross(eye_forward, dw->up);
    mat4 rotate_vert = mat4_rotate(eye_right, angle_y);
    mat4 rotate_hori = mat4_rotate(dw->up, angle_x);
    eye_forward =
        vec4_to_vec3(vec4{eye_forward.x, eye_forward.y, eye_forward.z, 0.f} *
                     rotate_vert * rotate_hori);
    dw->target = dw->eye + eye_forward * vec3_length(dw->target - dw->eye);
    dw->view_mat = mat4_look_at_rh(dw->eye, dw->target, dw->up);
    update_mvp(dw);
    dw->old_x = x;
    dw->old_y = y;
  }
}

void dodge_window_on_key_event(window_t* w,
                               key_code_e key,
                               bool is_down) {
  ((dodge_window_t*)w)->key_down[key] = is_down;
}

const window_fns_t gc_dodge_fns = {
    &dodge_window_init_internal,
    &dodge_window_destroy,
    &dodge_window_loop,
    &dodge_window_on_mouse_down,
    &dodge_window_on_mouse_up,
    &dodge_window_on_mouse_move,
    &dodge_window_on_key_event,
};

} // namespace

bool dodge_window_init(dodge_window_t* w, const os_char_t* name) {
  return window_init((window_t*)w, name, &gc_dodge_fns);
}
