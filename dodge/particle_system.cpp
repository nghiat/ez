//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "dodge/particle_system.hpp"

#include "core/core_allocators.hpp"
#include "core/dynamic_array.inl"
#include <stdlib.h>

bool ps_init(particle_system_t* ps,
             mono_time_t particle_duration,
             mono_time_t emission_delta,
             int particles_per_emission) {
  da_init(&ps->particles, g_general_allocator);
  da_init(&ps->tex_coords, g_general_allocator);
  da_init(&ps->colors, g_general_allocator);
  da_init(&ps->velocities, g_general_allocator);
  da_init(&ps->emission_times, g_general_allocator);
  ps->last_span_time = mtime_now();
  ps->particle_duration = particle_duration;
  ps->emission_delta = emission_delta;
  ps->particles_per_emission = particles_per_emission;
  ps->max_particles_num =
      ps->particle_duration / ps->emission_delta * particles_per_emission;
  return true;
}

void ps_destroy(particle_system_t* ps) {
  da_destroy(&ps->particles);
  da_destroy(&ps->tex_coords);
  da_destroy(&ps->colors);
  da_destroy(&ps->velocities);
  da_destroy(&ps->emission_times);
}

void ps_update(particle_system_t* ps, int64_t delta) {
  mono_time_t now = mtime_now();
  // Remove first
  for (size_t i = 0; i < ps->emission_times.length; ++i) {
    if (now - ps->emission_times.p[i] >= ps->particle_duration) {
      da_remove_at(&ps->emission_times, i);
      da_remove_range(&ps->particles,
                      i * ps->particles_per_emission,
                      ps->particles_per_emission);
      da_remove_range(&ps->tex_coords,
                      i * ps->particles_per_emission,
                      ps->particles_per_emission);
      da_remove_range(&ps->colors,
                      i * ps->particles_per_emission,
                      ps->particles_per_emission);
      da_remove_range(&ps->velocities,
                      i * ps->particles_per_emission,
                      ps->particles_per_emission);
      --i;
    }
  }
  // Add
  if (now - ps->last_span_time >= ps->emission_delta) {
    da_append(&ps->emission_times, now);
    srand(mtime_now());
    for (int i = 0; i < ps->particles_per_emission; ++i) {
      constexpr float particle_size = 32.f;
      Particle particle = {{{-particle_size, particle_size},
                            {particle_size, particle_size},
                            {particle_size, -particle_size},
                            {-particle_size, particle_size},
                            {particle_size, -particle_size},
                            {-particle_size, -particle_size}}};
      Tex_Coord coord = {{{0.f, 1.f},
                          {1.f, 1.f},
                          {1.f, 0.f},
                          {0.f, 1.f},
                          {1.f, 0.f},
                          {0.f, 0.f}}};
      vec3 color = {rand() % 196 / 196.f + .25f,
                    rand() % 196 / 196.f + .25f,
                    rand() % 196 / 196.f + .25f};
      Color3 colors = {color, color, color, color, color, color};
      vec2 velocity = {rand() % 100 - 50.f, rand() % 100 - 50.f};
      da_append(&ps->particles, particle);
      da_append(&ps->tex_coords, coord);
      da_append(&ps->colors, colors);
      da_append(&ps->velocities, velocity);
    }
    ps->last_span_time = now;
  }

  size_t count = ps->particles.length;
  float delta_s = mtime_to_s(delta);
  for (int i = 0; i < count; ++i) {
    ps->velocities.p[i].y -= .8f * delta_s;
    for (int j = 0; j < 6; ++j) {
      ps->particles.p[i].points[j].x += delta_s * ps->velocities.p[i].x;
      ps->particles.p[i].points[j].y += delta_s * ps->velocities.p[i].y;
    }
  }
}
