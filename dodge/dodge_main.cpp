//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/common_paths.hpp"
#include "core/core_allocators.hpp"
#include "core/core_init.hpp"
#include "core/log.hpp"
#include "core/platform.hpp"
#include "dodge/dodge_window.hpp"
#if defined(OS_WIN)
#  include <Windows.h>
#endif

#if defined(OS_WIN)
int CALLBACK WinMain(_In_ HINSTANCE instance,
                     _In_ HINSTANCE prev_instance,
                     _In_ LPSTR cmd_line,
                     _In_ int cmd_show) {
#elif defined(OS_LINUX)
int main(int argc, char** argv) {
#endif
  core_init(OS_LIT("dodge.log"));
  dodge_window_t dw;
  dodge_window_init(&dw, OS_LIT("Dodge"));
  window_loop(&dw);
  return 0;
}
