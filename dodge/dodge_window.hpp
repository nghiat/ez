//----------------------------------------------------------------------------//
// This file is distributed under the MIT License.                            //
// See LICENSE.txt for details.                                               //
// Copyright (C) Tran Tuan Nghia <trantuannghia95@gmail.com> 2019             //
//----------------------------------------------------------------------------//

#include "core/mono_time.hpp"
#include "dodge/particle_system.hpp"
#include "gfx/opengl/gl.hpp"
#include "math/mat4.hpp"
#include "math/vec4.hpp"
#include "window/window.hpp"

struct dodge_window_t : window_t {
  particle_system_t ps;
  mono_time_t last_frame_time;
  GLuint vbo;
  GLuint vao;
  GLuint program;
  GLuint texture;
  GLuint mvp_uniform;
  mat4 model_mat;
  mat4 view_mat;
  mat4 projection_mat;
  mat4 mvp;
  vec3 eye;
  vec3 target;
  vec3 up;
  int old_x;
  int old_y;
  bool is_mouse_down = false;
  bool key_down[key_code_e::KC_COUNT] = {};
};

bool dodge_window_init(dodge_window_t* w, const os_char_t* name);
